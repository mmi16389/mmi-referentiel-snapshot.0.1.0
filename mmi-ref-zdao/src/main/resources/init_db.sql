/*CREATE SCHEMA "public";*/

/*CREATE SEQUENCE hibernate_sequence START WITH 1;*/

CREATE TABLE codepostal (
	code_insee           varchar(255)  NOT NULL,
	libelle_ville        varchar(255)  ,
	libelle_commune      varchar(255)  ,
	type_commune         varchar(255)  ,
	code_postal          varchar(255)  ,
	CONSTRAINT codepostal_pkey PRIMARY KEY ( code_insee )
 );


CREATE TABLE langue ( 
	id                   bigint  NOT NULL,
	libelle_langue       varchar(255)  ,
	CONSTRAINT langue_pkey PRIMARY KEY ( id )
 );


CREATE TABLE "parameter" ( 
	param_context        varchar(255)  NOT NULL,
	param_name           varchar(255)  NOT NULL,
	param_value          varchar(255)  ,
	CONSTRAINT parameter_pkey PRIMARY KEY ( param_context, param_name )
 );

CREATE TABLE pays ( 
	id                   varchar(255)  NOT NULL,
	continent            varchar(255)  ,
	eu                   bool  ,
	code_insee           varchar(255)  ,
	libelle              varchar(255)  ,
	libelle_nationalite  varchar(255)  ,
	schengen             integer  ,
	CONSTRAINT pays_pkey PRIMARY KEY ( id )
 );

CREATE TABLE region (
	code_region          varchar(6)  NOT NULL,
	libelle_region       varchar(255)  ,
	CONSTRAINT region_pkey PRIMARY KEY ( code_region )
 );

CREATE TABLE departement (
	code_departement     varchar(3)  NOT NULL,
	libelle_departement  varchar(255)  ,
	code_region          varchar(6)  ,
	CONSTRAINT departement_pkey PRIMARY KEY ( code_departement )
 );


ALTER TABLE departement ADD CONSTRAINT fkk6o3euvoyybbaap2n47ccerfj FOREIGN KEY ( code_region ) REFERENCES region( code_region );

COMMENT ON CONSTRAINT fkk6o3euvoyybbaap2n47ccerfj ON departement IS '';


