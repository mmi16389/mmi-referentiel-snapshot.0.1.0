CREATE TABLE dt_adresse (
	id                   bigint  NOT NULL,
	ville                varchar(127)  ,
	complement_adresse   varchar(127)  ,
	code_insee           varchar(127)  ,
	libelle_voie         varchar(127)  ,
	numero_voie          varchar(127)  ,
	code_postal          varchar(30)  ,
	CONSTRAINT dt_adresse_pkey PRIMARY KEY ( id )
 );

CREATE TABLE direction_territorial (
	id                   bigint  NOT NULL,
	libelle_court        varchar(127)  ,
	libelle_long         varchar(255)  ,
	telephone            varchar(30)  ,
	fax                  varchar(30)  ,
	service              varchar(127)  ,
	directeur_prenom     varchar(127)  ,
	directeur_nom        varchar(127)  ,
	id_dt_adresse        bigint  ,
	signature            bytea  ,
	CONSTRAINT dt_direction_territorial_pkey PRIMARY KEY ( id )
 );

CREATE TABLE direction_territorial_departement (
	id                   bigint  NOT NULL,
	code_departement     varchar(3)  ,
	id_direction_territorial bigint  NOT NULL,
	CONSTRAINT dt_dt_dep_pkey PRIMARY KEY ( id )
 );

ALTER TABLE direction_territorial ADD CONSTRAINT fk_dt_addresse FOREIGN KEY ( id_dt_adresse ) REFERENCES dt_adresse( id );

ALTER TABLE direction_territorial_departement ADD CONSTRAINT fk_dt_dep FOREIGN KEY ( id_direction_territorial ) REFERENCES direction_territorial( id );