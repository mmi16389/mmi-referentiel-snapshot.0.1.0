package fr.mmi.ref.zerrors;


/**
 * All error codes in relation with src/main/resources/logs/messages.properties
 */
public enum TypedErrorCodes implements ErrorCode {

    SECURITY_GENERIC            ("APP_SECURITY_GENERICS"),
    SECURITY_LOGIN              ("APP_SECURITY_AUTLOGIN"),
    USER_NOT_FOUND              ("APP_USER_NOT_FOUND"),
    USER_EMAIL_NOT_FOUND        ("APP_USER_EMAIL_NOT_FOUND"),
    REQUIRED_FIELD              ("APP_REQUIRED_FIELD");

    private String code;

    TypedErrorCodes(String code) {
        this.code = code;
    }

    @Override
    public String get() {
        return this.code;
    }
}
