package fr.mmi.ref.jpa.dna;

import fr.mmi.ref.jpa.dna.entities.Language;
import org.springframework.data.repository.CrudRepository;

public interface LanguageRepository extends CrudRepository<Language,Long> {


}
