package fr.mmi.ref.jpa.dna;


import fr.mmi.ref.jpa.dna.entities.Department;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;


public interface DepartmentRepository extends CrudRepository<Department,String> {

    /**
     * retrieve a department from its code
     * @param departmentCode
     * @return
     */
    @Query("SELECT d FROM Department d WHERE d.departmentCode =:departmentCode")
    Department initDepartment(@Param("departmentCode") String departmentCode);

    /**
     * get all departments which name like 'name'
     *
     * @param name
     * @return
     */
    @Query("select d from Department d where UPPER(d.departmentName) like UPPER(:name)")
    List<Department> findAllDepartmentByName(@Param("name") String name);

    //@Query("SELECT d.departmentCode FROM Department d WHERE d.region.regionCode IN (SELECT c.region.regionCode FROM Department c where c.departmentCode =:departmentCode)")
    //List<String> findAllDepartmentRegion(@Param("departmentCode") String departmentCode);

    //@Query("select d from Department d where d.region.regionCode = :regionCode")
    //List<Department> findAllDepartmentByRegionCode(@Param("regionCode") String regionCode);
}
