package fr.mmi.ref.jpa.hexapost.entities;


import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "lhexavia")
public class LHexavia implements Serializable, EntityBean {

    private static final long serialVersionUID = 1L;


    /** id */
    @Id
    @Column(name="id_adresse")
    private String idAddress;

    @Column(name="code_postal")
    private Long postalCode;

    /** commune label */
    @Column(name="lib_commune")
    private String townLabel;

    @Column(name="lib_l5")
    private String info;

    public LHexavia(
            String idAddress, Long postalCode, String townLabel, String info
    ) {
        this.idAddress = idAddress;
        this.postalCode = postalCode;
        this.townLabel = townLabel;
        this.info = info;
    }

    public LHexavia() {

    }

    public String getIdAddress() {
        return idAddress;
    }

    public void setIdAddress(String idAddress) {
        this.idAddress = idAddress;
    }

    public String getTownLabel() {
        return townLabel;
    }

    public void setTownLabel(String townLabel) {
        this.townLabel = townLabel;
    }


    public String getInfo() {
        return info;
    }


    public void setInfo(String info) {
        this.info = info;
    }

    public Long getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(Long postalCode) {
        this.postalCode = postalCode;
    }


    public String asString() {
        StringBuilder sb = new StringBuilder();

        sb.append("id_adresse=").append(idAddress).append(", ");
        sb.append("code_postal=").append(postalCode).append(", ");
        sb.append("lib_commune=").append(townLabel).append(", ");
        sb.append("lib_l5=").append(info);

        return sb.toString();
    }

}
