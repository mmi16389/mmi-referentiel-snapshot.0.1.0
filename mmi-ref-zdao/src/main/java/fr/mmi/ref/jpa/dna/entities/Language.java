package fr.mmi.ref.jpa.dna.entities;


import javax.persistence.*;
import java.io.Serializable;


@Entity
@Table(name="Langue")
public class Language implements Serializable ,EntityBean {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="id")
	private Long id;

	@Column(name="libelle_langue")
	private String language;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}
}