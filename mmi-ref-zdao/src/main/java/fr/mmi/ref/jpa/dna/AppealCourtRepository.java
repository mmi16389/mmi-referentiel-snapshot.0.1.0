package fr.mmi.ref.jpa.dna;

import fr.mmi.ref.jpa.dna.entities.AppealCourt;
import org.springframework.data.repository.CrudRepository;


public interface AppealCourtRepository extends CrudRepository<AppealCourt, Long> {


}
