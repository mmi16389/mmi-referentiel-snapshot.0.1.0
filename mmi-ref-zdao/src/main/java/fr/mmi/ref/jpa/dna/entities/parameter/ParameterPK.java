package fr.mmi.ref.jpa.dna.entities.parameter;


import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

/**
 * The primary key class for the parameter database table.
 * 
 */
@Embeddable
public class ParameterPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name="param_context")
	private String paramContext;

	@Column(name="param_name")
	private String paramName;


	public ParameterPK() {
		//empty
	}

	public String getParamContext() {
		return paramContext;
	}

	public void setParamContext(String paramContext) {
		this.paramContext = paramContext;
	}

	public String getParamName() {
		return paramName;
	}

	public void setParamName(String paramName) {
		this.paramName = paramName;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof ParameterPK)) {
			return false;
		}
		ParameterPK castOther = (ParameterPK)other;
		return 
			this.paramName.equals(castOther.paramName)
			&& this.paramContext.equals(castOther.paramContext);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + this.paramName.hashCode();
		hash = hash * prime + this.paramContext.hashCode();
		
		return hash;
	}
}