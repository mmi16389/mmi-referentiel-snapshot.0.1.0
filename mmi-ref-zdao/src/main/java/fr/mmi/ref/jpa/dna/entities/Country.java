package fr.mmi.ref.jpa.dna.entities;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;


/**
 * The persistent class for the pays database table.
 * 
 */
@Entity
@Table(name="pays")
public class Country implements Serializable, EntityBean {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="id")
	private String id;

	@Column(name="code_insee")
	private String inseeCode;

	@Column(name="continent")
	private String continent;

	@Column(name="libelle")
	private String name;

	@Column(name="libelle_nationalite")
	private String nationalityName;

	@Column(name="schengen")
	private Integer schengen;

	@Column(name="eu")
	private Boolean eu;


	public Country() {
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getInseeCode() {
		return inseeCode;
	}

	public void setInseeCode(String inseeCode) {
		this.inseeCode = inseeCode;
	}

	public String getContinent() {
		return continent;
	}

	public void setContinent(String continent) {
		this.continent = continent;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getNationalityName() {
		return nationalityName;
	}

	public void setNationalityName(String nationalityName) {
		this.nationalityName = nationalityName;
	}

	public Integer getSchengen() {
		return schengen;
	}

	public void setSchengen(Integer schengen) {
		this.schengen = schengen;
	}

	public Boolean isEu() {
		return eu;
	}

	public void setEu(Boolean eu) {
		this.eu = eu;
	}
}