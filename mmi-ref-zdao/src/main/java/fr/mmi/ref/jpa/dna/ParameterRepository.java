package fr.mmi.ref.jpa.dna;

import fr.mmi.ref.jpa.dna.entities.parameter.Parameter;
import fr.mmi.ref.jpa.dna.entities.parameter.ParameterPK;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * The repository interface to manage Parameter entity
 *
 * @author Open groupe
 * @since 0.1.0-SNAPSHOT
 */
public interface ParameterRepository extends CrudRepository<Parameter, ParameterPK> {

    /**
     * @return all parameters
     */
    List<Parameter> findAll();

    /**
     * return the value matching the param name
     * @param paramName the parameter name
     * @return corresponding value
     */
    @Query("SELECT paramValue from Parameter p WHERE p.id.paramName = :paramName")
    String findByCode(@Param("paramName") String paramName);

    /**
     * return parameters whose matching 'paramContext'
     * @param paramContext
     * @return
     */
    @Query("SELECT p from Parameter p WHERE p.id.paramContext = :paramContext")
    List<Parameter> findByParamContext(@Param("paramContext") String paramContext);

    /**
     * return the value whose match the context, name couple
     * @param paramContext context
     * @param paramName name
     * @return a value
     */
    @Query("SELECT p.paramValue  FROM  Parameter p  WHERE  p.id.paramContext = :paramContext  AND   p.id.paramName = :paramName")
    String findParamValueById(@Param("paramContext") String paramContext, @Param("paramName") String paramName);

    /**
     * return the name match the context, value couple
     * @param paramContext the context
     * @param paramValue the value
     * @return the param's name
     */
    @Query("SELECT p.id.paramName FROM Parameter p WHERE p.id.paramContext = :paramContext AND p.paramValue = :paramValue ")
    String findParamNamebyContextAndValue(@Param("paramContext") String paramContext, @Param("paramValue") String paramValue);

}
