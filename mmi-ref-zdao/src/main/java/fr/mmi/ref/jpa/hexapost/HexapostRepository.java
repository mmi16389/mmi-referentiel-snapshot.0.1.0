//package fr.ofii.ref.jpa;
//
//import fr.ofii.ref.jpa.entities.hexapost.VHexavia;
//import org.springframework.data.jpa.repository.Query;
//import org.springframework.data.repository.CrudRepository;
//
//import java.util.List;
//
///**
// * VHexavia Repository
// */
//public interface HexapostRepository extends CrudRepository<VHexavia, Long> {
//
//    /**
//     * find all hexapost from VHexavia
//     *
//     * @return hexapost list
//     */
//    //@Query("SELECT a FROM VHexavia a JOIN FETCH a.hexapost adr where 1=1 and (( case length(str(a.hexapost.commune.postalCode)) when 4 then concat('0', str(a.hexapost.commune.postalCode)) else str(a.hexapost.commune.postalCode) end) like '20%')")
//    /*@Query("SELECT a FROM VHexavia a JOIN FETCH a.hexapost adr where 1=1 "+
//            "and (    str(a.hexapost.hexapostCommune.postalCode) like '01%' "+
//                //"  or str(a.hexapost.commune.postalCode) like '91%' "+
//                "  or str(a.hexapost.hexapostCommune.postalCode) like '28%' "+
//                //"  or str(a.hexapost.commune.postalCode) like '3%' "+
//            //"  or str(a.hexapost.commune.postalCode) like '43%' "+
//                ")")*/
//    @Query("SELECT a FROM VHexavia a JOIN FETCH a.hexapost adr where a.hexapost.hexapostCommune.postalCode < 29000" )
//    //@Query("SELECT a FROM VHexavia a JOIN FETCH a.hexapost adr where (a.hexapost.hexapostCommune.postalCode > 27999) and (a.hexapost.hexapostCommune.postalCode < 29000)" )
//    //@Query("SELECT a FROM VHexavia a JOIN FETCH a.hexapost adr")
//    List<VHexavia> findAll();
//
//
//}
package fr.mmi.ref.jpa.hexapost;

import fr.mmi.ref.jpa.hexapost.entities.LHexavia;
import fr.mmi.ref.jpa.hexapost.entities.VHexavia;
import fr.mmi.ref.jpa.hexapost.entities.VHexavia3;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * VHexavia Repository
 */
public interface HexapostRepository extends CrudRepository<VHexavia, Long> {

    /**
     * find all hexapost from VHexavia
     *
     * @return hexapost list
     */
    @Query("SELECT v FROM VHexavia v JOIN FETCH v.lHexavia l")
    List<VHexavia> findAll();

    /**
     *
     * @param b0
     * @param b1
     * @return
     */
    @Query("SELECT new VHexavia(v.id, v.wayLabel, v.postalCode, l.idAddress, l.townLabel, l.info) FROM VHexavia v JOIN v.lHexavia l where (v.postalCode >= :b0) and (v.postalCode < :b1)" )
    List<VHexavia> getAddressesFromTo(@Param("b0") Long b0, @Param("b1") Long b1);

    /**
     *
     * @param b0
     * @param b1
     * @return
     */
    @Query("SELECT new VHexavia3(v.id, v.wayLabel, v.postalCode, v.idAddress, v.townLabel, v.info) FROM VHexavia3 v where (v.postalCode >= :b0) and (v.postalCode < :b1)" )
    List<VHexavia3> getAddressesFromTo3(@Param("b0") Long b0, @Param("b1") Long b1);

}

