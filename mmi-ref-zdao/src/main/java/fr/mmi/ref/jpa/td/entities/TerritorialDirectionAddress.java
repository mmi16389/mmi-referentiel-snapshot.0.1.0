package fr.ofii.ref.jpa.td.entities;

import com.opengroup.pop.EntityBean;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name="dt_adresse")
public class TerritorialDirectionAddress implements Serializable, EntityBean {

    private static final long serialVersionUID = 1L;

    @Override
    public String toString() {
        return "TerritorialDirectionAddress{" +
                "id=" + id +
                ", city='" + city + '\'' +
                ", complement='" + complement + '\'' +
                ", inseeCode='" + inseeCode + '\'' +
                ", streetName='" + streetName + '\'' +
                ", streetNumber='" + streetNumber + '\'' +
                ", zipCode='" + zipCode + '\'' +
                '}';
    }

    @Id
    @Column(name="id")
    private Long id;
    @Column(name="ville")
    private String city;
    @Column(name="complement_adresse")
    private String complement;
    @Column(name="code_insee")
    private String inseeCode;
    @Column(name="libelle_voie")
    private String streetName;
    @Column(name="numero_voie")
    private String streetNumber;
    @Column(name="code_postal")
    private String zipCode;

    public TerritorialDirectionAddress() {

    }

    public TerritorialDirectionAddress(Long id) {

    }

    public TerritorialDirectionAddress(Long id, String city, String complement, String inseeCode, String streetName, String streetNumber, String zipCode) {
        this.id = id;
        this.city = city;
        this.complement = complement;
        this.inseeCode = inseeCode;
        this.streetName = streetName;
        this.streetNumber = streetNumber;
        this.zipCode = zipCode;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getComplement() {
        return complement;
    }

    public void setComplement(String complement) {
        this.complement = complement;
    }

    public String getInseeCode() {
        return inseeCode;
    }

    public void setInseeCode(String inseeCode) {
        this.inseeCode = inseeCode;
    }

    public String getStreetName() {
        return streetName;
    }

    public void setStreetName(String streetName) {
        this.streetName = streetName;
    }

    public String getStreetNumber() {
        return streetNumber;
    }

    public void setStreetNumber(String streetNumber) {
        this.streetNumber = streetNumber;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }
}
