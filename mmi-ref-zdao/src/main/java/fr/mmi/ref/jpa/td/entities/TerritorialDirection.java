package fr.ofii.ref.jpa.td.entities;

import com.opengroup.pop.EntityBean;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name="direction_territorial")
public class TerritorialDirection implements Serializable, EntityBean {
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name="id")
    private Long id;

    @Column(name = "libelle_court")
    private String shortLabel;

    @Column(name="libelle_long")
    private String longLabel;

    @Column(name="telephone")
    private String phoneNumber;

    @Column(name="fax")
    private String faxNumber;

    @Column(name="service")
    private String service;

    @Column(name="directeur_prenom")
    private String firstNameDirector;

    @Column(name="directeur_nom")
    private String lastNameDirector;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="id_dt_adresse")
    private TerritorialDirectionAddress address;

    @Column(name="signature_chemin")
    private String signaturePath;


    public TerritorialDirection() {

    }

    public TerritorialDirection(Long id) {

    }

    public TerritorialDirection(Long id, String shortLabel, String longLabel, String phoneNumber, String faxNumber, String service, String firstNameDirector, String lastNameDirector, TerritorialDirectionAddress address, String signaturePath) {
        this.id = id;
        this.shortLabel = shortLabel;
        this.longLabel = longLabel;
        this.phoneNumber = phoneNumber;
        this.faxNumber = faxNumber;
        this.service = service;
        this.firstNameDirector = firstNameDirector;
        this.lastNameDirector = lastNameDirector;
        this.address = address;
        this.signaturePath = signaturePath;
    }

    public Long getId() { return id; }
    public void setId(Long id) { this.id = id; }

    public String getShortLabel() {
        return shortLabel;
    }

    public void setShortLabel(String shortLabel) {
        this.shortLabel = shortLabel;
    }

    public String getLongLabel() {
        return longLabel;
    }

    public void setLongLabel(String longLabel) {
        this.longLabel = longLabel;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getFaxNumber() {
        return faxNumber;
    }

    public void setFaxNumber(String faxNumber) {
        this.faxNumber = faxNumber;
    }

    public String getService() {
        return service;
    }

    public void setService(String service) {
        this.service = service;
    }

    public String getFirstNameDirector() {
        return firstNameDirector;
    }

    public void setFirstNameDirector(String firstNameDirector) {
        this.firstNameDirector = firstNameDirector;
    }

    public String getLastNameDirector() {
        return lastNameDirector;
    }

    public void setLastNameDirector(String lastNameDirector) {
        this.lastNameDirector = lastNameDirector;
    }

    public TerritorialDirectionAddress getAddress() {
        return address;
    }

    public void setAddress(TerritorialDirectionAddress address) {
        this.address = address;
    }

    public String getSignaturePath() {
        return signaturePath;
    }

    public void setSignaturePath(String signaturePath) {
        this.signaturePath = signaturePath;
    }

    @Override
    public String toString() {
        return "TerritorialDirection{" +
                "id=" + id +
                ", shortLabel='" + shortLabel + '\'' +
                ", longLabel='" + longLabel + '\'' +
                ", phoneNumber='" + phoneNumber + '\'' +
                ", faxNumber='" + faxNumber + '\'' +
                ", service='" + service + '\'' +
                ", firstNameDirector='" + firstNameDirector + '\'' +
                ", lastNameDirector='" + lastNameDirector + '\'' +
                ", address=" + address + '\'' +
                ", signaturePath=" + signaturePath +
                '}';
    }
}
