package fr.mmi.ref.jpa.dna;

import fr.mmi.ref.jpa.dna.entities.Country;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface CountryRepository extends CrudRepository<Country, String> {

    /**
     * retrieve country from its id
     *
     * @param id
     * @return
     */
    @Query(" SELECT c FROM Country c WHERE c.id = :id")
    Country getCountryById(@Param("id") String id);

    /**
     * get all nationalities which name like 'nationalityName'
     *
     * @param nationalityName
     * @return
     */
    @Query(" SELECT c FROM Country c WHERE UPPER(c.nationalityName) like UPPER(:nationalityName)")
    List<Country> searchByNationalityName(@Param("nationalityName") String nationalityName);

}
