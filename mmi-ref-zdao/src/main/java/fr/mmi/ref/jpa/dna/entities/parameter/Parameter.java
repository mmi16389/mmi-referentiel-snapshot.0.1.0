package fr.mmi.ref.jpa.dna.entities.parameter;


import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQuery;
import java.io.Serializable;


/**
 * The persistent class for the parameter database table.
 */
@Entity
@NamedQuery(name = "Parameter.findAll", query = "SELECT p FROM Parameter p")
public class Parameter implements Serializable, EntityBean {
    private static final long serialVersionUID = 1L;

    @EmbeddedId
    private ParameterPK id;


    @Column(name = "param_value")
    private String paramValue;


    public Parameter() {
        // empty
    }

    public ParameterPK getId() {
        return this.id;
    }

    public void setId(ParameterPK id) {
        this.id = id;
    }

    public String getParamValue() {
        return paramValue;
    }

    public void setParamValue(String paramValue) {
        this.paramValue = paramValue;
    }
}