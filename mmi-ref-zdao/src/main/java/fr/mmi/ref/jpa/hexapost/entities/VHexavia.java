package fr.mmi.ref.jpa.hexapost.entities;


import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "vhexavia")
public class VHexavia implements Serializable, EntityBean {
    private static final long serialVersionUID = 1L;

    /** id */
    @Id
    @Column(name="mat_voie")
    private Long id;


    /** lib_voie */
    @Column(name="lib_voie")
    private String wayLabel;

    /** code_postal */
    @Column(name="code_postal")
    private Long postalCode;

    /** join to LHexavia */
    @OneToOne()
    @JoinColumn(name="id_adresse")
    private LHexavia lHexavia;






    /**
     * empty constructor
     */
    public VHexavia() {
    }

    /**
     * q0 query
     * @param vId
     * @param vWayLabel
     * @param vPostalCode
     * @param lIdAddress
     * @param lTownLabel
     * @param lInfo
     */
    public VHexavia(Long vId, String vWayLabel, Long vPostalCode, String lIdAddress, String lTownLabel, String lInfo) {
        this.id = vId;
        this.wayLabel = vWayLabel;
        this.postalCode = vPostalCode;
        this.lHexavia = new LHexavia();
        this.lHexavia.setIdAddress( lIdAddress);
        this.lHexavia.setInfo( lInfo );
        this.lHexavia.setPostalCode( vPostalCode );
        this.lHexavia.setTownLabel( lTownLabel );
    }


    public VHexavia assignWay(String wayLabel, Long postalCode) {
        this.wayLabel = wayLabel;
        this.postalCode = postalCode;
        return this;
    }

    public VHexavia assignTown(LHexavia lHexavia) {
        this.lHexavia = lHexavia;
        return this;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getWayLabel() {
        return wayLabel;
    }

    public void setWayLabel(String wayLabel) {
        this.wayLabel = wayLabel;
    }

    public Long getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(Long postalCode) {
        this.postalCode = postalCode;
    }

    public LHexavia getlHexavia() {
        return lHexavia;
    }

    public void setlHexavia(LHexavia lHexavia) {
        this.lHexavia = lHexavia;
    }



    public String asString() {
        StringBuilder sb = new StringBuilder();

        sb.append("mat_voie=").append(id).append(", ");
        sb.append("lib_voie=").append(wayLabel).append(", ");
        sb.append("code_postal=").append(postalCode).append(", ");
        sb.append( "[").append( lHexavia.asString() ).append("]");

        return sb.toString();
    }


}

