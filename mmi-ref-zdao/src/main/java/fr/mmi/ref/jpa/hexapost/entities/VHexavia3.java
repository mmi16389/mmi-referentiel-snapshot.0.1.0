package fr.mmi.ref.jpa.hexapost.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

@Entity
@Table(name = "vhexavia")
@SecondaryTable(name="lhexavia", pkJoinColumns={ @PrimaryKeyJoinColumn(name="id_adresse")})
public class VHexavia3 implements Serializable, EntityBean {
    private static final long serialVersionUID = 1L;

    /** id */
    //@Id
    @Column(name="mat_voie")
    private Long id;


    /** lib_voie */
    @Column(name="lib_voie")
    private String wayLabel;

    @Column(name="code_postal")
    private Long postalCode;

    @Id
    @Column(name="id_adresse")
    private String idAddress;

    /** commune label */
    @Column(name="lib_commune", table = "lhexavia")
    private String townLabel;

    @Column(name="lib_l5", table = "lhexavia")
    private String info;


    /**
     * empty constructor
     */
    public VHexavia3() {
    }


    public VHexavia3(Long id, String wayLabel, Long postalCode, String idAddress, String townLabel, String info) {
        this.id = id;
        this.wayLabel = wayLabel;
        this.postalCode = postalCode;
        this.idAddress = idAddress;
        this.townLabel = townLabel;
        this.info = info;
    }


    public VHexavia3 assignWay(Long id, String wayLabel, Long postalCode) {
        this.id = id;
        this.wayLabel = wayLabel;
        this.postalCode = postalCode;
        return this;
    }

    public VHexavia3 assignTown(String townLabel, String info) {
        this.townLabel = townLabel;
        this.info = info;
        return this;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getWayLabel() {
        return wayLabel;
    }

    public void setWayLabel(String wayLabel) {
        this.wayLabel = wayLabel;
    }

    public Long getPostalCode() {
        return this.postalCode;
    }

    public String getPostalCodeAsString() {
        return (postalCode<10000l) ? new StringBuilder("0").append( String.valueOf(this.postalCode)).toString() : String.valueOf(this.postalCode);
    }

    public void setPostalCode(Long postalCode) {
        this.postalCode = postalCode;
    }

    public String getTownLabel() {
        return townLabel;
    }

    public void setTownLabel(String townLabel) {
        this.townLabel = townLabel;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public String getIdAddress() {
        return idAddress;
    }

    public void setIdAddress(String idAddress) {
        this.idAddress = idAddress;
    }

    @Override
    public String toString() {
        return "VHexavia3{" +
                "id=" + id +
                ", wayLabel='" + wayLabel + '\'' +
                ", postalCode=" + postalCode +
                ", idAddress='" + idAddress + '\'' +
                ", townLabel='" + townLabel + '\'' +
                ", info='" + info + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (!(o instanceof  VHexavia3))  {
            return false;
        }

        VHexavia3 o2 = (VHexavia3)o;

        //System.out.println("ibi");

        return this.getPostalCodeAsString().equals( o2.getPostalCodeAsString() )
                && this.getTownLabel().equals(o2.getTownLabel())
                && this.getWayLabel().equals(o2.getWayLabel());

    }

    @Override
    public int hashCode() {
        return Objects.hash(getPostalCodeAsString(), getTownLabel(), getWayLabel(), getInfo());
    }
}

