package fr.ofii.ref.jpa.td.entities;

import com.opengroup.pop.EntityBean;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name="direction_territorial_departement")
public class TerritorialDirectionDepartment implements Serializable, EntityBean {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name="id")
    private Long id;

    @Column(name="code_departement")
    private String departmentCode;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="id_direction_territorial")
    private TerritorialDirection territorialDirection;

    public TerritorialDirectionDepartment() {
    }

    public TerritorialDirectionDepartment(Long id, String departmentCode, TerritorialDirection territorialDirection) {
        this.id = id;
        this.departmentCode = departmentCode;
        this.territorialDirection = territorialDirection;
    }

    public TerritorialDirectionDepartment(Long id, String departmentCode, Long territorialDirectionId,
                                          String shortLabel, String longLabel, String phoneNumber,
                                          String faxNumber, String service, String firstNameDirector,
                                          String lastNameDirector,Long addressId, String city, String complement,
                                          String inseeCode, String streetName, String streetNumber, String zipCode, String signaturePath) {
        this.id = id;
        this.departmentCode = departmentCode;
        TerritorialDirectionAddress territorialDirectionAddress =  new TerritorialDirectionAddress(addressId, city, complement,
                                                                        inseeCode, streetName, streetNumber, zipCode);
        this.territorialDirection = new TerritorialDirection(territorialDirectionId,shortLabel,
                longLabel,phoneNumber,faxNumber,service,
                firstNameDirector,lastNameDirector,
                territorialDirectionAddress,signaturePath);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDepartmentCode() {
        return departmentCode;
    }

    public void setDepartmentCode(String departmentCode) {
        this.departmentCode = departmentCode;
    }

    public TerritorialDirection getTerritorialDirection() {
        return territorialDirection;
    }

    public void setTerritorialDirection(TerritorialDirection territorialDirection) {
        this.territorialDirection = territorialDirection;
    }

    @Override
    public String toString() {
        return "TerritorialDirectionDepartment{" +
                "id=" + id +
                ", departmentCode='" + departmentCode + '\'' +
                ", territorialDirection=" + territorialDirection +
                '}';
    }
}
