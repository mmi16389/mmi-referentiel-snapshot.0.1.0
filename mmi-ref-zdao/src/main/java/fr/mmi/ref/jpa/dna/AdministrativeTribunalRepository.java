package fr.mmi.ref.jpa.dna;

import fr.mmi.ref.jpa.dna.entities.AdministrativeTribunal;
import org.springframework.data.repository.CrudRepository;


public interface AdministrativeTribunalRepository extends CrudRepository<AdministrativeTribunal, Long> {


}
