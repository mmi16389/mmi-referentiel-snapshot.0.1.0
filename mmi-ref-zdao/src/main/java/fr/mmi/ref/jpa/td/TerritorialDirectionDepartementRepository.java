package fr.mmi.ref.jpa.td;

import fr.mmi.ref.jpa.td.entities.TerritorialDirection;
import fr.mmi.ref.jpa.td.entities.TerritorialDirectionDepartment;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface TerritorialDirectionDepartementRepository extends CrudRepository<TerritorialDirectionDepartment, Long> {

    @Query(value = "select new TerritorialDirectionDepartment(t.id,t.departmentCode,dt.id,dt.shortLabel," +
            " dt.longLabel, dt.phoneNumber,dt.faxNumber,dt.service, dt.firstNameDirector, dt.lastNameDirector, " +
            "ad.id, ad.city, ad.complement, ad.inseeCode, ad.streetName, ad.streetNumber, ad.zipCode, dt.signaturePath)  " +
            "from TerritorialDirectionDepartment t JOIN  t.territorialDirection dt JOIN  dt.address ad  ")
    List<TerritorialDirectionDepartment> getAllTerritorialDirectionDepartment();

}
