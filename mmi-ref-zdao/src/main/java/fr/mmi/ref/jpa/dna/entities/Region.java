package fr.mmi.ref.jpa.dna.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "Region")
public class Region implements Serializable, EntityBean {

    @Id
    @Column(name = "code_region", nullable = false, length = 6)
    private String regionCode;

    @Column(name = "libelle_region")
    private String regionName;

    public Region define(String regionCode, String regionName) {
        this.regionCode = regionCode;
        this.regionName = regionName;
        return this;
    }

    public String getRegionCode() {
        return regionCode;
    }

    public void setRegionCode(String regionCode) {
        this.regionCode = regionCode;
    }

    public String getRegionName() {
        return regionName;
    }

    public void setRegionName(String regionName) {
        this.regionName = regionName;
    }
}
