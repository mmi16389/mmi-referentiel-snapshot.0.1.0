package fr.mmi.ref.jpa.dna.entities;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by MMI on 05/09/2017.
 */
@Entity
@Table(name="Departement")
public class Department implements Serializable, EntityBean {

    @Id
    @Column(name="code_departement",nullable = false,length = 3)
    private String departmentCode;

    @Column(name="libelle_departement")
    private String departmentName;

    @ManyToOne
    @JoinColumn(name="code_region")
    private Region region;

    public Department(){
    }

    public Department(String departmentCode, String departmentName, Region region) {
        this.departmentCode = departmentCode;
        this.departmentName = departmentName;
        this.region = region;
    }

    public String getDepartmentCode() {
        return departmentCode;
    }

    public void setDepartmentCode(String departmentCode) {
        this.departmentCode = departmentCode;
    }

    public String getDepartmentName() {
        return departmentName;
    }

    public void setDepartmentName(String departmentName) {
        this.departmentName = departmentName;
    }

    public Region getRegion() {
        return region;
    }

    public void setRegion(Region region) {
        this.region = region;
    }
}
