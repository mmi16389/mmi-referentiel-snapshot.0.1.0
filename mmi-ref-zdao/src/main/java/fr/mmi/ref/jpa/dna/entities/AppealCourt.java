package fr.mmi.ref.jpa.dna.entities;
import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by MMI on 12/11/2017.
 */
@Entity
@Table(name = "cour_appel")
public class AppealCourt implements Serializable, EntityBean {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "libelle_court")
    private String shortName;

    @Column(name = "libelle_long")
    private String longName;

    public AppealCourt() {
    }

    public AppealCourt assignAll(Long id, String shortName, String longName) {
        this.id = id;
        this.shortName = shortName;
        this.longName = longName;
        return this;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public String getLongName() {
        return longName;
    }

    public void setLongName(String longName) {
        this.longName = longName;
    }

}
