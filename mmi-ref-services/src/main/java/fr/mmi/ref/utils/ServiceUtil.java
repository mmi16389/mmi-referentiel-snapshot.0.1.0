package fr.ofii.ref.utils;

import com.opengroup.pop.ErrorCode;
import com.opengroup.pop.LogMessages;
import com.opengroup.pop.exceptions.DomainException;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;

import java.util.Date;

/**
 * Utility class for Service
 */
public final class ServiceUtil {

    public static final String VALIDATE_NAME_PATTERN = "(?i)^[a-z]([a-z' -]*[a-z])?$";
    public static final String VALIDATE_ALPHANUMERIQUE_PATTERN = "^[a-zA-Z0-9]*$";
    public static final String VALIDATE_TELEPHONE_PATTERN = "^0[1-9][0-9]{8}$" ;
    public static final String VALIDATE_EMAIL_PATTERN =  "[^@]+@[^\\.]+\\..+" ;

    private ServiceUtil() {
        // empty
    }

    public static void assertNotNull(Object o, Logger logger, ErrorCode errorCode, Object... parameters) throws DomainException {
        if (o == null) {
            throw new DomainException(LogMessages.create(logger, null).add(errorCode, parameters ));
        }
    }

    public static void assertNotNull(Object o, Logger logger) throws DomainException {
        if (o == null) {
            throw new DomainException(LogMessages.create(logger, null));
        }
    }

    public static boolean isEmpty(Long o) {
        return (o == null || o == 0);
    }

    public static boolean isEmpty(String s) {
        return StringUtils.isEmpty(s);
    }


    public static String getValue(String oldValue, String newValue) {
        if (!StringUtils.isEmpty(newValue)) {
            return newValue;
        }
        return oldValue;
    }

    public static Date getValue(Date oldValue, Date newValue) {
        if (newValue!=null) {
            return newValue;
        }
        return oldValue;
    }

    public static Integer getValue(Integer oldValue, Integer newValue) {
        if (newValue!=null) {
            return newValue;
        }
        return oldValue;
    }

    public static String StringToInteger(Integer theValue) {
        return theValue == null ? null : String.valueOf(theValue);
    }
    public static Integer IntegerToString(String theValue) {
        return theValue == null ? null : Integer.valueOf(theValue);
    }

    /*public static void compareToStringPossibleNull(String s1, String s2, Logger LOGGER) throws DomainException {
        boolean b = s1 == null ? s2 == null : s1.equals(s2);
        if(!b || !s1.equalsIgnoreCase(s2) ){
            assertNotNull( null, LOGGER , DomainMethodControlCode.ID_USAGER_PORTAl_DNA);
        }
    }*/




}
