package fr.ofii.ref.utils;

import com.opengroup.pop.DomainBean;
import com.opengroup.pop.ErrorCode;
import org.apache.commons.lang3.StringUtils;

import java.util.Collection;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Extend the DomainBean base class
 */
public abstract class ExtendedDomainBean extends DomainBean {


    /**
     * Append errorCode to the internal logMessage if obj is null
     * @param obj obj to check
     * @param errorCode errorCode
     * @param parameters params
     * @return this
     */
    protected ExtendedDomainBean checkNotNull(Object obj, ErrorCode errorCode, Object... parameters)  {
        if (obj == null) {
            logMessages.add(errorCode, parameters);
        }
        return this;
    }

    protected ExtendedDomainBean checkIsNumber(String tel, ErrorCode errorCode, Object... parameters){
        if(!StringUtils.isNumeric(tel)){
            logMessages.add(errorCode, parameters);
        }
        return this;
    }

    protected ExtendedDomainBean checkIsEmailAdress(String email, ErrorCode errorCode, Object... parameters){
        Pattern p = Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,4}$");
        Matcher m = p.matcher(email.toUpperCase());
        if(!m.matches()){
            logMessages.add(errorCode, parameters);
        }
        return this;
    }

    protected ExtendedDomainBean checkListNotEmpty(Collection collection, ErrorCode errorCode, Object... parameters){
        if(collection == null || collection.size()==0){
            logMessages.add(errorCode, parameters);
        }
        return  this;
    }

    /**
     * Append errorCode to the internal logMessage if str is null or empty
     * @param str str to check
     * @param errorCode errorCode
     * @param parameters params
     * @return this
     */
    protected ExtendedDomainBean checkNotEmpty(String str, ErrorCode errorCode, Object... parameters) {
        if (str == null || "".equals(str)) {
            logMessages.add(errorCode, parameters);
        }
        return this;
    }

    /**
     * Append errorCode to the internal logMessage if obj0 is not EQUALS to obj1
     * @param obj0 first obj
     * @param obj1 second obj
     * @param errorCode errorCode
     * @param parameters params
     * @return this
     */
    @Deprecated
    protected ExtendedDomainBean checkEquals(Object obj0, Object obj1, ErrorCode errorCode, Object... parameters) {
        //
        if (obj0!=null && obj0.equals(obj1))  {
            logMessages.add(errorCode, parameters);
        }
        return this;
    }


    @Deprecated
    protected ExtendedDomainBean checkNotEquals(Object obj0, Object obj1, ErrorCode errorCode, Object... parameters) {
        //
        if (obj0!=null && !obj0.equals(obj1))  {
            logMessages.add(errorCode, parameters);
        }
        return this;
    }

    protected ExtendedDomainBean checkAlphaOnly(String str, ErrorCode errorCode, Object... parameters) {
        if (!StringUtils.isAlpha(str)) {
            logMessages.add(errorCode, parameters);
        }
        return this;
    }

}
