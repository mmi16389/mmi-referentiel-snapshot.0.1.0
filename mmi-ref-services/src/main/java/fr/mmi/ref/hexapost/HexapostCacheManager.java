package fr.mmi.ref.hexapost;

import com.hazelcast.core.HazelcastInstance;

import fr.mmi.ref.hexapost.domain.DomainHexapost;
import fr.mmi.ref.jpa.hexapost.entities.VHexavia3;

import java.util.List;

public interface HexapostCacheManager {


    void initialize(HazelcastInstance hazel, String info, List<DomainHexapost> allAddress)  throws DomainException;

    void initialize3(HazelcastInstance hazel, String info, List<VHexavia3> allAddress)  throws DomainException;


    List<DomainHexapost> find( String filter);

    List<String> findPostalCode(String filter);

    List<String> findPostalCommuneByPostalCode(String postalCode);

    List<DomainHexapost> postalWayFilter(String postalCodeAndCommuneFilter) throws DomainException;

    List<String> getListCodeInseeByZip(String zipCode) throws DomainException;

}
