//package fr.ofii.ref.domain.hexapost;
//
//import com.opengroup.pop.exceptions.DomainException;
//
//import java.io.Serializable;
//
///**
// * An Status Domain object
// *
// * misc hexapost values
// */
//public class DomainHexapostStatus implements Serializable {
//    //
//    private static final long serialVersionUID = 1L;
//
//    /** indic */
//    private final Integer indic;
//    /** majCode */
//    private final String majCode;
//    /** filler */
//    private final String filler;
//
//    /**
//     * full constructor
//     * @param indic indic
//     * @param majCode majCode
//     * @param filler filler
//     * @throws DomainException exception
//     */
//    private DomainHexapostStatus(Integer indic, String majCode, String filler) {
//        // domain validations
//
//        //
//        this.indic = indic;
//        this.majCode = majCode;
//        this.filler = filler;
//    }
//
//    /**
//     * full functional misc object builder
//     * @param indic indic
//     * @param majCode majCode
//     * @param filler filler
//     * @return status status
//     * @throws DomainException exception
//     */
//    public static DomainHexapostStatus newInstance(Integer indic, String majCode, String filler) {
//        return new DomainHexapostStatus( indic,  majCode, filler);
//    }
//
//    /**
//     * equals
//     * @param obj status object
//     * @return
//     */
//    @Override
//    public boolean equals(Object obj) {
//        if (this == obj) {
//            return true;
//        }
//        if (!(obj instanceof DomainHexapostStatus)) {
//            return false;
//        }
//
//
//        return indic.equals(  ((DomainHexapostStatus)obj).getIndic() )
//                && majCode.equals(  ((DomainHexapostStatus)obj).getMajCode())
//                && filler.equals(  ((DomainHexapostStatus)obj).getFiller());
//    }
//
//    /**
//     * hashcode
//     * @return integer
//     */
//    @Override
//    public int hashCode() {
//        return this.filler != null ? this.filler.hashCode() : 0;
//    }
//
//    /**
//     * return hexapost indicator
//     * @return integer
//     */
//    public Integer getIndic() {
//        return indic;
//    }
//
//    /**
//     * return maj status
//     * @return majCode
//     */
//    public String getMajCode() {
//        return majCode;
//    }
//
//    /**
//     * return filler
//     * @return filler
//     */
//    public String getFiller() {
//        return filler;
//    }
//
//}
