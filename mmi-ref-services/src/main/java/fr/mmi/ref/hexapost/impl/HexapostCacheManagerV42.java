package fr.ofii.ref.hexapost.impl;

import com.hazelcast.core.HazelcastInstance;
import com.opengroup.pop.exceptions.DomainException;
import fr.ofii.ref.hexapost.HexapostCacheManager;
import fr.ofii.ref.hexapost.domain.DomainHexapost;
import fr.ofii.ref.jpa.hexapost.entities.VHexavia3;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;


public class HexapostCacheManagerV42 implements HexapostCacheManager {

    private static final Logger LOGGER = LoggerFactory.getLogger(HexapostCacheManagerV42.class);
    private static final int RESULT_LIMIT = 50;

    // niv 0 => way label
    // niv 1 => postal code
    //
    private final TreeMap<String, TreeMap<String, List<DomainHexapost>>> cache = new TreeMap<>();

    private enum SHORT_NAMES {

        AVENUE("AV ", "AVENUE ", "AV", "AVENUE"),
        PLACE("PL ", "PLACE ", "PL", "PLACE"),
        MARECHAL("MAL ", "MARECHAL ", "MAL", "MARECHAL"),
        BLD("BLD ", "BOULEVARD ", "BLD", "BOULEVARD"),
        BD("BD ", "BOULEVARD ", "BD", "BOULEVARD"),
        ALL("ALL ", "ALLEE ", "ALL", "ALLEE"),
        AL("AL ", "ALLEE ", "AL", "ALLEE"),
        AR("AR ", "ARRONDISSEMENT ", "AR", "ARRONDISSEMENT"),
        LD("LD ", "LIEU-DIT", "LD", "LIEU-DIT"),
        IMP("IMP ", "IMPASSE ", "IMP", "IMPASSE"),
        RTE("RTE ", "ROUTE ", "RTE", "ROUTE"),
        RES("RES ", "RESIDENCE ", "RES", "RESIDENCE"),
        RPT("RPT ", "ROND-POINT ", "RPT", "ROND-POINT"),
        SQ("QS ", "SQUARE ", "SQ", "SQUARE"),
        PDT("PDT ", "PRESIDENT ", "PDT", "PRESIDENT"),
        //ARC("ARC ", "ARCADES ")
        ;

        private String option0;
        private String option1;
        private String option2;
        private String option3;




        SHORT_NAMES(String option0, String option1, String option2, String option3) {
            this.option0 = option0;
            this.option1 = option1;
            this.option2 = option2;
            this.option3 = option3;
        }
        boolean isSwapping0(String src, String what) {
            return ( (src.indexOf( option0 )>-1) && (what.indexOf( option1 )>-1) );
        }
        boolean isSwapping1(String src, String what) {
            return ( (src.indexOf( option1 )>-1) && (what.indexOf( option0 )>-1) );
        }
    }

    /** {@inheritDoc} */
    @Override
    public void initialize(HazelcastInstance hazel, String info, List<DomainHexapost> allAddresses) throws DomainException {

        long t0 = System.currentTimeMillis();
        //Set<String> postal = new TreeSet<>();

        allAddresses.forEach(address->{
            //
            String postalCode = address.getPostalCode();
            if (postalCode.length()==4) {
                postalCode = new StringBuilder("0").append(postalCode).toString();
            }
            //postal.add(postalCode);

            //
            String[] keys = address.getWayLabel().split(" ");
            for(String key : keys) {
                if (!cache.containsKey(key)) {
                    cache.put(key, new TreeMap<>() );
                }
                if (!cache.get(key).containsKey(postalCode)) {
                    cache.get(key).put(postalCode, new ArrayList<>());
                }
                cache.get(key).get(postalCode).add( address );
            }
        });

        //
        if (LOGGER.isInfoEnabled()) {
            LOGGER.info("  ----> cache in {} total addresses: {} ", (System.currentTimeMillis() - t0), cache.size());
        }

    }

    /** {@inheritDoc} */
    @Override
    public void initialize3(HazelcastInstance hazel, String info, List<VHexavia3> allAddresses) throws DomainException {
    }

    /** {@inheritDoc} */
    @Override
    public List<String> findPostalCode(String filter) {
        return null;
    }

    /** {@inheritDoc} */
    @Override
    public List<String> findPostalCommuneByPostalCode(String filter) {
        return null;
    }

    /** {@inheritDoc} */
    @Override
    public List<DomainHexapost> postalWayFilter(String postalCodeAndCommuneFilter) throws DomainException {
        return null;
    }

    /** {@inheritDoc} */
    @Override
    public List<String> getListCodeInseeByZip(String zipCode) throws DomainException {
        return null;
    }


    private boolean contains(String src, String what) {
        String src2 = src;
        String what2 = what;

        for(SHORT_NAMES cst : SHORT_NAMES.values()) {
            if (cst.isSwapping0( src2, what2)) {
                src2 = src2.replaceAll( cst.option0, cst.option1 );
                //what2 = what2.replace( what, cst.option1 );
            } else
            if (cst.isSwapping1( src2, what2)) {
                src2 = src2.replaceAll(cst.option1, cst.option0);
                //what2 = what2.replace( what, cst.option1 );
            }
        }

        return src2.contains(what2);
    }


    /**
     * search , first pass
     *
     * @param key
     * @param postal
     * @param commune
     * @param path
     * @param result
     */
    private void findAddress(String key, String postal, String commune, String path, Set<DomainHexapost> result) {
        NavigableMap<String, TreeMap<String, List<DomainHexapost>>> nm = cache.subMap(key, true, key+"|", true);
        for(Map.Entry<String, TreeMap<String, List<DomainHexapost>>> me : nm.entrySet()) {
            NavigableMap<String, List<DomainHexapost>> nmPostal = me.getValue().subMap(postal, true, postal+"|", true);
            for(Map.Entry<String, List<DomainHexapost>> mePostal : nmPostal.entrySet()) {

                for (DomainHexapost davh : mePostal.getValue()) {
                    if (( contains( davh.getWayLabel(), path) && (davh.getTownLabel().startsWith(commune)) )) {
                        result.add(davh);
                    }
                    if (result.size() > RESULT_LIMIT) {
                        break;
                    }
                }
                if (result.size() > RESULT_LIMIT) {
                    break;
                }
            }
            if (result.size() > RESULT_LIMIT) {
                break;
            }
        }
    }

    /**
     * strict search, second pass
     *
     * @param key
     * @param postal
     * @param commune
     * @param path
     * @param result
     */
    private void findAddress2(String key, String postal, String commune, String path, Set<DomainHexapost> result) {
        LOGGER.debug("findAddress2: {} {}", key, path );
        TreeMap<String, List<DomainHexapost>> nm = cache.get(key);

        if (nm==null) {
            LOGGER.debug("no values associates with key: {}", key);
            return;
        }

        NavigableMap<String, List<DomainHexapost>> nmPostal = nm.subMap(postal, true, postal+"|", true);
        for(Map.Entry<String, List<DomainHexapost>> mePostal : nmPostal.entrySet()) {

            for (DomainHexapost davh : mePostal.getValue()) {
                if (( contains( davh.getWayLabel(), path) && (davh.getTownLabel().startsWith(commune)) )) {
                    result.add(davh);
                }
                if (result.size() > RESULT_LIMIT) {
                    break;
                }
            }
            if (result.size() > RESULT_LIMIT) {
                break;
            }
        }
    }

    private String reformePath(String[] keys, String newKey) {
        StringBuilder sb = new StringBuilder();
        for(int i=0; i<keys.length - 1; i++) {
            if (sb.length()>0) {
                sb.append(" ");
            }
            sb.append( keys[i]);
        }
        if (sb.length()>0) {
            sb.append(" ");
        }
        sb.append(newKey);
        return sb.toString();
    }

    /** {@inheritDoc} */
    @Override
    public List<DomainHexapost> find( String filter) {
        long t0 = System.currentTimeMillis();
        Set<DomainHexapost> result = new HashSet<>();

        // poste 0 libelle voie
        // poste 1 code postal
        // poste 2 ville
        filter = filter.replaceAll("[']", " ");
        String[] p1 = filter.split("[,]");

        String path = (p1.length>0) ?  p1[0].trim() : "";
        String postal = (p1.length>1) ? p1[1].trim() : "";
        String commune = (p1.length>2) ? p1[2].trim() : "";

        if (path.length()+postal.length()+commune.length()==0) {
            return new ArrayList<>();
        }

        String[] keys = path.split(" ");

        //
        int deep = keys.length - 1;
        String key = keys[ deep ].trim();


        // first pass,
        findAddress( key, postal, commune, path, result);

        // process shorts names on last key
        for(SHORT_NAMES shortName : SHORT_NAMES.values()) {
            if (shortName.option3.startsWith(key)) {
                findAddress2(shortName.option2, postal, commune, reformePath(keys, shortName.option2), result);
            } else
            if (shortName.option2.startsWith(key) ) {
                findAddress2(shortName.option3, postal, commune, reformePath(keys, shortName.option3), result);
            }

        }


        //
        List<DomainHexapost> a = new ArrayList<>( result );
        a.sort((o1, o2) -> {
            int i0 = o1.getTownLabel().compareTo(o2.getTownLabel());
            return i0 == 0 ? o1.getWayLabel().compareTo(o2.getWayLabel()) : i0;
        });

        LOGGER.debug("in {} ms", (System.currentTimeMillis() - t0) );

        return a;
    }
}
