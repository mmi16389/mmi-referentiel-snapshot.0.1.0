//package fr.ofii.ref.domain.hexapost;
//
//import com.opengroup.pop.exceptions.DomainException;
//
//import java.io.Serializable;
//
///**
// * An City Domain object
// */
//public class DomainHexapostCommune implements Serializable {
//    //
//    private static final long serialVersionUID = 1L;
//
//    /** label */
//    private final String label;
//    /** insee code */
//    private final String inseeCode;
//    /** old insee code */
//    private final String oldInseeCode;
//    /** postal code */
//    private final String postalCode;
//    /** postal type */
//    private final String postalType;
//
//    /**
//     * full constructor
//     * @param label label
//     * @param inseeCode insee code
//     * @param oldInseeCode old insee code
//     * @param postalCode postal code
//     * @param postalType postal type
//     * @throws DomainException exception
//     */
//    private DomainHexapostCommune(String label, String inseeCode, String oldInseeCode, String postalCode, String postalType) {
//        // domain validations
//
//        //
//        this.label = label;
//        this.inseeCode = inseeCode;
//        this.postalType = postalType;
//        this.postalCode = postalCode;
//        this.oldInseeCode = oldInseeCode;
//    }
//
//    /**
//     * full functional city object builder
//     *
//     * @param label label
//     * @param inseeCode insee code
//     * @param oldInseeCode old insee code
//     * @param postalCode postal code
//     * @param postalType postal type
//     * @return city object
//     * @throws DomainException exception
//     */
//    public static DomainHexapostCommune newInstance(String label, String inseeCode, String oldInseeCode, String postalCode, String postalType)  {
//        return new DomainHexapostCommune(label, inseeCode, oldInseeCode, postalCode, postalType);
//    }
//
//    /**
//     * equal
//     * @param obj commune object
//     * @return boolean
//     */
//    @Override
//    public boolean equals(Object obj) {
//        if (this == obj) {
//            return true;
//        }
//        if (!(obj instanceof DomainHexapostCommune)) {
//            return false;
//        }
//
//        return label.equals( ((DomainHexapostCommune)obj).getLabel() )
//            && inseeCode.equals( ((DomainHexapostCommune)obj).getInseeCode())
//            && oldInseeCode.equals( ((DomainHexapostCommune)obj).getOldInseeCode())
//            && postalCode.equals( ((DomainHexapostCommune)obj).getPostalCode())
//            && postalType.equals( ((DomainHexapostCommune)obj).getPostalType());
//    }
//
//    /**
//     * hashcode
//     * @return integer
//     */
//    @Override
//    public int hashCode() {
//        return this.label != null ? this.label.hashCode() : 0;
//    }
//
//    /**
//     * return label
//     * @return label
//     */
//    public String getLabel() { return label; }
//
//    /**
//     * return insee code
//     * @return insee code
//     */
//    public String getInseeCode() { return inseeCode; }
//
//    /**
//     * return old insee code
//     * @return old insee code
//     */
//    public String getOldInseeCode() { return oldInseeCode; }
//
//    /**
//     * return postal code
//     * @return postal code
//     */
//    public String getPostalCode() { return postalCode; }
//
//    /**
//     * return postal type
//     * @return postal type
//     */
//    public String getPostalType() { return postalType; }
//
//    @Override
//    public String toString() {
//        return "HexapostCommune{" +
//                "label='" + label + '\'' +
//                ", inseeCode='" + inseeCode + '\'' +
//                ", oldInseeCode='" + oldInseeCode + '\'' +
//                ", postalCode='" + postalCode + '\'' +
//                ", postalType='" + postalType + '\'' +
//                '}';
//    }
//}
