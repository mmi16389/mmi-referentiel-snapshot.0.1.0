package fr.ofii.ref.hexapost.impl;

import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.MultiMap;
import com.hazelcast.core.TransactionalMap;
import com.hazelcast.core.TransactionalMultiMap;
import com.hazelcast.transaction.TransactionContext;
import com.opengroup.pop.exceptions.DomainException;
import fr.ofii.ref.CacheNames;
import fr.ofii.ref.hexapost.HexapostCacheManager;
import fr.ofii.ref.hexapost.domain.DomainHexapost;
import fr.ofii.ref.jpa.hexapost.entities.VHexavia3;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;
import java.util.stream.Collectors;


public class HexapostCacheManagerV45 implements HexapostCacheManager {
// se charge

    private static final Logger LOGGER = LoggerFactory.getLogger(HexapostCacheManagerV45.class);
    private static final int RESULT_LIMIT = 10;

    private enum SHORT_NAMES {

        AVENUE("AV ", "AVENUE "),
        PLACE("PL ", "PLACE "),
        MARECHAL("MAL ", "MARECHAL "),
        BLD("BLD ", "BOULEVARD "),
        BD("BD ", "BOULEVARD "),
        ALL("ALL ", "ALLEE "),
        AL("AL ", "ALLEE "),
        AR("AR ", "ARRONDISSEMENT "),
        LD("LD ", "LIEU-DIT "),
        IMP("IMP ", "IMPASSE "),
        RTE("RTE ", "ROUTE "),
        RES("RES ", "RESIDENCE "),
        RPT("RPT ", "ROND-POINT "),
        SQ("QS ", "SQUARE "),
        PDT("PDT ", "PRESIDENT "),
        //ARC("ARC ", "ARCADES ")
        ;

        private String option0;
        private String option1;




        SHORT_NAMES(String option0, String option1) {
            this.option0 = option0;
            this.option1 = option1;
        }
    }

    private HazelcastInstance hazel;

    /** address by zipcode and town label */
    private MultiMap<String, VHexavia3> adrMap3;
    /** zipCode */
    private TreeSet<String> postalCode;
    /** town label by zipcode */
    private Map<String, Set<String>> town;

    /** {@inheritDoc} */
    @Override
    public void initialize(HazelcastInstance hazel, String info, List<DomainHexapost> allAddresses) throws DomainException {

    }

    /** {@inheritDoc} */
    @Override
    public void initialize3(HazelcastInstance hazel, String info, List<VHexavia3> allAddresses) throws DomainException {

        long t0 = System.currentTimeMillis();

        synchronized (this) {
            if (this.hazel == null) {
                LOGGER.info("### hazel set V3");
                this.hazel = hazel;
                //this.adrMap3 = hazel.getMultiMap(CacheNames.ADDRESS.getDesignation());
                this.postalCode = new TreeSet<>();
                this.town = new HashMap<>();
            }
        }


        adrMap3 = hazel.getMultiMap(CacheNames.ADDRESS.getDesignation());

        LOGGER.info("depart: V3 {} ", adrMap3.size() );
        Set<String> doublons = new TreeSet<>();
        int[] ct = {0, 0};

        allAddresses.forEach(adr->{
            //
            String k = new StringBuilder(adr.getPostalCodeAsString())
                    .append(",")
                    .append(adr.getTownLabel())
                    .toString();

            //
            postalCode.add( adr.getPostalCodeAsString() );
            if (!town.containsKey(adr.getPostalCodeAsString())) {
                town.put(adr.getPostalCodeAsString(), new HashSet<>());
            }
            town.get(adr.getPostalCodeAsString()).add( adr.getTownLabel() );

//            if (adrMap3.get(k).contains( adr )) {
//                StringBuilder k2 = new StringBuilder(adr.getPostalCodeAsString())
//                        .append(",")
//                        .append(adr.getTownLabel());
//                if (adr.getInfo()!=null) {
//                    k2.append(",").append(adr.getInfo());
//                }
//                k2.append(",").append( adr.getWayLabel() );
//                doublons.add( k2.toString() );
//                ct[1]++;
//                //LOGGER.info("### doublons {} ", k2);
//            }

            //
            adrMap3.put(k, adr);
            ct[0]++;




        });

        //

        //
        //LOGGER.info(adrMap.getLocalMapStats().toString().replaceAll(", ", "\n   "));

        LOGGER.info("---- doublons V3 ");
        doublons.stream()
                .limit(10)
                .forEach( e->{
                    LOGGER.info("## {}", e);
                });
        LOGGER.info(" V3 --> {} key doublons",  doublons.size(), ct[1]);
        LOGGER.info("  ----> V45 {} cache in {} total_traitee:{} cache:{} postal_code:{}", info, (System.currentTimeMillis() - t0), allAddresses.size(), adrMap3.size(), postalCode.size());
    }



    /** {@inheritDoc} */
    @Override
    public List<DomainHexapost> find( String filter) {
        return null;
    }

    /** {@inheritDoc} */
    @Override
    public List<String> findPostalCode(String filter) {

        return postalCode
                .subSet(filter, true, filter+"|", true)
                .stream()
                .limit(RESULT_LIMIT)
                .collect(Collectors.toList());

    }

    /** {@inheritDoc} */
    @Override
    public List<String> findPostalCommuneByPostalCode(String filter) {

        String[] f = filter.split("[,]");

        return town
                .get( f[0] )
                .stream()
                .filter( x-> (f.length == 1) || x.startsWith(f[1] ))
                .collect(Collectors.toList());
    }

    /** {@inheritDoc} */
    @Override
    public List<DomainHexapost> postalWayFilter(String postalCodeAndCommuneFilter) throws DomainException {

        String[] f = postalCodeAndCommuneFilter.split("[,]");

        return adrMap3
                .get( new StringBuilder(f[1]).append(",").append(f[2]).toString() )
                .stream()
                .filter(x->x.getWayLabel().startsWith(f[0]))
                .map( x-> DomainHexapost.create()
                        .assignAddress(x.getIdAddress(), x.getWayLabel(), x.getPostalCodeAsString(), x.getTownLabel())
                        .assignDivers(x.getInfo()) )
                .collect(Collectors.toList());

    }

    /** {@inheritDoc} */
    @Override
    public List<String> getListCodeInseeByZip(String zipCode) throws DomainException {
        return town
                .get( zipCode )
                .stream()
                .collect(Collectors.toList());
    }


}
