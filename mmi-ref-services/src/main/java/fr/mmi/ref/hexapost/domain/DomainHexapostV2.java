//package fr.ofii.ref.domain.hexapost;
//
//import com.opengroup.pop.DomainBean;
//
//import javax.persistence.Column;
//import java.io.Serializable;
//
//public class DomainHexapostV2  extends DomainBean implements Serializable {
//    //
//    private static final long serialVersionUID = 1L;
//
//    /** id */
//    private Long id;
//
//    private String idAddress;
//
//    private String wayLabel;
//
//    private String postalCode;
//
//    private String townLabel;
//
//    private DomainHexapostV2() {
//    }
//
//    public static DomainHexapostV2 create() {
//        return new DomainHexapostV2();
//    }
//
//    public DomainHexapostV2 assign(Long id) {
//        this.id = id;
//        return this;
//    }
//
//    public DomainHexapostV2 assignAddress(String idAddress, String wayLabel, String postalCode, String townLabel) {
//        this.idAddress = idAddress;
//        this.wayLabel = wayLabel;
//        this.postalCode = postalCode;
//        this.townLabel = townLabel;
//        return this;
//    }
//
//
//    public Long getId() {
//        return id;
//    }
//
//    public String getIdAddress() {
//        return idAddress;
//    }
//
//    public String getWayLabel() {
//        return wayLabel;
//    }
//
//    public String getPostalCode() {
//        if (postalCode.length()==4) {
//            return "0"+postalCode;
//        }
//        return postalCode;
//    }
//
//    public String getTownLabel() {
//        return townLabel;
//    }
//
//    @Override
//    public boolean equals(Object obj) {
//        if (this == obj) {
//            return true;
//        }
//        if (!(obj instanceof DomainHexapostV2)) {
//            return false;
//        }
//
//        DomainHexapostV2 obj2 = (DomainHexapostV2)obj;
//
//        return id.equals(  obj2.getId() )
//                && wayLabel!=null && wayLabel.equals(  obj2.getWayLabel())
//                && postalCode!=null && postalCode.equals( obj2.getPostalCode())
//                && townLabel!=null && townLabel.equals( obj2.getTownLabel());
//    }
//
//}
