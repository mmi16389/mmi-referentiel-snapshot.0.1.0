//package fr.ofii.ref.domain.hexapost;
//
//import com.opengroup.pop.DomainBean;
//import com.opengroup.pop.exceptions.DomainException;
//
//import java.io.Serializable;
//
///**
// * An Address Domain object
// */
//public class DomainHexapost extends DomainBean implements Serializable {
//    //
//    private static final long serialVersionUID = 1L;
//
//    /** id */
//    private final String id;
//    /** row5Label */
//    private final String row5Label;
//    /** wayLabel */
//    private final String wayLabel;
//    /** commune */
//    private final DomainHexapostCommune commune;
//    /** status */
//    private final DomainHexapostStatus status;
//
//
//    /**
//     * full constructor
//     * @param id id
//     * @param row5Label row5Label
//     * @param wayLabel way label i.e number, kind of way, way label
//     * @param commune city object
//     * @param status misc value object
//     * @throws DomainException exception
//     */
//    private DomainHexapost(String id, String row5Label, String wayLabel, DomainHexapostCommune commune, DomainHexapostStatus status) {
//        super();
//
//        // domain validations
//
//        //
//        this.id = id;
//        this.row5Label = row5Label;
//        this.wayLabel = wayLabel;
//        this.commune = commune;
//        this.status = status;
//    }
//
//    /**
//     * full functional hexapost object builder
//     *
//     * @param id id
//     * @param row5Label row5Label
//     * @param wayLabel way label i.e number, kind of way, way label
//     * @param commune city object
//     * @param status misc value object
//     * @return functional hexapost
//     * @throws DomainException exception
//     */
//    public static DomainHexapost newInstance(String id, String row5Label, String wayLabel, DomainHexapostCommune commune, DomainHexapostStatus status) {
//        return new DomainHexapost(id, row5Label, wayLabel, commune, status);
//    }
//
//    /**
//     * equals
//     * @param obj domain object
//     * @return boolean
//     */
//    @Override
//    public boolean equals(Object obj) {
//        if (this == obj) {
//            return true;
//        }
//        if (!(obj instanceof DomainHexapost)) {
//            return false;
//        }
//
//        DomainHexapost obj2 = (DomainHexapost)obj;
//
//        return id.equals(  obj2.getId() )
//               && row5Label.equals(  obj2.getRow5Label())
//               && wayLabel.equals(  obj2.getWayLabel())
//               && ((commune != null) && commune.equals(  obj2.getCommune()))
//               && ((status != null) && status.equals(  obj2.getStatus()));
//    }
//
//    /**
//     * hashCode
//     * @return integer
//     */
//    @Override
//    public int hashCode() {
//        return this.id  != null ? this.id.hashCode() : 0;
//    }
//
//    /**
//     * return id
//     * @return id
//     */
//    public String getId() {
//        return id;
//    }
//
//    /**
//     * return row5Label
//     * @return label
//     */
//    public String getRow5Label() {
//        return row5Label;
//    }
//
//    /**
//     * return way label
//     * @return way label
//     */
//    public String getWayLabel() {
//        return wayLabel;
//    }
//
//    @Override
//    public String toString() {
//        return "DomainHexapost{" +
//                "id='" + id + '\'' +
//                ", row5Label='" + row5Label + '\'' +
//                ", wayLabel='" + wayLabel + '\'' +
//                ", commune=" + commune +
//                ", status=" + status +
//                '}';
//    }
//
//    /**
//     * return city object
//     * @return city
//     */
//    public DomainHexapostCommune getCommune() { return commune; }
//
//    /**
//     * return misc value object
//     * @return object
//     */
//    public DomainHexapostStatus getStatus() { return status; }
//
//}

package fr.ofii.ref.hexapost.domain;

import com.opengroup.pop.DomainBean;

import java.io.Serializable;

public class DomainHexapost  extends DomainBean implements Serializable, Comparable<DomainHexapost> {
    //
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    private String idAddress;

    private String wayLabel;

    private String postalCode;

    private String townLabel;

    private String info;

    private String filler;

    private DomainHexapost() {
    }

    public static DomainHexapost create() {
        return new DomainHexapost();
    }

    public DomainHexapost assign(Long id) {
        this.id = id;
        return this;
    }

    public DomainHexapost assignAddress(String idAddress, String wayLabel, String postalCode, String townLabel) {
        this.idAddress = idAddress;
        this.wayLabel = wayLabel;
        this.postalCode = postalCode;
        this.townLabel = townLabel;
        return this;
    }

    public DomainHexapost assignAddressAndValidate(String idAddress, String wayLabel, String postalCode, String townLabel) {
        this.idAddress = idAddress;
        this.wayLabel = wayLabel;
        this.postalCode = postalCode;
        this.townLabel = townLabel;
        return this;
    }

    public DomainHexapost assignDivers(String info) {
        this.info = info;
        return this;
    }

    public DomainHexapost assignFiller(String filler) {
        this.filler = filler;
        return this;
    }

    @Override
    public int compareTo(DomainHexapost o) {
        int a = getPostalCode().compareTo( o.getPostalCode() );
        int b = getTownLabel().compareTo( o.getTownLabel() );
        int c = getWayLabel().compareTo(o.getWayLabel());
        return a == 0 ? (b==0 ? c : b) : a;
    }



    public Long getId() {
        return id;
    }

    public String getIdAddress() {
        return idAddress;
    }

    public String getWayLabel() {
        return wayLabel;
    }

    public String getPostalCode() {
        if (postalCode.length()==4) {
            return new StringBuilder("0").append( postalCode ).toString();
        }
        return postalCode;
    }

    public String getTownLabel() {
        return townLabel;
    }

    public String getInfo() { return info; }

    public String getFiller() { return  filler; }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof DomainHexapost)) {
            return false;
        }

        DomainHexapost obj2 = (DomainHexapost)obj;

        return id.equals(  obj2.getId() )
                && wayLabel!=null && wayLabel.equals(  obj2.getWayLabel())
                && postalCode!=null && postalCode.equals( obj2.getPostalCode())
                && townLabel!=null && townLabel.equals( obj2.getTownLabel());
    }

}

