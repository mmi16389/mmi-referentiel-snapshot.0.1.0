package fr.ofii.ref.hexapost.impl;

import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.MultiMap;
import com.opengroup.pop.exceptions.DomainException;
import fr.ofii.ref.CacheNames;
import fr.ofii.ref.hexapost.HexapostCacheManager;
import fr.ofii.ref.hexapost.domain.DomainHexapost;
import fr.ofii.ref.jpa.hexapost.entities.VHexavia3;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


public class HexapostCacheManagerV44 implements HexapostCacheManager {

    private static final Logger LOGGER = LoggerFactory.getLogger(HexapostCacheManagerV44.class);
    private static final int RESULT_LIMIT = 10;


    private enum SHORT_NAMES {

        AVENUE("AV ", "AVENUE "),
        PLACE("PL ", "PLACE "),
        MARECHAL("MAL ", "MARECHAL "),
        BLD("BLD ", "BOULEVARD "),
        BD("BD ", "BOULEVARD "),
        ALL("ALL ", "ALLEE "),
        AL("AL ", "ALLEE "),
        AR("AR ", "ARRONDISSEMENT "),
        LD("LD ", "LIEU-DIT "),
        IMP("IMP ", "IMPASSE "),
        RTE("RTE ", "ROUTE "),
        RES("RES ", "RESIDENCE "),
        RPT("RPT ", "ROND-POINT "),
        SQ("QS ", "SQUARE "),
        PDT("PDT ", "PRESIDENT "),
        //ARC("ARC ", "ARCADES ")
        ;

        private String option0;
        private String option1;




        SHORT_NAMES(String option0, String option1) {
            this.option0 = option0;
            this.option1 = option1;
        }
    }

    private HazelcastInstance hazel;

    //private IMap<String, DomainHexapost> adrMap;
    //private IMap<String, String> postalCodeMap;

    private MultiMap<String, VHexavia3> adrMap3;

    /** {@inheritDoc} */
    @Override
    public void initialize(HazelcastInstance hazel, String info, List<DomainHexapost> allAddresses) throws DomainException {

    }

    /** {@inheritDoc} */
    @Override
    public void initialize3(HazelcastInstance hazel, String info, List<VHexavia3> allAddresses) throws DomainException {

        long t0 = System.currentTimeMillis();

        synchronized (this) {
            if (this.hazel == null) {
                LOGGER.info("### hazel set V3");
                this.hazel = hazel;
                this.adrMap3 = hazel.getMultiMap(CacheNames.ADDRESS.getDesignation());
                //this.adrMap3.addIndex("postalCode", false);
                //this.adrMap3.addIndex("townLabel", false);
                //this.postalCodeMap = hazel.getMap(CODE_CACHE);
            }
        }

        LOGGER.info("depart: V3 " + adrMap3.size() );
        Map<String, List<String>> doublons = new HashMap<>();
        int[] ct = new int[2];

        allAddresses.forEach(adr->{
            String k = new StringBuilder(adr.getPostalCodeAsString())
                    .append(",")
                    .append(adr.getTownLabel())
                    .toString();
            //if (adr.getInfo()!=null) {
            //    k += " "+adr.getInfo();
            //}

            // gestion des doublons
            StringBuilder k2 = new StringBuilder(adr.getWayLabel())
                    .append(" ")
                    .append(adr.getPostalCodeAsString())
                    .append(" ")
                    .append(adr.getTownLabel());
            if (adr.getInfo()!=null) {
                k2.append(" ").append(adr.getInfo());
            }

            //
            adrMap3.put(k, adr);

            //} else {
            //    //
            //    if (!doublons.containsKey(k)) {
            //        doublons.put(k, new ArrayList<>());
            //    }
            //    doublons.get(k).add( "("+adr.getId()+") "+adr.getWayLabel()+","+adr.getPostalCode()+","+adr.getTownLabel()+","+adr.getIdAddress());
            //    ct[0]++;
//
  //          }


            //
            //String k2 = adr.getPostalCodeAsString()+","+adr.getTownLabel();
            //if (!postalCodeMap.containsKey(k2)) {
            //    postalCodeMap.put( k2, k2 );
            //}
        });

        //
        //LOGGER.info(adrMap.getLocalMapStats().toString().replaceAll(", ", "\n   "));

        LOGGER.info("---- doublons V3 ");
        doublons.entrySet().stream().limit(10)
                .forEach( e->{
                    LOGGER.info("" + e.getKey());
                    e.getValue().stream().forEach(s -> LOGGER.info("   " + s) );
                });
        LOGGER.info(" V3 --> {} key doublons  total:",  doublons.size(), ct[0]);
        LOGGER.info("  ----> V44 {} cache in {} total_traitee:{} cache:{} postal_code:{}", info, (System.currentTimeMillis() - t0), allAddresses.size(), adrMap3.size());
    }




    private String reduceLast(String[] p2) {
        StringBuilder sb = new StringBuilder();
        for(int i = 0; i<p2.length - 1; i++ ) {
            if (sb.length()>0) {
                sb.append(" ");
            }
            sb.append( p2[i] );
        }
        return sb.toString();
    }


    /** {@inheritDoc} */
    @Override
    public List<DomainHexapost> find( String filter) {
//        long t0 = System.currentTimeMillis();
//
//        String[] p1 = filter.split("[,]");
//        String way = (p1.length>0) ?  p1[0].trim() : "";
//        String postal = (p1.length>1) ? p1[1].trim() : "";
//        String town = (p1.length>2) ? p1[2].trim() : "";
//
//        //
//        Predicate<Long, DomainHexapost> sql = new SqlPredicate("__key like '%"+way+"%'");
//
//        // pas bon pr MAL
//        for(SHORT_NAMES shortNames : SHORT_NAMES.values()) {
//            if (way.startsWith( shortNames.option0 )) {
//                sql = Predicates.or( sql, new SqlPredicate("__key like '%"+way.replace(shortNames.option0,  shortNames.option1)+"%'") );
//            } else
//            if (way.startsWith( shortNames.option1 )) {
//                sql = Predicates.or(sql, new SqlPredicate("__key like '%" + way.replace(shortNames.option1, shortNames.option0) + "%'"));
//            }
//        }
//
//        //
//        if (way.indexOf(" ")>-1) {
//            String[] p2 = way.split(" ");
//            String wayWithoutLastWord = reduceLast( p2 );
//            town = p2[ p2.length - 1];
//            sql = Predicates.or( sql, new SqlPredicate("__key like '%"+wayWithoutLastWord+"%'") );
//            for(SHORT_NAMES shortNames : SHORT_NAMES.values()) {
//                if (way.startsWith( shortNames.option0 )) {
//                    sql = Predicates.or( sql, new SqlPredicate("__key like '%"+wayWithoutLastWord.replace(shortNames.option0,  shortNames.option1)+"%'") );
//                } else
//                if (way.startsWith( shortNames.option1 )) {
//                    sql = Predicates.or(sql, new SqlPredicate("__key like '%" + wayWithoutLastWord.replace(shortNames.option1, shortNames.option0) + "%'"));
//                }
//            }
//        }
//
//        //
//        if (postal.length()>0) {
//            Predicate<Long, DomainHexapost> sql1 = new SqlPredicate("__key like '%" + postal + "%'");
//            sql = Predicates.and(sql, sql1);
//        }
//
//        //
//        if (town.length()>0) {
//            Predicate<Long, DomainHexapost> sql1 = new SqlPredicate("__key like '%" + town + "%'");
//            sql = Predicates.and(sql, sql1);
//        }
//
//
//
//        List<DomainHexapost> l = adrMap3.values( new PagingPredicate<>( sql, RESULT_LIMIT ) ).stream()
//                .map( x-> DomainHexapost.create()
//                        .assignAddress(x.getIdAddress(), x.getWayLabel(), x.getPostalCodeAsString(), x.getTownLabel())
//                        .assignDivers(x.getInfo())
//                ).collect(Collectors.toList());
//
//        LOGGER.debug(sql.toString()+ " in " + (System.currentTimeMillis() - t0)+" ms " );
//
//        return l;
        return null;
    }

    /** {@inheritDoc} */
    @Override
    public List<String> findPostalCode(String filter) {
        return new ArrayList<>(adrMap3
                .keySet()
                .stream()
                .filter(x->x.startsWith(filter) )
                .map(x->x.split(",")[0])
                .limit(RESULT_LIMIT)
                .collect(Collectors.toSet()) )
                ;
    }

    /** {@inheritDoc} */
    @Override
    public List<String> findPostalCommuneByPostalCode(String filter) {

        return adrMap3
                .keySet()
                .stream()
                .filter(x->x.startsWith(filter) )
                .map(x->x.split(",")[1])
                .limit(RESULT_LIMIT)
                .collect(Collectors.toList());
    }

    /** {@inheritDoc} */
    @Override
    public List<DomainHexapost> postalWayFilter(String postalCodeAndCommuneFilter) throws DomainException {

        String[] f = postalCodeAndCommuneFilter.split("[,]");


        return adrMap3.get(f[1]+','+f[2])
                .stream()
                .filter(x->x.getWayLabel().startsWith(f[0]))
                .map( x-> DomainHexapost.create()
                        .assignAddress(x.getIdAddress(), x.getWayLabel(), x.getPostalCodeAsString(), x.getTownLabel())
                        .assignDivers(x.getInfo()) )
                .collect(Collectors.toList());

    }

    /** {@inheritDoc} */
    @Override
    public List<String> getListCodeInseeByZip(String zipCode) throws DomainException {
        return null;
    }


}
