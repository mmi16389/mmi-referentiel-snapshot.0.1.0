package fr.mmi.ref.hexapost;

import fr.mmi.ref.hexapost.domain.DomainHexapost;

import java.util.List;

/**
 * Domain Address service
 */
public interface HexapostReferentialServices {

    /**
     * return all secondary keys "postalCode commune" whose match postalCommuneFilter
     *
     * @param postalCode postalCode or commune to search
     * @return a collection of second level keys
     * @throws DomainException domainException
     */
    List<String> postalCommuneFilterByPostalCode(String postalCode) throws DomainException;

    List<String> postalCodeFilter(String postalCodeFilter) throws DomainException;

    List<DomainHexapost> postalWayFilter(String postalCodeAndCommuneFilter) throws DomainException;

    /**
     * return all hexapost whose match communeCodeFilter (give by the postalCommuneFilter service) and wayFilter
     *
     * @param wayFilter filter
     * @return a collection of hexapost
     * @throws DomainException domainException
     */
    List<DomainHexapost> wayFilter2(String wayFilter) throws DomainException;


    //DomainAddress2 addressFinder(DomainAddress2 criteria) throws DomainException;

    List<String> getListCodeInseeByZip(String zipCode) throws DomainException;
}
