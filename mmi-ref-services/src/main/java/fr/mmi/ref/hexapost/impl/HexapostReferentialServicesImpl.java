package fr.ofii.ref.hexapost.impl;

import com.hazelcast.core.HazelcastInstance;
import com.opengroup.pop.exceptions.DomainException;
import fr.ofii.ref.hexapost.HexapostCacheManager;
import fr.ofii.ref.hexapost.HexapostReferentialServices;
import fr.ofii.ref.hexapost.domain.DomainHexapost;
import fr.ofii.ref.hexapost.impl.mappers.DomainHexapostMapper;
import fr.ofii.ref.jpa.hexapost.HexapostRepository;
import fr.ofii.ref.jpa.hexapost.entities.VHexavia3;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static java.lang.System.nanoTime;

/**
 * Implementation to manage Parameter service
 *
 * @author Open groupe
 * @since 1.0.0
 */
@Service
public class HexapostReferentialServicesImpl implements HexapostReferentialServices {


    /** logger */
    private static final Logger LOGGER = LoggerFactory.getLogger(HexapostReferentialServicesImpl.class);

    /** hazelcast instance */
    @Autowired
    private HazelcastInstance hazelcast; // = Hazelcast.newHazelcastInstance();


    private HexapostCacheManager hexapostCacheManager = new HexapostCacheManagerV45();

    /** addressVHexavia repository */
    @Autowired
    private HexapostRepository hexapostRepository;

    /** mapper */
    @Autowired
    private DomainHexapostMapper hexapostMapper;

    @Value("${address.limited:}")
    private String addressLimited;


    private class Range {
        private long[] r = {0l, 0l};
        public Range valueOf(String s) {
            String[] ss = s.split("[,]");
            r[0] = Long.valueOf(ss[0]);
            r[1] = Long.valueOf(ss[1]);
            return this;
        }
        public long getInf() { return r[0]; }
        public long getSup() { return r[1]; }
    }



    /** {@inheritDoc} */
    @Override
    public List<String> postalCommuneFilterByPostalCode(String postalCode) throws DomainException {
        return hexapostCacheManager.findPostalCommuneByPostalCode(postalCode);

    }

    @Override
    public List<DomainHexapost> postalWayFilter(String postalCodeAndCommuneFilter) throws DomainException {
        return hexapostCacheManager.postalWayFilter(postalCodeAndCommuneFilter);
    }


    /** {@inheritDoc} */
    @Override
    public List<String> postalCodeFilter(String postalCodeFilter) throws DomainException {
        return hexapostCacheManager.findPostalCode(postalCodeFilter);
    }

    /** {@inheritDoc} */
    @Override
    public List<DomainHexapost> wayFilter2(String wayFilter) throws DomainException {

        return hexapostCacheManager.find( wayFilter);

    }

    /** {@inheritDoc} */
    @Override
    public List<String> getListCodeInseeByZip(String zipCode) throws DomainException {
        return hexapostCacheManager.getListCodeInseeByZip( zipCode );
    }

    private void doGenerate(int queryOrder, long[] queryTotalSize, long b0, long b1 ) throws DomainException {
        long start11 = nanoTime();
        List<VHexavia3> q0 = hexapostRepository.getAddressesFromTo3(b0, b1);

        LOGGER.info("query q{} ok", queryOrder);
        hexapostCacheManager.initialize3(hazelcast, String.format("q%d", queryOrder), q0);
        queryTotalSize[0] += q0.size();

        LOGGER.info("  ----> q{} in {} traitées:{} cache:{} ", queryOrder, TimeUnit.NANOSECONDS.toMillis(nanoTime() - start11), q0.size(), hazelcast.getMultiMap("address").size());
    }

    /**
     * Generate cache at the application launch
     *
     * @throws Exception exception
     */
    @PostConstruct
    public void generateCache() throws DomainException {

        //hazelcast.shutdown();
        //hazelcast = Hazelcast.newHazelcastInstance();

        //PartitionService partitionService = hazelcast.getPartitionService();
        //if (partitionService.isClusterSafe()) {

        long[] queryTotalSize = { 0 };
        long start11 = nanoTime();

        if (addressLimited.length()>0) {
            Range r = new Range().valueOf(addressLimited);
            doGenerate(0, queryTotalSize, r.getInf(), r.getSup());
            LOGGER.warn("Loaded address is limited");
        } else {
            doGenerate(0, queryTotalSize, 0L, 50000L);
            doGenerate(1, queryTotalSize, 50000L, 100000L);
        }


        LOGGER.info("##### TOTAL {} cache:{} load:{} in {} ",
                hexapostRepository.count(),
                hazelcast.getMultiMap("address").size(),
                (queryTotalSize[0]),
                TimeUnit.NANOSECONDS.toMillis(nanoTime() - start11)
        );


    }



}