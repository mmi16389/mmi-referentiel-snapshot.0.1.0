//package fr.ofii.ref.impl;
//
//import com.opengroup.pop.exceptions.DomainException;
//import fr.ofii.ref.hexapost.HexapostCacheManager;
//import fr.ofii.ref.domain.hexapost.DomainVHexavia;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//
//import java.util.*;
//
//
//public class HexapostCacheManagerV41 implements HexapostCacheManager {
//
//    private static final Logger LOGGER = LoggerFactory.getLogger(HexapostCacheManagerV41.class);
//
//    private int idents = 0;
//
//    class Node {
//        final int id;
//        final String key;
//        Integer stat;
//        final List<DomainVHexavia> items;  // seul le niv le plus profond aura des Rue
//        Node(String key) {
//            this.id = idents++;
//            this.key = key;
//            this.stat = 0;
//            this.items = new ArrayList<>();
//        }
//    }
//
//    private final TreeMap<String, Node> cache = new TreeMap<>();
//
//
//    // avec  gestion de certaines abreviations
//    //
//    @Override
//    public void generate(List<DomainVHexavia> allAddresses) throws DomainException {
//
//        long t0 = System.currentTimeMillis();
//        //
//        allAddresses.forEach(address->{
//
//            String[] keys = address.getWay2Label().split(" ");
//
//            for(String key : keys) {
//                if (!cache.containsKey(key)) {
//                    cache.put(key, new Node(key));
//                }
//                cache.get(key).stat++;
//                cache.get(key).items.add( address );
//            }
//        });
//
//        //
//        if (LOGGER.isInfoEnabled()) {
//            LOGGER.info(String.format("  ----> cache in %d total addresses: %d ", (System.currentTimeMillis() - t0), cache.size()));
//        }
//
//        List<DomainVHexavia> l = find(  ",28300,");
//        System.out.println("total:" + l.size());
//        for(DomainVHexavia davh : l) {
//            System.out.println("> " + davh.getWay2Label()+", "+davh.getPostalCode()+", " + davh.getLabelCommune());
//        }
//
//
//    }
//
//    @Override
//    public List<DomainVHexavia> find( String filter) {
//        long t0 = System.currentTimeMillis();
//        Set<DomainVHexavia> result = new HashSet<>();
//        //List<DomainVHexavia> result = new ArrayList<>();
//
//        // poste 0 libelle voie
//        // poste 1 code postal
//        // poste 2 ville
//        filter = filter.replaceAll("[']", " ");
//        String[] p1 = filter.split("[,]");
//
//        String path = p1[0].trim();
//        String postal = (p1.length>1) ? p1[1].trim() : "";
//        String commune = (p1.length>2) ? p1[2].trim() : "";
//
//        String[] keys = path.split(" ");
//
//
//        //
//        int deep = keys.length - 1;
//        String key = keys[ deep ].trim();
//        NavigableMap<String, Node> nm = cache.subMap(key, true, key+"|", true);
//
//        if (LOGGER.isDebugEnabled()) {
//            LOGGER.debug(">> nm=" + filter+" ["+key + "] [" + path + "] (" + commune + ") (" +postal+")" + nm.size());
//        }
//
//        for(Map.Entry<String, Node> me : nm.entrySet()) {
//            for(DomainVHexavia davh : me.getValue().items) {
//                //LOGGER.debug(">>>>>>>>>>>>>>>>>>>> " + davh);
//                if ((davh.getWay2Label().contains(path) && (davh.getLabelCommune().startsWith(commune)) && (davh.getPostalCode().startsWith(postal)) )) {
//                    result.add(davh);
//                    //LOGGER.debug(">> az=" + davh);
//                }
//                if (result.size()>200) {
//                    break;
//                }
//            }
//            if (result.size()>200) {
//                break;
//            }
//        }
//        LOGGER.debug(">> az2=" + result.size());
//
//        //
//        List<DomainVHexavia> a = new ArrayList<>( result );
//        a.sort((o1, o2) -> {
//            int i0 = o1.getLabelCommune().compareTo(o2.getLabelCommune());
//            return i0 == 0 ? o1.getWay2Label().compareTo(o2.getWay2Label()) : i0;
//        });
//
//        if (a.size()>200) {
//            a= a.subList(0, 200);
//        }
//
//
//        //System.out.println("je cherche " + filter);
//        //System.out.println("Result.size = " + result.size());
//
//        //System.out.println("------------------------");
//        //for(DomainVHexavia domainVHexavia : result) {
//        //    System.out.println(">" + domainVHexavia.toString() );
//        //}
//
//        LOGGER.debug("in " + (System.currentTimeMillis() - t0)+" ms " );
//
//        return a;
//    }
//}
