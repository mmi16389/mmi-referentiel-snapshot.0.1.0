//package fr.ofii.ref.domain.hexapost;
//
//import com.opengroup.pop.DomainBean;
//
//import java.io.Serializable;
//
///**
// * Represent the full hexapost
// */
//public class DomainVHexavia extends DomainBean implements Serializable {
//    /** */
//    private static final long serialVersionUID = 1L;
//
//    /** ident */
//    private final Long id;
//    /** way2Label */
//    private final String way2Label;
//    /** Hexapost hexapost */
//    private final DomainHexapost address;
//
//
//    /**
//     * default constructor
//     * @param id VHexaviaAddress ident
//     * @param way2Label way2Label
//     * @param da Hexapost Address
//     */
//    protected DomainVHexavia(Long id, String way2Label, DomainHexapost da) {
//        this.id = id;
//        this.way2Label = way2Label;
//        this.address = da;
//    }
//
//    /**
//     * builder
//     * @param id VHexavia hexapost ident
//     * @param way2Label way label
//     * @param da hexapost hexapost
//     * @return complete hexapost
//     */
//    public static DomainVHexavia newInstance(Long id, String way2Label, DomainHexapost da) {
//        return new DomainVHexavia(id, way2Label, da);
//    }
//
//    /**
//     * getId
//     * @return ident
//     */
//    public Long getId() {
//        return id;
//    }
//
//    /**
//     * getWay2Label
//     * @return way name
//     */
//    public String getWay2Label() {
//        return way2Label;
//    }
//
//    /**
//     * getLabelCommune
//     * @return the commune label
//     */
//    public String getLabelCommune() { return address.getCommune().getLabel(); }
//
//    /**
//     * getPostalCode
//     * @return the commune postal code
//     */
//    public String getPostalCode() {
//        if (address.getCommune().getPostalCode().length()==4) {
//            return "0"+address.getCommune().getPostalCode();
//        }
//        return address.getCommune().getPostalCode();
//    }
//
//    public String getDepartmentCode() {
//        String res = "";
//        if (getPostalCode().length()==4) {
//            res = "0"+getPostalCode().substring(0, 1);
//        } else
//        if ("97".equals(getPostalCode().substring(0, 2) ) ) {
//            res = getPostalCode().substring(0, 3);
//        } else {
//            res = getPostalCode().substring(0, 2);
//        }
//        return res;
//    }
//
//    public String getDepartmentLabel() {
//        return "";
//    }
//
//
//    public String azerty() {
//        //items2.push(data[i].way2Label + ", " + data[i].labelCommune+", "+ data[i].postalCode);
//        return getWay2Label()+", "+getLabelCommune()+", "+getPostalCode();
//    }
//
//    /**
//     * toString
//     * @return string
//     */
//    @Override
//    public String toString() {
//        return "DomainAddressVHexavia{" +
//                "id=" + id +
//                ", way2Label='" + way2Label + '\'' +
//                ", labelCommune='" + getLabelCommune() + '\'' +
//                ", postalCode='" + getPostalCode() + '\'' +
//                '}';
//    }
//}
