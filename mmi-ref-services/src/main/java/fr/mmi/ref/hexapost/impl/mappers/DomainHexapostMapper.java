package fr.ofii.ref.hexapost.impl.mappers;

import com.opengroup.pop.exceptions.DomainException;
import com.opengroup.pop.mappers.DomainMapper;
import fr.ofii.ref.hexapost.domain.DomainHexapost;
import fr.ofii.ref.jpa.hexapost.entities.VHexavia;
import org.springframework.stereotype.Component;

/**
 * A Domain / Entity VHexapost Address mappers
 */
@Component
public class DomainHexapostMapper implements DomainMapper<DomainHexapost, VHexavia> {

    /**
     */
    @Override
    public DomainHexapost toOneDomain(VHexavia entity) throws DomainException {

        DomainHexapost z = DomainHexapost.create()
                .assign(entity.getId())
                .assignAddress( entity.getlHexavia().getIdAddress(), entity.getWayLabel(), String.valueOf(entity.getPostalCode()), entity.getlHexavia().getTownLabel())
                .assignDivers( entity.getlHexavia().getInfo() )
                .assignFiller( entity.asString() );

        return z;

    }

    /**
     */
    @Override
    public VHexavia toOneEntity(DomainHexapost domain) {

        return null;
    }
}

