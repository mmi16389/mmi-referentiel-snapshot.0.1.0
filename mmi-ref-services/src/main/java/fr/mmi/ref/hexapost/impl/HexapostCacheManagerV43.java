package fr.ofii.ref.hexapost.impl;

import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IMap;
import com.hazelcast.mapreduce.aggregation.Aggregations;
import com.hazelcast.mapreduce.aggregation.PropertyExtractor;
import com.hazelcast.mapreduce.aggregation.Supplier;
import com.hazelcast.query.PagingPredicate;
import com.hazelcast.query.Predicate;
import com.hazelcast.query.Predicates;
import com.hazelcast.query.SqlPredicate;
import com.opengroup.pop.exceptions.DomainException;
import fr.ofii.ref.hexapost.HexapostCacheManager;
import fr.ofii.ref.hexapost.domain.DomainHexapost;
import fr.ofii.ref.jpa.hexapost.entities.VHexavia3;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;


public class HexapostCacheManagerV43 implements HexapostCacheManager {

    // se charge

    private static final Logger LOGGER = LoggerFactory.getLogger(HexapostCacheManagerV43.class);
    private static final int RESULT_LIMIT = 10;

    private static final String ADR_CACHE = "address";
    private static final String CODE_CACHE = "postalCode";

    private enum SHORT_NAMES {

        AVENUE("AV ", "AVENUE "),
        PLACE("PL ", "PLACE "),
        MARECHAL("MAL ", "MARECHAL "),
        BLD("BLD ", "BOULEVARD "),
        BD("BD ", "BOULEVARD "),
        ALL("ALL ", "ALLEE "),
        AL("AL ", "ALLEE "),
        AR("AR ", "ARRONDISSEMENT "),
        LD("LD ", "LIEU-DIT "),
        IMP("IMP ", "IMPASSE "),
        RTE("RTE ", "ROUTE "),
        RES("RES ", "RESIDENCE "),
        RPT("RPT ", "ROND-POINT "),
        SQ("QS ", "SQUARE "),
        PDT("PDT ", "PRESIDENT "),
        //ARC("ARC ", "ARCADES ")
        ;

        private String option0;
        private String option1;




        SHORT_NAMES(String option0, String option1) {
            this.option0 = option0;
            this.option1 = option1;
        }
    }

    private HazelcastInstance hazel;

    //private IMap<String, DomainHexapost> adrMap;
    private IMap<String, String> postalCodeMap;

    private IMap<String, VHexavia3> adrMap3;

    /** {@inheritDoc} */
    @Override
    public void initialize(HazelcastInstance hazel, String info, List<DomainHexapost> allAddresses) throws DomainException {

//        long t0 = System.currentTimeMillis();
//
//        synchronized (this) {
//            if (this.hazel == null) {
//                LOGGER.info("### hazel set");
//                this.hazel = hazel;
//                this.adrMap = hazel.getMap(ADR_CACHE);
//                this.postalCodeMap = hazel.getMap(CODE_CACHE);
//            }
//        }
//
//        LOGGER.info("depart: " + adrMap.size() );
//        Map<String, List<String>> doublons = new HashMap<>();
//        int[] ct = new int[2];
//
//
//
//        allAddresses.forEach(adr->{
//            String k = adr.getWayLabel()+" "+adr.getPostalCode()+" "+adr.getTownLabel();
//            if (adr.getInfo()!=null) {
//                k += " "+adr.getInfo();
//            }
//
//
//            //
//            if (!adrMap.containsKey(k)) {
//                adrMap.put(k, DomainHexapost
//                        .create()
//                        .assign(adr.getId())
//                        .assignAddress(null, adr.getWayLabel(), adr.getPostalCode(), adr.getTownLabel())
//                        .assignDivers(adr.getInfo())
//                );
//
//            } else {
//                //
//                if (!doublons.containsKey(k)) {
//                    doublons.put(k, new ArrayList<>());
//                }
//                doublons.get(k).add( "("+adr.getId()+") "+adr.getWayLabel()+","+adr.getPostalCode()+","+adr.getTownLabel()+","+adr.getIdAddress());
//                ct[0]++;
//
//            }
//
//
//            //
//            String k2 = adr.getPostalCode()+","+adr.getTownLabel();
//            if (!postalCodeMap.containsKey(k2)) {
//                postalCodeMap.put( k2, k2 );
//            }
//        });
//
//        //
//        //LOGGER.info(adrMap.getLocalMapStats().toString().replaceAll(", ", "\n   "));
//
//        LOGGER.info("---- doublons ");
//        doublons.entrySet().stream().limit(50)
//                .forEach( e->{
//                        LOGGER.info("" + e.getKey());
//                        e.getValue().stream().forEach(s -> {
//                            LOGGER.info("   " + s);
//                        });
//                });
//        LOGGER.info("  --> " + doublons.size()+ " key doublons  total:" + ct[0]);
//        LOGGER.info(String.format("  ----> V43 %s cache in %d total_traitee:%d cache:%d postal_code:%d", info, (System.currentTimeMillis() - t0), allAddresses.size(), adrMap.size(), postalCodeMap.size() ));
    }



    /** {@inheritDoc} */
    @Override
    public void initialize3(HazelcastInstance hazel, String info, List<VHexavia3> allAddresses) throws DomainException {

        long t0 = System.currentTimeMillis();

        synchronized (this) {
            if (this.hazel == null) {
                LOGGER.info("### hazel set V3");
                this.hazel = hazel;
                this.adrMap3 = hazel.getMap(ADR_CACHE);
                this.adrMap3.evictAll();
                //this.adrMap3.addIndex("postalCode", false);
                //this.adrMap3.addIndex("townLabel", false);
                this.postalCodeMap = hazel.getMap(CODE_CACHE);
                this.postalCodeMap.evictAll();
            }
        }

        LOGGER.info("depart: V3 " + adrMap3.size() );
        Map<String, List<String>> doublons = new HashMap<>();
        int[] ct = new int[2];

        allAddresses.forEach(adr->{
            StringBuilder k = new StringBuilder(adr.getWayLabel())
                    .append(" ")
                    .append(adr.getPostalCodeAsString())
                    .append(" ")
                    .append(adr.getTownLabel())
                    ;
            if (adr.getInfo()!=null) {
                k.append( " ").append( adr.getInfo() );
            }


            //
            if (!adrMap3.containsKey(k.toString())) {
                adrMap3.put(k.toString(), adr);

            } else {
                //
                if (!doublons.containsKey(k.toString())) {
                    doublons.put(k.toString(), new ArrayList<>());
                }
                doublons.get(k.toString()).add( "("+adr.getId()+") "+adr.getWayLabel()+","+adr.getPostalCode()+","+adr.getTownLabel()+","+adr.getIdAddress());
                ct[0]++;

            }


            //
            String k2 = new StringBuilder(adr.getPostalCodeAsString()).append(",").append(adr.getTownLabel()).toString();
            if (!postalCodeMap.containsKey(k2)) {
                postalCodeMap.put( k2, k2 );
            }
        });

        //
        //LOGGER.info(adrMap.getLocalMapStats().toString().replaceAll(", ", "\n   "));

        LOGGER.info("---- doublons V3 ");
        doublons.entrySet().stream().limit(10)
                .forEach( e->{
                    LOGGER.info("{}", e.getKey());
                    e.getValue().stream().forEach(s -> LOGGER.info("   {}", s) );
                });
        LOGGER.info(" V3 --> {} key doublons  total:{}", doublons.size(), ct[0]);
        LOGGER.info(" -----> V43 {} cache in {} total_traitee:{} cache:{} postal_code:{}", info, (System.currentTimeMillis() - t0), allAddresses.size(), adrMap3.size(), postalCodeMap.size() );
    }





    private String reduceLast(String[] p2) {
        StringBuilder sb = new StringBuilder();
        for(int i = 0; i<p2.length - 1; i++ ) {
            if (sb.length()>0) {
                sb.append(" ");
            }
            sb.append( p2[i] );
        }
        return sb.toString();
    }


    private SqlPredicate createLike(String value) {
        return new SqlPredicate(
                new StringBuilder("__key like '%")
                        .append( value )
                        .append( "%'")
                        .toString()
        );
    }

    private SqlPredicate createEqual(String property, String value) {
        return new SqlPredicate(
                new StringBuilder(property)
                        .append("=")
                        .append( value )
                        .append( "%'")
                        .toString()
        );
    }

    /** {@inheritDoc} */
    @Override
    public List<DomainHexapost> find( String filter) {
        long t0 = System.currentTimeMillis();

        String[] p1 = filter.split("[,]");
        String way = (p1.length>0) ?  p1[0].trim() : "";
        String postal = (p1.length>1) ? p1[1].trim() : "";
        String town = (p1.length>2) ? p1[2].trim() : "";

        //
        Predicate<Long, DomainHexapost> sql = createLike( way );

        // pas bon pr MAL
        for(SHORT_NAMES shortNames : SHORT_NAMES.values()) {
            if (way.startsWith( shortNames.option0 )) {
                sql = Predicates.or( sql, createLike( way.replace(shortNames.option0,  shortNames.option1)) );
            } else
            if (way.startsWith( shortNames.option1 )) {
                sql = Predicates.or(sql, createLike( way.replace(shortNames.option1, shortNames.option0)) );
            }
        }

        //
        if (way.indexOf(" ")>-1) {
            String[] p2 = way.split(" ");
            String wayWithoutLastWord = reduceLast( p2 );
            town = p2[ p2.length - 1];
            sql = Predicates.or( sql, createLike( wayWithoutLastWord+"%'") );
            for(SHORT_NAMES shortNames : SHORT_NAMES.values()) {
                if (way.startsWith( shortNames.option0 )) {
                    sql = Predicates.or( sql, createLike( wayWithoutLastWord.replace(shortNames.option0,  shortNames.option1)) );
                } else
                if (way.startsWith( shortNames.option1 )) {
                    sql = Predicates.or(sql, createLike( wayWithoutLastWord.replace(shortNames.option1, shortNames.option0) ));
                }
            }
        }

        //
        if (postal.length()>0) {
            Predicate<Long, DomainHexapost> sql1 = createLike( postal);
            sql = Predicates.and(sql, sql1);
        }

        //
        if (town.length()>0) {
            Predicate<Long, DomainHexapost> sql1 = new SqlPredicate("__key like '%" + town + "%'");
            sql = Predicates.and(sql, sql1);
        }



        List<DomainHexapost> l = adrMap3
                .values( new PagingPredicate<>( sql, RESULT_LIMIT ) )
                .stream()
                .map( x-> DomainHexapost.create()
                        .assignAddress(x.getIdAddress(), x.getWayLabel(), x.getPostalCodeAsString(), x.getTownLabel())
                        .assignDivers(x.getInfo()))
                .collect(Collectors.toList());

        LOGGER.debug(  " {} in {} ms ", sql.toString(), (System.currentTimeMillis() - t0) );

        return l;
    }

    /** {@inheritDoc} */
    @Override
    public List<String> findPostalCode(String filter) {
        PropertyExtractor<String, String> extractor = (value) -> value.split(",")[0];

        return postalCodeMap
                .aggregate( Supplier.fromPredicate( x-> x.getValue().startsWith(filter), Supplier.all(extractor)), Aggregations.distinctValues())
                .stream()
                .map(x-> String.valueOf(x).split(",")[0] )
                .limit(RESULT_LIMIT)
                .collect(Collectors.toList());
    }

    /** {@inheritDoc} */
    @Override
    public List<String> findPostalCommuneByPostalCode(String filter) {
        return postalCodeMap
                .values( createLike(filter) )
                .stream()
                .map(x->x.split(",")[1])
                .collect(Collectors.toList());
    }

    /** {@inheritDoc} */
    @Override
    public List<DomainHexapost> postalWayFilter(String postalCodeAndCommuneFilter) throws DomainException {

        String[] f = postalCodeAndCommuneFilter.split("[,]");

        return adrMap3
                .values( Predicates.and(createLike(f[0]), createEqual("postalCode", f[1]), createEqual("townLabel", f[2]) ))
                .stream().limit(50)
                .map( x-> DomainHexapost.create()
                        .assignAddress(x.getIdAddress(), x.getWayLabel(), x.getPostalCodeAsString(), x.getTownLabel())
                        .assignDivers(x.getInfo()) )
                .collect(Collectors.toList());
    }

    /** {@inheritDoc} */
    @Override
    public List<String> getListCodeInseeByZip(String zipCode) throws DomainException {
        return null;
    }


}
