package fr.mmi.ref.dna;

import fr.mmi.ref.dna.domain.DomainAppealCourt;

import java.util.List;

public interface AppealCourtServices {

    DomainAppealCourt findOne(Long id) throws DomainException;

    List<DomainAppealCourt> findAll() throws DomainException;
}
