package fr.ofii.ref.dna.impl.mappers;

import com.opengroup.pop.exceptions.DomainException;
import com.opengroup.pop.mappers.DomainMapper;
import fr.ofii.ref.dna.domain.DomainRegion;
import fr.ofii.ref.jpa.dna.entities.Region;
import org.springframework.stereotype.Component;

@Component
public class DomainRegionMapper implements DomainMapper<DomainRegion,Region> {

    @Override
    public DomainRegion toOneDomain(Region region) throws DomainException {
        if (region==null) {
            return null;
        }
        return DomainRegion.from(region.getRegionCode(),region.getRegionName());
    }

    @Override
    public Region toOneEntity(DomainRegion domainRegion) throws DomainException {
        return new Region().define(domainRegion.getCode(), domainRegion.getLabel());
    }
}
