package fr.ofii.ref.dna.impl.mappers;

import com.opengroup.pop.exceptions.DomainException;
import com.opengroup.pop.mappers.DomainMapper;
import fr.ofii.ref.dna.domain.DomainCountry;
import fr.ofii.ref.jpa.dna.entities.Country;
import org.springframework.stereotype.Component;

@Component
public class DomainCountryMapper implements DomainMapper<DomainCountry, Country> {

    @Override
    public DomainCountry toOneDomain(Country countryEntity) throws DomainException {
        if (countryEntity==null) {
            return null;
        }
        return DomainCountry.from(countryEntity.getId(), countryEntity.getNationalityName(), countryEntity.getName());
    }

/*
    with this model mapper, the conversion logic is quick and simple –
    we’re using the map API of the mapper  and getting the data converted
    without writing a single line of conversion logic.
 */
    @Override
    public Country toOneEntity(DomainCountry domainCountry) throws DomainException {
        Country countryEntity = new Country();
        countryEntity.setId(domainCountry.getId());
        countryEntity.setNationalityName(domainCountry.getNationalityName());
        countryEntity.setName(domainCountry.getName());
        return countryEntity;
    }
}
