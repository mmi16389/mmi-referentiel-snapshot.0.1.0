package fr.mmi.ref.dna.domain;


public class DomainCountry extends DomainBean {

    private String id;
    private String name;
    private String nationalityName;

    private DomainCountry(String id, String nationalityName, String name) {
        this.id = id;
        this.nationalityName = nationalityName;
        this.name = name;
    }

    public static DomainCountry from(String id, String nationalityName, String name) {
        return new DomainCountry(id, nationalityName, name);
    }

    public  String getNationalityName() {
        return nationalityName;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

}
