package fr.mmi.ref.dna.domain;

import fr.ofii.ref.utils.ExtendedDomainBean;


public class DomainAppealCourt extends ExtendedDomainBean {

    private Long id;
    private String shortName;
    private String longName;

    private DomainAppealCourt() {
    }

    public static DomainAppealCourt create() {
        return new DomainAppealCourt();
    }

    public DomainAppealCourt assignFind(Long id, String shortName, String longName) {
        this.id = id;
        this.shortName = shortName;
        this.longName = longName;
        return this;
    }

    public Long getId() {
        return id;
    }

    public String getShortName() {
        return shortName;
    }

    public String getLongName() {
        return longName;
    }
}
