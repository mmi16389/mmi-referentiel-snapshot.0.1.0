package fr.mmi.ref.dna.domain;

import fr.mmi.ref.utils.ExtendedDomainBean;

import java.io.Serializable;

/**
 * @author MMI
 * @since 0.1.0-SNAPSHOT
 */
public class DomainParameter extends ExtendedDomainBean implements Serializable {

    private String key;

    private String paramName;

    private String value;

    /**
     *
     * @param key
     * @param value
     */
    private DomainParameter(String key, String parameterName, String value){
        this.key=key;
        this.value=value;
        this.paramName = parameterName;
    }

    public static DomainParameter fromParamterContextAndValue(String parameterContext, String parameterName, String value){
        return new DomainParameter(parameterContext, parameterName,value);
    }


    public String getKeyNameCacheKey() {
        return new StringBuilder(key).append(".").append(paramName).toString();


    }

    public String getKeyValueCacheKey() {
        return new StringBuilder(key).append(".").append(value).toString();
    }


    /**
     * Gets key.
     *
     * @return Value of key.
     */
    public String getKey() {
        return key;
    }

    /**
     * Gets value.
     *
     * @return Value of value.
     */
    public String getValue() {
        return value;
    }

    public String getParamName() {
        return paramName;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof DomainParameter)) {
            return false;
        }
        if (this.key != null && !this.key.equals(((DomainParameter) obj).getKey())) {
            return false;
        }
        return !(this.value != null && !this.value.equals(((DomainParameter) obj).getValue()));
    }
}
