package fr.ofii.ref.dna.impl;

import com.opengroup.pop.exceptions.DomainException;
import fr.ofii.ref.dna.AppealCourtServices;
import fr.ofii.ref.dna.domain.DomainAppealCourt;
import fr.ofii.ref.dna.impl.mappers.DomainAppealCourtMapper;
import fr.ofii.ref.jpa.dna.AppealCourtRepository;
import fr.ofii.ref.jpa.dna.entities.AppealCourt;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 *
 */
@Service
public class AppealCourtServicesImpl implements AppealCourtServices {
    private static final Logger LOGGER = LoggerFactory.getLogger(AppealCourtServicesImpl.class);

    @Autowired
    private AppealCourtRepository repository;

    @Autowired
    private DomainAppealCourtMapper domainMapper;

    private Map<Long, DomainAppealCourt> map;


    @Override
    public DomainAppealCourt findOne(Long id) throws DomainException {
        return map.get( id );
    }

    @Override
    public List<DomainAppealCourt> findAll() throws DomainException {
        return new ArrayList<>( map.values() );
    }

    @PostConstruct
    private void initialize() throws DomainException {

        this.map = domainMapper.toDomains( (List<AppealCourt>)repository.findAll() )
                .stream()
                .collect( Collectors.toMap(DomainAppealCourt::getId, Function.identity()));

        LOGGER.info("##### countries loaded {} total {}", map.size(), repository.count() );

    }
}