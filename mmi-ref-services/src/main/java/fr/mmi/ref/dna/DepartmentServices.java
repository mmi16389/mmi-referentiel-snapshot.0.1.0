package fr.mmi.ref.dna;

import fr.mmi.ref.dna.domain.DomainDepartment;

import java.util.List;

/**
 *
 */
public interface DepartmentServices {

    /**
     * get all departments
     * @return list of domain department
     * @throws DomainException exception
     */
    List<DomainDepartment> getAllDepartments() throws DomainException;

    /**
     *
     * @param departmentCode
     * @return
     * @throws DomainException
     */
    DomainDepartment getDepartmentByCode(String departmentCode) throws DomainException;

    List<DomainDepartment> getDepartmentContainsLabel(String label) throws DomainException;

}