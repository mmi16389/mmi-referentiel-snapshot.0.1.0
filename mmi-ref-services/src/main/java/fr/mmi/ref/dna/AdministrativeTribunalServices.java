package fr.mmi.ref.dna;


import fr.mmi.ref.dna.domain.DomainAdministrativeTribunal;

import java.util.List;

public interface AdministrativeTribunalServices {

    DomainAdministrativeTribunal findOne(Long id) throws DomainException;

    List<DomainAdministrativeTribunal> findAll() throws DomainException;

}
