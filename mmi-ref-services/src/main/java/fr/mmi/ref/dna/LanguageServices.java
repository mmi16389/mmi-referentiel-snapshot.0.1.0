package fr.mmi.ref.dna;

import fr.mmi.ref.dna.domain.DomainLanguage;

import java.util.List;

public interface LanguageServices {

    /**
     *
     * @param idLanguage
     * @return
     * @throws DomainException
     */
    DomainLanguage getLanguage(Long idLanguage)  throws DomainException;

    List<DomainLanguage> getLanguages()  throws DomainException;

}
