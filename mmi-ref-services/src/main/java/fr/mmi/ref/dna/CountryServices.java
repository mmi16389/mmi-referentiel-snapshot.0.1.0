package fr.mmi.ref.dna;


import fr.mmi.ref.dna.domain.DomainCountry;

import java.util.List;
import java.util.Map;

/**
 * Created by MMI on 26/07/2017.
 */
public interface CountryServices {

    /**
     *
     * @param id
     * @return
     * @throws DomainException
     */
    DomainCountry getCountry(String id)  throws DomainException;

    Map<String, DomainCountry> getCountries()  throws DomainException;

    List<DomainCountry> getNationalities()  throws DomainException;

    List<DomainCountry> getNationality(String namePattern)  throws DomainException;
}
