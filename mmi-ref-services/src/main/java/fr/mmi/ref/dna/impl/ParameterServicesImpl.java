package fr.ofii.ref.dna.impl;


import com.opengroup.pop.exceptions.DomainException;
import fr.ofii.ref.dna.ParameterServices;
import fr.ofii.ref.dna.domain.DomainParameter;
import fr.ofii.ref.dna.impl.mappers.DomainParameterMapper;
import fr.ofii.ref.jpa.dna.ParameterRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by EXT_HHE37 on 08/08/2017.
 * <p>
 * provide parameters services
 */
@Service
public class ParameterServicesImpl implements ParameterServices {

    private static final Logger LOGGER = LoggerFactory.getLogger(ParameterServicesImpl.class);

    @Autowired
    private ParameterRepository parameterRepository;

    //@Autowired
    //private CacheManager cacheManager;

    @Autowired
    private DomainParameterMapper domainParameterMapper;

    @Override
    //@Cacheable(value = "#paramContext.#paramName")
    public String getParameterValueByContextAndName(String paramContext, String paramName) {
        return parameterRepository.findParamValueById(paramContext, paramName);
    }

    @Override
    //@Cacheable(value = "#paramContext.#paramValue")
    public String getParameterNameByContextAndValue(String paramContext, String paramValue) {
        return parameterRepository.findParamNamebyContextAndValue(paramContext, paramValue);
    }

    /**
     * @param paramContext
     * @return
     */
    @Override
    //@Cacheable(value = "#paramContext")
    public List<DomainParameter> getParameterByParamContext(String paramContext) throws DomainException {
        return domainParameterMapper.toDomains(parameterRepository.findByParamContext(paramContext));
    }

    /**
     * after create instance prepare caches
     */
    @PostConstruct
    private void loadAll() throws DomainException {

//        final Cache cache = cacheManager.getCache("parameters");
//        //cache.clear();
//        cacheManager.getCacheNames().forEach(System.out::println);
//
//        List<DomainParameter> parameters = domainParameterMapper.toDomains( parameterRepository.findAll() );
//
//        parameters.forEach(p -> {
//            System.out.println(">>" + p.getKey()+" "+p.getValue());
//            cache.put( p.getKeyNameCacheKey(), p.getValue());
//            //cache.put( p.getKey()+'.'+p.getValue(), p.getParamName() );
//            //cache.put(p.getKey() + "." + p.getValue(), p.getParamName() );
//
//            //
//            Cache.ValueWrapper value = cache.get(p.getKey());
//            if (value == null ) {
//                cache.put(p.getKey(), new ArrayList<DomainParameter>());
//                value = cache.get(p.getKey());
//            }
//            ((List<DomainParameter>)value.get()).add( p );
//
//        });

        //LOGGER.info("##### parameters loaded {} total {}", parameters.size(), parameterRepository.count());

    }
}
