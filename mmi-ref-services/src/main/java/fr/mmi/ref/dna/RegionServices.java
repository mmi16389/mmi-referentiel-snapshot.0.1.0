package fr.mmi.ref.dna;

import fr.mmi.ref.dna.domain.DomainRegion;

import java.util.List;

public interface RegionServices {


    List<DomainRegion> getRegions() throws DomainException;
}
