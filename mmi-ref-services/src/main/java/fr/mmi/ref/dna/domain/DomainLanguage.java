package fr.mmi.ref.dna.domain;


import fr.ofii.ref.utils.DomainMethodControlCode;
import fr.ofii.ref.utils.ExtendedDomainBean;


public class DomainLanguage extends ExtendedDomainBean {

    private Long id;
    private String language;

    private DomainLanguage(Long id, String language) throws DomainException {
        checkNotEmpty(language, DomainMethodControlCode.NOT_FIELD_REGION_NAME);
        checkIfValid();
        this.id = id;
        this.language = language;
    }

    public static DomainLanguage from(Long id, String language) throws DomainException {
        return new DomainLanguage(id, language);
    }

    public Long getId() {
        return id;
    }

    public String getLanguage() {
        return language;
    }
}