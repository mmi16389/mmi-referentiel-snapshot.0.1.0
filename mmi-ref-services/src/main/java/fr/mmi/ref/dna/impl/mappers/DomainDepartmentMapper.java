package fr.ofii.ref.dna.impl.mappers;

import com.opengroup.pop.exceptions.DomainException;
import com.opengroup.pop.mappers.DomainMapper;
import fr.ofii.ref.dna.domain.DomainDepartment;
import fr.ofii.ref.jpa.dna.entities.Department;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Map a department "entity" into a "domain" department or a department "domain" into an "entity" department
 */
@Component
public class DomainDepartmentMapper implements DomainMapper<DomainDepartment, Department> {

    @Autowired
    private DomainRegionMapper domainRegionMapper;

    @Override
    public DomainDepartment toOneDomain(Department department) throws DomainException {
        if (department==null) {
            return null;
        }

        return DomainDepartment.from(
                department.getDepartmentCode(),
                department.getDepartmentName(),
                domainRegionMapper.toOneDomain(department.getRegion())
        );
    }

    @Override
    public Department toOneEntity(DomainDepartment domainDepartment) throws DomainException {
        Department departmentEntity=new Department();
        departmentEntity.setDepartmentCode(domainDepartment.getCode());
        departmentEntity.setDepartmentName(domainDepartment.getLabel());
        departmentEntity.setRegion(domainRegionMapper.toOneEntity(domainDepartment.getDomainRegion()));
        return departmentEntity;
    }
}
