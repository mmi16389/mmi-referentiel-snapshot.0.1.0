package fr.ofii.ref.dna.impl;

import com.opengroup.pop.exceptions.DomainException;
import fr.ofii.ref.dna.AdministrativeTribunalServices;
import fr.ofii.ref.dna.domain.DomainAdministrativeTribunal;
import fr.ofii.ref.dna.impl.mappers.DomainAdministrativeTribunalMapper;
import fr.ofii.ref.jpa.dna.AdministrativeTribunalRepository;
import fr.ofii.ref.jpa.dna.entities.AdministrativeTribunal;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 *
 */
@Service
public class AdministrativeTribunalServicesImpl implements AdministrativeTribunalServices {
    private static final Logger LOGGER = LoggerFactory.getLogger(AdministrativeTribunalServicesImpl.class);

    @Autowired
    private AdministrativeTribunalRepository repository;

    @Autowired
    private DomainAdministrativeTribunalMapper domainMapper;

    private Map<Long, DomainAdministrativeTribunal> map;


    @Override
    public DomainAdministrativeTribunal findOne(Long id) throws DomainException {
        return map.get( id );
    }

    @Override
    public List<DomainAdministrativeTribunal> findAll() throws DomainException {
        return new ArrayList<>( map.values() );
    }

    @PostConstruct
    private void initialize() throws DomainException {

        this.map = domainMapper.toDomains( (List<AdministrativeTribunal>)repository.findAll() )
                .stream()
                .collect( Collectors.toMap(DomainAdministrativeTribunal::getId, Function.identity()));

        LOGGER.info("##### countries loaded {} total {}", map.size(), repository.count() );

    }
}