package fr.ofii.ref.dna.impl;

import com.opengroup.pop.exceptions.DomainException;
import fr.ofii.ref.dna.CountryServices;
import fr.ofii.ref.dna.domain.DomainCountry;
import fr.ofii.ref.dna.impl.mappers.DomainCountryMapper;
import fr.ofii.ref.utils.DomainMethodControlCode;
import fr.ofii.ref.utils.ServiceUtil;
import fr.ofii.ref.jpa.dna.CountryRepository;
import fr.ofii.ref.jpa.dna.entities.Country;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 *
 */
@Service
public class CountryServicesImpl implements CountryServices {
    private static final Logger LOGGER = LoggerFactory.getLogger(CountryServicesImpl.class);

    @Autowired
    private CountryRepository countryRepository;

    @Autowired
    private DomainCountryMapper domainCountryMapper;

    private Map<String, DomainCountry> map;


    @Override
    public DomainCountry getCountry(String id) throws DomainException {
        ServiceUtil.assertNotNull(id, LOGGER, DomainMethodControlCode.INSEE_PAYS_NATIONALITE_NOT_FOUND);
        return map.get( id );
    }

    @Override
    public Map<String, DomainCountry> getCountries() throws DomainException {
        return map;
    }

    @Override
    public List<DomainCountry> getNationality(String namePattern)  throws DomainException {

        return map.values().stream()
                .filter(dc->dc.getNationalityName().contains(namePattern))
                .collect(Collectors.toList());

    }

    @Override
    public List<DomainCountry> getNationalities()  throws DomainException {

        return map.values().stream()
                .collect(Collectors.toList());

    }


    @PostConstruct
    private void initialize() throws DomainException {

        this.map = domainCountryMapper.toDomains( (List<Country>)countryRepository.findAll())
                .stream()
                .collect( Collectors.toMap(DomainCountry::getId, Function.identity()));

        LOGGER.info("##### countries loaded {} total {}", map.size(), countryRepository.count());

    }
}