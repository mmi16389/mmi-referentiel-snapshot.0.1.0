//package fr.ofii.ref.dna.impl.mappers;
//
//import com.opengroup.pop.exceptions.DomainException;
//import com.opengroup.pop.mappers.DomainMapper;
//import fr.ofii.ref.dna.domain.DomainTown;
//import fr.ofii.ref.jpa.dna.entities.Town;
//import org.springframework.stereotype.Component;
//
//@Component
//public class DomainTownMapper implements DomainMapper<DomainTown, Town> {
//    @Override
//    public DomainTown toOneDomain(Town entity) throws DomainException {
//            return DomainTown.from(
//                    entity.getInseeCode(),
//                    entity.getTownType(),
//                    entity.getZipCode(),
//                    entity.getTownName(),
//                    entity.getCityName()
//            );
//    }
//
//    @Override
//    public Town toOneEntity(DomainTown domainTown) throws DomainException {
//        Town entity = new Town();
//        entity.setInseeCode(domainTown.getInseeCode());
//        entity.setCityName(domainTown.getStreetCity());
//        entity.setTownType(domainTown.getTownType());
//        entity.setZipCode(domainTown.getZipCode());
//        entity.setTownName(domainTown.getStreetTown());
//        return entity;
//    }
//}
//
