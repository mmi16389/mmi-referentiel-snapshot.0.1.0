package fr.ofii.ref.dna.impl;

import com.opengroup.pop.exceptions.DomainException;
import fr.ofii.ref.dna.DepartmentServices;
import fr.ofii.ref.dna.domain.DomainDepartment;
import fr.ofii.ref.dna.impl.mappers.DomainDepartmentMapper;
import fr.ofii.ref.jpa.dna.DepartmentRepository;
import fr.ofii.ref.jpa.dna.entities.Department;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;


/**
 *
 */
@Service
public class DepartmentServicesImpl implements DepartmentServices {

    private static final Logger LOGGER = LoggerFactory.getLogger(DepartmentServicesImpl.class);

    /**
     * The department repository
     */
    @Autowired
    private DepartmentRepository departmentRepository;

    /**
     * mapper domain / entities for departments
     */
    @Autowired
    private DomainDepartmentMapper domainDepartmentMapper;

    private Map<String, DomainDepartment> map;


    /**
     * return all departments from a cafeine cache
     *
     * @return list of departments domain
     * @throws DomainException exception
     */
    @Override
    public List<DomainDepartment> getAllDepartments() throws DomainException {
        return new ArrayList<>(map.values());
    }

    /**
     * @param departmentCode the name of the department
     * @return list of departments domain
     * @throws DomainException exception
     */
    @Override
    public DomainDepartment getDepartmentByCode(String departmentCode) throws DomainException {
        return map.get( departmentCode );
    }

    @Override
    public List<DomainDepartment> getDepartmentContainsLabel(String label) throws DomainException {
        return map.values()
                .stream()
                .filter( dept -> dept.getLabel().contains(label))
                .collect(Collectors.toList());
    }

    /**
     * after create instance prepare caches
     */
    @PostConstruct
    private void initialize() throws DomainException {

        this.map = domainDepartmentMapper.toDomains( (List<Department>)departmentRepository.findAll())
                .stream()
                .collect( Collectors.toMap(DomainDepartment::getCode, Function.identity()));

        LOGGER.info("##### departments loaded {} total {}", map.size(), departmentRepository.count());

    }

}
