package fr.ofii.ref.dna.impl.mappers;

import com.opengroup.pop.exceptions.DomainException;
import com.opengroup.pop.mappers.DomainMapper;
import fr.ofii.ref.dna.domain.DomainAppealCourt;
import fr.ofii.ref.jpa.dna.entities.AppealCourt;
import org.springframework.stereotype.Component;

@Component
public class DomainAppealCourtMapper implements DomainMapper<DomainAppealCourt, AppealCourt> {

    @Override
    public DomainAppealCourt toOneDomain(AppealCourt entity) throws DomainException {
        if (entity==null) {
            return null;
        }

        return DomainAppealCourt
                .create()
                .assignFind(entity.getId(), entity.getShortName(), entity.getLongName() );
    }

    @Override
    public AppealCourt toOneEntity(DomainAppealCourt domain) throws DomainException {
        return new AppealCourt()
                .assignAll(domain.getId(), domain.getShortName(), domain.getLongName());
    }
}
