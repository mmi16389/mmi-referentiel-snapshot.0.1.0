package fr.ofii.ref.dna.impl;

import com.opengroup.pop.exceptions.DomainException;
import fr.ofii.ref.dna.LanguageServices;
import fr.ofii.ref.dna.domain.DomainLanguage;
import fr.ofii.ref.dna.impl.mappers.DomainLanguageMapper;
import fr.ofii.ref.jpa.dna.LanguageRepository;
import fr.ofii.ref.jpa.dna.entities.Language;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 *
 */
@Service
public class LanguageServicesImpl implements LanguageServices {
    private static final Logger LOGGER = LoggerFactory.getLogger(LanguageServicesImpl.class);

    @Autowired
    private LanguageRepository languageRepository;

    @Autowired
    private DomainLanguageMapper domainLanguageMapper;

    private Map<Long, DomainLanguage> map;

    @Override
    public DomainLanguage getLanguage(Long idlanguage) throws DomainException {
        if (idlanguage == null) {
            return null;
        }
        return map.get( idlanguage );
    }

    @Override
    public List<DomainLanguage> getLanguages() throws DomainException {
        return new ArrayList<>( map.values() );
    }

    /**
     * After create instance prepare caches
     */
    @PostConstruct
    private void initialize() throws DomainException {

        this.map = domainLanguageMapper.toDomains( (List<Language>)languageRepository.findAll())
                .stream()
                .collect( Collectors.toMap(DomainLanguage::getId, Function.identity()));

        LOGGER.info("##### languages loaded {} total {}", map.size(), languageRepository.count());


    }
}
