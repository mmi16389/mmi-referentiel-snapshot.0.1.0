package fr.mmi.ref.dna;

/**
 * This  goal of this class, is to provide the Param context;
 * Created by MMI on 08/08/2017.
 */
public enum ParamContextEnum {

    SITE_TYPE("SiteType"),  // SPA(spa)  GUI(guichet)  PRF(prefecture)
    PROCEDURE_TYPE("ProcedureType"), // PRI(prioritaire)  - NOR(normale)  - DUB(dubliné)
    PROCEDURE_SITUATION("ProcedureSituation"), // PS1(Procedure situation 1) - PS2(Procedure situation 2)
    SEXE_TYPE("SexeType"),   // FEM(F)  - MAS (M)
    MATRIOMONIAL_TYPE("MatriomonialType"),  // CEL(celibataire) - MAR(marie) - DIV(divorce) - CON(conjoint) - ISO(isole) -  SEP(separe) - VEU(veuf)
    DEMANDEUR_TYPE("DemandeurType"),  // PRI(principale) - CON(conjoint) - AAD(Autre adulte)
    HOUR_START_OF_MORNING(" 00:00:00"),
    HOUR_END_OF_NIGHT(" 23:59:59"),
    FRENCH_DATE_FORMAT("dd/MM/yyyy HH:mm:ss"),
    BRITISH_DATE_FORMAT("yyyy-MM-dd HH:mm:ss"),
    ACCOMMODATION_STATUS("StatusHebergement"), // 1(valide), 0(invalide)
    ACCOMMODATION_TYPE("SiteTypeHebergement"), // CLF(Collectif), SCF(Semi-collectif), DFS(Diffus), TNR(NR)
    ACCOMMODATION_MEDICAL_EQUIPMENT("EquipementsMedicaux"),// VCH(Ville avec CHU), VSH(Ville avec équipements médicaux (sans CHU)), SEM(Site éloigné des équipements médicaux), MNR(NR)
    ACCOMMODATION_SCHOLL_EQUIPMENT("EquipementsScolaires"), // NIM(Niveau Maternelle), NIP(Niveau Primaire), NIC(Niveau collège), NIL(Niveau lycée)
    ACCOMMODATION_PLACE_TYPE("PlaceType"),//P1(appart avec une piece), P2(appart avec deux piece), P3(appart avec trois pieces)
    ACCOMMODATION_PLACE_MAIN_OPERATIONS("MainOperations"), // à voir avec les PO
    Forward_To_Status("ForwardToStatus"), // 0(oriénté) - 1(entrée) - 2(refus) - 3(non présentation)
    Forward_To_Type_Not_Entered("ForwardToTypeNotEntered"), //
    // NIM(Niveau maternelle), NIP(Niveau primaire), NIC(Niveau collège), NIL(Niveau lycée)
    QUESTION("Question"),
    RESIDENT_PERMIT_TYPE("TitreSejourType"),
    DECISION_NATURE("NatureDecision")
    ;



   private String paramContext;

   ParamContextEnum(String paramContext) {
        this.paramContext=paramContext;
    }


    public String getValue() {
        return this.paramContext;
    }
}

