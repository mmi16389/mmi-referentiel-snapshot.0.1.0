package fr.mmi.ref.dna.domain;

import fr.mmi.ref.utils.DomainMethodControlCode;
import fr.mmi.ref.utils.ExtendedDomainBean;

public class DomainRegion extends ExtendedDomainBean {


    private String code;
    private String label;

    private DomainRegion(String code,String label)throws DomainException{
        //
        checkNotEmpty( code, DomainMethodControlCode.NOT_FIELD_REGION_CODE);
        checkNotEmpty( label, DomainMethodControlCode.NOT_FIELD_REGION_NAME);
        checkIfValid();
        this.code=code;
        this.label=label;
    }

    public static DomainRegion from(String code, String label)throws DomainException{
      return new DomainRegion(code,label);
    }

    public String getCode() {
        return code;
    }

    public String getLabel() {
        return label;
    }


}
