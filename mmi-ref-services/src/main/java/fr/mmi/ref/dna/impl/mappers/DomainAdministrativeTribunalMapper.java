package fr.ofii.ref.dna.impl.mappers;

import com.opengroup.pop.exceptions.DomainException;
import com.opengroup.pop.mappers.DomainMapper;
import fr.ofii.ref.dna.domain.DomainAdministrativeTribunal;
import fr.ofii.ref.jpa.dna.entities.AdministrativeTribunal;
import org.springframework.stereotype.Component;

@Component
public class DomainAdministrativeTribunalMapper implements DomainMapper<DomainAdministrativeTribunal, AdministrativeTribunal> {

    @Override
    public DomainAdministrativeTribunal toOneDomain(AdministrativeTribunal entity) throws DomainException {
        if (entity==null) {
            return null;
        }
        
        return DomainAdministrativeTribunal
                .create()
                .assignFind(entity.getId(), entity.getShortName(), entity.getLongName() );
    }

    @Override
    public AdministrativeTribunal toOneEntity(DomainAdministrativeTribunal domain) throws DomainException {
        return new AdministrativeTribunal()
                .assignAll(domain.getId(), domain.getShortName(), domain.getLongName());
    }
}
