package fr.ofii.ref.dna.impl.mappers;

import com.opengroup.pop.exceptions.DomainException;
import com.opengroup.pop.mappers.DomainMapper;
import fr.ofii.ref.dna.domain.DomainLanguage;
import fr.ofii.ref.jpa.dna.entities.Language;
import org.springframework.stereotype.Component;

@Component
public class DomainLanguageMapper implements DomainMapper<DomainLanguage,Language> {

    @Override
    public DomainLanguage toOneDomain(Language language) throws DomainException {
        if (language == null) {
            return null;
        }

        return DomainLanguage.from(language.getId(), language.getLanguage());
    }

    @Override
    public Language toOneEntity(DomainLanguage domainLanguage) throws DomainException {

        Language language = new Language();
        language.setId(domainLanguage.getId());
        language.setLanguage(domainLanguage.getLanguage());
        return language;
    }
}
