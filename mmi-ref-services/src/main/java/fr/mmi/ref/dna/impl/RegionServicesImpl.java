package fr.ofii.ref.dna.impl;

import com.opengroup.pop.exceptions.DomainException;
import fr.ofii.ref.dna.DepartmentServices;
import fr.ofii.ref.dna.RegionServices;
import fr.ofii.ref.dna.domain.DomainRegion;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;


/**
 *
 */
@Service
public class RegionServicesImpl implements RegionServices {

    private static final Logger LOGGER = LoggerFactory.getLogger(RegionServicesImpl.class);

    @Autowired
    private DepartmentServices departmentServices;


    @Override
    public List<DomainRegion> getRegions() throws DomainException {

        Set<DomainRegion> regions = new TreeSet<>(Comparator.comparing(DomainRegion::getCode));

        departmentServices.getAllDepartments()
                .stream()
                .filter( dept -> dept.getDomainRegion() != null )
                .forEach(dept -> regions.add( dept.getDomainRegion() ));

        return new ArrayList<>( regions );
    }


}
