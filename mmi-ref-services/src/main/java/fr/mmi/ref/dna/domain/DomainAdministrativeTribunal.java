package fr.mmi.ref.dna.domain;

import fr.mmi.ref.utils.ExtendedDomainBean;


public class DomainAdministrativeTribunal extends ExtendedDomainBean {

    private Long id;

    private String shortName;

    private String longName;

    private DomainAdministrativeTribunal() {
    }

    public static DomainAdministrativeTribunal create() {
        return new DomainAdministrativeTribunal();
    }

    public DomainAdministrativeTribunal assignFind(Long id, String shortName, String longName) {
        this.id = id;
        this.shortName = shortName;
        this.longName = longName;
        return this;
    }


    public Long getId() {
        return id;
    }

    public String getShortName() {
        return shortName;
    }

    public String getLongName() {
        return longName;
    }

}
