//package fr.ofii.ref.dna.impl;
//
//import com.opengroup.pop.exceptions.DomainException;
//import fr.ofii.ref.dna.TownServices;
//import fr.ofii.ref.dna.domain.DomainTown;
//import fr.ofii.ref.dna.impl.mappers.DomainTownMapper;
//import fr.ofii.ref.jpa.dna.TownRepository;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Service;
//
//import java.util.List;
//
//@Service
//public class TownServicesImpl implements TownServices {
//
//    @Autowired
//    private TownRepository townRepository;
//    @Autowired
//    private DomainTownMapper domainTownMapper;
//
//    @Override
//    public List<DomainTown> getListCodeInseeByZip(String zipCode) throws DomainException{
//        return domainTownMapper.toDomains(townRepository.getListCodeInseeByZip(zipCode));
//    }
//}
