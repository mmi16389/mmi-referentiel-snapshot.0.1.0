package fr.ofii.ref.dna.impl.mappers;

import com.opengroup.pop.exceptions.DomainException;
import com.opengroup.pop.mappers.DomainMapper;
import fr.ofii.ref.dna.domain.DomainParameter;
import fr.ofii.ref.jpa.dna.entities.parameter.Parameter;
import org.springframework.stereotype.Component;



/**
 * A parameter mappers
 */
@Component
public class DomainParameterMapper implements DomainMapper<DomainParameter, Parameter> {

    @Override
    public DomainParameter toOneDomain(Parameter entity) throws DomainException {

        return DomainParameter.fromParamterContextAndValue(
                entity.getId().getParamContext(),
                entity.getId().getParamName(),
                entity.getParamValue()
        );
    }

    @Override
    public Parameter toOneEntity(DomainParameter domain) {
        return null;
    }
}
