package fr.mmi.ref.dna.domain;

import fr.mmi.ref.utils.DomainMethodControlCode;

import java.io.Serializable;

/**
 * The type Domain Departement.
 */
public class DomainDepartment extends fr.ofii.ref.utils.ExtendedDomainBean implements Serializable {

    /**
     * The name of the department
     */
    private String label;
    private String code;
    private DomainRegion domainRegion;

    private DomainDepartment(String code,String label, DomainRegion domainRegion) throws DomainException {
        checkNotEmpty(code, fr.ofii.ref.utils.DomainMethodControlCode.NOT_FILLED_ADRESSE_NUM_VOIE);
        checkNotEmpty(label, DomainMethodControlCode.NOT_FILLED_ADRESSE_LIBELLE_VOIE);
        checkIfValid();

        this.code=code;
        this.label=label;
        this.domainRegion=domainRegion;
    }

    public static DomainDepartment from(String code, String label, DomainRegion domainRegion) throws DomainException {
        return new DomainDepartment(code,label,domainRegion);
    }

    public String getLabel() {
        return label;
    }

    public String getCode() {
        return code;
    }

    public DomainRegion getDomainRegion() {
        return domainRegion;
    }
}
