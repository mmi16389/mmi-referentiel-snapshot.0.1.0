package fr.mmi.ref.dna;

import fr.mmi.ref.dna.domain.DomainParameter;

import java.util.List;

/**
 * Created by EXT_HHE37 on 26/07/2017.
 */
public interface ParameterServices {

    /**
     * Get Parameter Value by Context And Name
     * @param paramContext
     * @param paramName
     * @return
     */
      String getParameterValueByContextAndName(String paramContext, String paramName);

    /**
     * Get Parameter Name by Context and Value
     * @param paramContext
     * @param paramValue
     * @return
     */
      String getParameterNameByContextAndValue(String paramContext, String paramValue);


      List<DomainParameter> getParameterByParamContext(String paramContext)throws DomainException;
}
