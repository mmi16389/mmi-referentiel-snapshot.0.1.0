package fr.mmi.ref.td.impl;

import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IMap;
import com.hazelcast.query.SqlPredicate;
import fr.mmi.ref.CacheNames;
import fr.mmi.ref.jpa.td.TerritorialDirectionDepartementRepository;
import fr.mmi.ref.jpa.td.entities.TerritorialDirectionDepartment;
import fr.mmi.ref.td.TerritorialDirectionServices;
import fr.mmi.ref.td.domain.DomainTerritorialDirection;
import fr.mmi.ref.td.impl.mappers.DomainTerritorialDirectionMapper;
import fr.ofii.ref.utils.DomainMethodControlCode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.Optional;

@Service
public class TerritorialDirectionServicesImpl implements TerritorialDirectionServices {

    private static final Logger LOGGER = LoggerFactory.getLogger(TerritorialDirectionServicesImpl.class);

    @Autowired
    private TerritorialDirectionDepartementRepository territorialDirectionRepository;


    @Autowired
    private DomainTerritorialDirectionMapper domainTerritorialDirectionMapper;

    @Autowired
    private HazelcastInstance hazel;

    private IMap<String, TerritorialDirectionDepartment> map;



    @Override
    public DomainTerritorialDirection findTerritorialDirectionByDepartmentCode(String departmentCode) throws DomainException {

        Optional<TerritorialDirectionDepartment> item = map
                .values( new SqlPredicate( new StringBuilder("__key = '").append( departmentCode ).append( "'").toString()) )
                .stream()
                .findFirst();

        if (!item.isPresent()) {
            throw  new DomainException(LogMessages.create(LOGGER).add(DomainMethodControlCode.TD_NOT_FOUND) );
        }

        return domainTerritorialDirectionMapper.toOneDomain(item.get().getTerritorialDirection());

    }

    @PostConstruct
    private void initialize() throws DomainException {

        this.map = hazel.getMap(CacheNames.DT.getDesignation());
        //this.map.clear();

        territorialDirectionRepository.getAllTerritorialDirectionDepartment()
                .forEach( x-> map.put( x.getDepartmentCode(), x ) );

        Long count = territorialDirectionRepository.count();




        if (LOGGER.isInfoEnabled()) {
            //map.values().forEach(x->LOGGER.info(x.toString()));
            LOGGER.info("##### territorial direction loaded: {}   total: {}", map.size(), count );
        }


    }


}
