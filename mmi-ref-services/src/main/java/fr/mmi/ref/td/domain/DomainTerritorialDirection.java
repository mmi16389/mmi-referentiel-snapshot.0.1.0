package fr.ofii.ref.td.domain;

import com.opengroup.pop.exceptions.DomainException;
import fr.ofii.ref.utils.ExtendedDomainBean;

import java.io.Serializable;


public class DomainTerritorialDirection extends ExtendedDomainBean implements Serializable {

    private Long id;
    private String shortLabel;
    private String longLabel;
    private String phoneNumber;
    private String faxNumber;
    private String service;
    private String firstNameDirector;
    private String lastNameDirector;
    private DomainTerritorialDirectionAddress address;
    private String signaturePath;

    private DomainTerritorialDirection(Long id, String shortLabel, String longLabel, String phoneNumber, String faxNumber, String service, String firstNameDirector, String lastNameDirector, DomainTerritorialDirectionAddress address,String signaturePath) {
        this.id = id;
        this.shortLabel = shortLabel;
        this.longLabel = longLabel;
        this.phoneNumber = phoneNumber;
        this.faxNumber = faxNumber;
        this.service = service;
        this.firstNameDirector = firstNameDirector;
        this.lastNameDirector = lastNameDirector;
        this.address = address;
        this.signaturePath = signaturePath;
    }


    public static DomainTerritorialDirection from(Long id, String label, String name, String phoneNumber, String faxNumber, String service, String firstNameDirector, String lastNameDirector, DomainTerritorialDirectionAddress address, String signaturePath) throws DomainException {
        return new DomainTerritorialDirection(id, label, name, phoneNumber, faxNumber, service, firstNameDirector, lastNameDirector, address, signaturePath);
    }

    public Long getId() {
        return id;
    }

    public String getShortLabel() {
        return shortLabel;
    }

    public String getLongLabel() {
        return longLabel;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public String getFaxNumber() {
        return faxNumber;
    }

    public String getService() {
        return service;
    }

    public void setService(String service) {
        this.service = service;
    }

    public String getFirstNameDirector() {
        return firstNameDirector;
    }

    public String getLastNameDirector() {
        return lastNameDirector;
    }

    public DomainTerritorialDirectionAddress getAddress() {
        return address;
    }

    public String getSignaturePath() {
        return signaturePath;
    }

}
