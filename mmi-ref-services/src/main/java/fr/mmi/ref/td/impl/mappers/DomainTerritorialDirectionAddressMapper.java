package fr.mmi.ref.td.impl.mappers;


import fr.mmi.ref.jpa.td.entities.TerritorialDirectionAddress;
import fr.mmi.ref.td.domain.DomainTerritorialDirectionAddress;
import org.springframework.stereotype.Component;

@Component
public class DomainTerritorialDirectionAddressMapper implements DomainMapper<DomainTerritorialDirectionAddress, TerritorialDirectionAddress> {
    @Override
    public DomainTerritorialDirectionAddress toOneDomain(TerritorialDirectionAddress territorialDirectionAddress) throws DomainException {
        if (territorialDirectionAddress==null) {
            return null;
        }

        return DomainTerritorialDirectionAddress.from(
                territorialDirectionAddress.getId(),
                territorialDirectionAddress.getCity(),
                territorialDirectionAddress.getComplement(),
                territorialDirectionAddress.getInseeCode(),
                territorialDirectionAddress.getStreetName(),
                territorialDirectionAddress.getStreetNumber(),
                territorialDirectionAddress.getZipCode()
        );
    }

    @Override
    public TerritorialDirectionAddress toOneEntity(DomainTerritorialDirectionAddress domainTerritorialDirectionAddress) throws DomainException {
        TerritorialDirectionAddress territorialDirectionAddressEntity = new TerritorialDirectionAddress();
        territorialDirectionAddressEntity.setId(domainTerritorialDirectionAddress.getId());
        territorialDirectionAddressEntity.setCity(domainTerritorialDirectionAddress.getCity());
        territorialDirectionAddressEntity.setComplement(domainTerritorialDirectionAddress.getComplement());
        territorialDirectionAddressEntity.setInseeCode(domainTerritorialDirectionAddress.getInseeCode());
        territorialDirectionAddressEntity.setStreetName(domainTerritorialDirectionAddress.getStreetName());
        territorialDirectionAddressEntity.setStreetNumber(domainTerritorialDirectionAddress.getStreetNumber());
        territorialDirectionAddressEntity.setZipCode(domainTerritorialDirectionAddress.getZipCode());
        return territorialDirectionAddressEntity;
    }
}
