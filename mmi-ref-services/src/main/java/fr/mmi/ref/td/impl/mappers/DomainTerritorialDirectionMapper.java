package fr.mmi.ref.td.impl.mappers;

import fr.ofii.ref.jpa.td.entities.TerritorialDirection;
import fr.ofii.ref.td.domain.DomainTerritorialDirection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.io.File;

/**
 * Map a department "entity" into a "domain" department or a department "domain" into an "entity" department
 */
@Component
public class DomainTerritorialDirectionMapper implements DomainMapper<DomainTerritorialDirection, TerritorialDirection> {

    @Autowired
    private DomainTerritorialDirectionAddressMapper domainTerritorialDirectionAddressMapper;

    @Value("${signature.root.path}")
    private  String signatureRootPath ;

    @Override
    public DomainTerritorialDirection toOneDomain(TerritorialDirection territorialDirection) throws DomainException {
        if (territorialDirection==null) {
            return null;
        }

        return DomainTerritorialDirection.from(
                territorialDirection.getId(),
                territorialDirection.getShortLabel(),
                territorialDirection.getLongLabel(),
                territorialDirection.getPhoneNumber(),
                territorialDirection.getFaxNumber(),
                territorialDirection.getService(),
                territorialDirection.getFirstNameDirector(),
                territorialDirection.getLastNameDirector(),
                domainTerritorialDirectionAddressMapper.toOneDomain(territorialDirection.getAddress()),
                buildFullPath(territorialDirection.getSignaturePath())
        );
    }

    private String buildFullPath(String signaturePath) {
        return (StringUtils.isEmpty(signaturePath))?null: new StringBuilder(signatureRootPath).append(File.separator).append(signaturePath).toString();
    }

    @Override
    public TerritorialDirection toOneEntity(DomainTerritorialDirection domainTerritorialDirection) throws DomainException {
        TerritorialDirection territorialDirectionEntiy=new TerritorialDirection();
        territorialDirectionEntiy.setId(domainTerritorialDirection.getId());
        territorialDirectionEntiy.setShortLabel(domainTerritorialDirection.getShortLabel());
        territorialDirectionEntiy.setLongLabel(domainTerritorialDirection.getLongLabel());
        territorialDirectionEntiy.setPhoneNumber(domainTerritorialDirection.getPhoneNumber());
        territorialDirectionEntiy.setFaxNumber(domainTerritorialDirection.getFaxNumber());
        territorialDirectionEntiy.setService(domainTerritorialDirection.getService());
        territorialDirectionEntiy.setFirstNameDirector(domainTerritorialDirection.getFirstNameDirector());
        territorialDirectionEntiy.setLastNameDirector(domainTerritorialDirection.getLastNameDirector());
        territorialDirectionEntiy.setAddress(domainTerritorialDirectionAddressMapper.toOneEntity(domainTerritorialDirection.getAddress()));
        return territorialDirectionEntiy;
    }
}
