package fr.mmi.ref.td;


import fr.mmi.ref.td.domain.DomainTerritorialDirection;

public interface TerritorialDirectionServices {


    DomainTerritorialDirection findTerritorialDirectionByDepartmentCode(String departmentCode) throws DomainException;

}
