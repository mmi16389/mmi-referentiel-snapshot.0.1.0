package fr.ofii.ref.td.domain;

import fr.ofii.ref.utils.ExtendedDomainBean;

import java.io.Serializable;

public class DomainTerritorialDirectionAddress extends ExtendedDomainBean implements Serializable {

    private Long id;
    private String city;
    private String complement;
    private String inseeCode;
    private String streetName;
    private String streetNumber;
    private String zipCode;

    private DomainTerritorialDirectionAddress(Long id, String city, String complement, String inseeCode, String streetName, String streetNumber, String zipCode) {
        this.id = id;
        this.city = city;
        this.complement = complement;
        this.inseeCode = inseeCode;
        this.streetName = streetName;
        this.streetNumber = streetNumber;
        this.zipCode = zipCode;
    }

    public static DomainTerritorialDirectionAddress from(
            Long id, String city, String complement, String inseeCode, String streetName,
            String streetNumber, String zipCode
    ){
        return new DomainTerritorialDirectionAddress(id,city,complement,inseeCode,streetName,streetNumber,zipCode);
    }

    public Long getId() {
        return id;
    }

    public String getCity() {
        return city;
    }

    public String getComplement() {
        return complement;
    }

    public String getInseeCode() {
        return inseeCode;
    }

    public String getStreetName() {
        return streetName;
    }

    public String getStreetNumber() {
        return streetNumber;
    }

    public String getZipCode() {
        return zipCode;
    }

}
