package fr.mmi.ref;

public enum CacheNames {

    ADDRESS("address"),
    DT("dt"),

    ;

    private String designation;

    CacheNames(String designation) {
        this.designation = designation;
    }

    public String getDesignation() { return designation; }

}
