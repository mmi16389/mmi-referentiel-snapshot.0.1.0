package fr.mmi.ref.resources.dna.mappers;

import com.mmigroup.pop.exceptions.DomainException;
import com.mmigroup.pop.mappers.FrontMapper;
import fr.mmi.ref.dna.domain.DomainRegion;
import fr.mmi.ref.resources.dna.model.RegionRepresentation;
import org.springframework.stereotype.Component;

@Component
public class RegionRepresentionMapper implements FrontMapper<DomainRegion, RegionRepresentation> {


    @Override
    public DomainRegion toOneDomain(RegionRepresentation regionRepresentation) throws DomainException {
        return null;
    }

    @Override
    public RegionRepresentation toOneRepresentation(DomainRegion domainRegion) {
        if (domainRegion == null) {
            return null;
        }

        return new RegionRepresentation().create(domainRegion.getCode(), domainRegion.getLabel());
    }
}
