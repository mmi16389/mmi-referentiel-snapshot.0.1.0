package fr.mmi.ref.resources.dna.model;

import com.mmigroup.pop.RepresentationBean;

public class RegionRepresentation implements RepresentationBean {
    private String code;
    private String label;

    public RegionRepresentation() {
    }

    public RegionRepresentation create(String code,String label) {
        this.code = code;
        this.label = label;
        return this;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }
}
