package fr.mmi.ref.resources.dna;

import com.mmigroup.pop.exceptions.DomainException;
import fr.mmi.ref.dna.ParameterServices;
import fr.mmi.ref.resources.dna.model.ParameterRepresentation;
import fr.mmi.ref.resources.dna.mappers.ParameterRepresentationMapper;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(path = "/api-public/parameter")
public class ParameterResources {

    /**  */
    @Autowired
    private ParameterServices parameterServices;

    /**  */
    @Autowired
    private ParameterRepresentationMapper parameterRepresentationMapper;

    /**
     *
     * @param paramContext
     * @param paramName
     * @return
     */
    @RequestMapping(value = "/get-value-from-context-name", method = RequestMethod.POST)
    @ApiOperation(httpMethod = "POST", value = "")
    public String getParameterValueByContextAndName(String paramContext, String paramName) {
        return parameterServices.getParameterValueByContextAndName(paramContext, paramName);
    }

    /**
     *
     * @param paramContext
     * @param paramValue
     * @return
     */
    @RequestMapping(value = "/get-name-from-context-value", method = RequestMethod.POST)
    @ApiOperation(httpMethod = "POST", value = "")
    public String getParameterNameByContextAndValue(String paramContext, String paramValue) {
        return parameterServices.getParameterNameByContextAndValue(paramContext, paramValue);
    }

    /**
     *
     * @param paramContext
     * @return
     * @throws DomainException
     */
    @RequestMapping(value = "/get-all-from-context", method = RequestMethod.POST)
    @ApiOperation(httpMethod = "POST", value = "")
    public List<ParameterRepresentation> getParameterByParamContext(String paramContext)throws DomainException {
        return parameterRepresentationMapper.toRepresentations( parameterServices.getParameterByParamContext(paramContext));
    }



}
