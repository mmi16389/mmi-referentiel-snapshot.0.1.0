package fr.ofii.ref.resources.hexapost.model;

import com.opengroup.pop.RepresentationBean;

/**
 * An Hexapost (postal referential) client representation
 */
public class HexapostRepresentation implements RepresentationBean {

    /** id */
    private Long id;
    /** row5Label */
    private String row5Label;
    /** wayLabel */
    private String wayLabel;
    /** commune */
    private HexapostCommuneRepresentation commune;
    /** misc data */
    private HexapostStatusRepresentation status;
    /** way2Label */
    private String way2Label;

    /**
     * default constructor
     */
    public HexapostRepresentation() {
    }

    /**
     * full constructor
     * @param id id of the hexapost
     * @param row5Label first label
     * @param wayLabel number, kind of way, label of way
     * @param commune a city object
     * @param status misc data
     * @param way2Label main way label
     */
    public HexapostRepresentation(
            Long id,
            String row5Label,
            String wayLabel,
            HexapostCommuneRepresentation commune,
            HexapostStatusRepresentation status,
            String way2Label
    ) {
        this.id = id;
        this.row5Label = row5Label;
        this.wayLabel = wayLabel;
        this.commune = commune;
        this.status = status;
        this.way2Label = way2Label;
    }

    /**
     * return the id
     * @return id
     */
    public Long getId() {
        return id;
    }

    /**
     * set the id
     * @param id id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * return first label
     * @return label
     */
    public String getRow5Label() {
        return row5Label;
    }

    /**
     * set the first label
     * @param row5Label row5Label
     */
    public void setRow5Label(String row5Label) {
        this.row5Label = row5Label;
    }

    /**
     * get the number, kind of way, label of way value
     * @return label
     */
    public String getWayLabel() {
        return wayLabel;
    }

    /**
     * set the number, kind of way, label of way value
     * @param wayLabel waylabel
     */
    public void setWayLabel(String wayLabel) {
        this.wayLabel = wayLabel;
    }

    /**
     * return a City object
     * @return commune
     */
    public HexapostCommuneRepresentation getCommune() { return commune; }

    /**
     * set a City Object
     * @param commune commune
     */
    public void setCommune(HexapostCommuneRepresentation commune) { this.commune = commune; }

    /**
     * return some misc data
     * @return misc data
     */
    public HexapostStatusRepresentation getStatus() { return status; }

    /**
     * set some misc data
     * @param status status
     */
    public void setStatus(HexapostStatusRepresentation status) { this.status = status; }

    /**
     * return main way label
     * @return main way label
     */
    public String getWay2Label() {
        return way2Label;
    }


    /**
     * set main way label
     * @param way2Label label
     */
    public void setWay2Label(String way2Label) {
        this.way2Label=way2Label;
    }

}
