package fr.mmi.ref.resources.dna.mappers;

import com.mmigroup.pop.exceptions.DomainException;
import com.mmigroup.pop.mappers.FrontMapper;
import fr.mmi.ref.dna.domain.DomainCountry;
import fr.mmi.ref.resources.dna.model.CountryRepresentation;
import org.springframework.stereotype.Component;

@Component
public class CountryRepresentationMapper implements FrontMapper<DomainCountry, CountryRepresentation> {


    @Override
    public DomainCountry toOneDomain(CountryRepresentation countryRepresentation) throws DomainException {
        return null;
    }

    @Override
    public CountryRepresentation toOneRepresentation(DomainCountry domainCountry) {
        return new CountryRepresentation()
                .assign(domainCountry.getId(), domainCountry.getNationalityName(),domainCountry.getName());
    }
}
