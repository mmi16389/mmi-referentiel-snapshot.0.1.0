package fr.mmi.ref.resources.dna;

import com.mmigroup.pop.exceptions.DomainException;
import fr.mmi.ref.dna.AdministrativeTribunalServices;
import fr.mmi.ref.resources.dna.mappers.AdministrativeTribunalRepresentationMapper;
import fr.mmi.ref.resources.dna.model.AdministrativeTribunalRepresentation;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


@RestController
@RequestMapping(path = "/api-public/administrative")
public class AdministrativeTribunalResources {

    /**  */
    @Autowired
    private AdministrativeTribunalServices services;

    /**  */
    @Autowired
    private AdministrativeTribunalRepresentationMapper mapper;

    /**
     * retrieve one administrative tribunal from id
     *
     * @param id technical id
     * @return one administrative tribunal
     * @throws DomainException exception
     */
    @RequestMapping(value = "/find-one", method = RequestMethod.POST)
    @ApiOperation(httpMethod = "POST", value = "Get one administrative tribunal")
    public AdministrativeTribunalRepresentation findOne(Long id)  throws DomainException {
        return mapper.toOneRepresentation( services.findOne(id) );

    }

    /**
     * get all administrative tribunal
     *
     * @return administrative tribunal list
     * @throws DomainException exception
     */
    @RequestMapping(value = "/find-all", method = RequestMethod.POST)
    @ApiOperation(httpMethod = "POST", value = "Get all administrative tribunal")
    public List<AdministrativeTribunalRepresentation> findAll()  throws DomainException {
        return mapper.toRepresentations( services.findAll() );
    }

}
