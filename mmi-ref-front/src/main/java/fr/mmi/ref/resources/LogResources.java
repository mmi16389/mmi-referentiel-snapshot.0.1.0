package fr.mmi.ref.resources;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.PostConstruct;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Paths;


@RestController
//@RequestMapping(path = "/api-public")
public class LogResources {

    @Autowired
    private Environment env;

    private StringBuilder readLog(File file) throws IOException {
        StringBuilder sb = new StringBuilder();
        String line;
        try ( BufferedReader br = new BufferedReader(new FileReader( file ) ) ) {
            while ((line = br.readLine()) != null) {
                sb.append("\n").append( line );
            }
        }
        return sb;
    }

    @RequestMapping(value = "/getLogGda", method = RequestMethod.GET)
    public String getLogGda() throws Exception {
        return getLog("gda");
    }

    @RequestMapping(value = "/get-log", method = RequestMethod.GET)
    public String getLog(String logName) throws Exception {

        return readLog(
                Paths.get(String.format(env.getProperty("logging.path"), logName)).toFile()
        ).toString();

    }


}
