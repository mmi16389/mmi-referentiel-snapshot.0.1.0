package fr.mmi.ref.resources.dna.mappers;

import com.mmigroup.pop.exceptions.DomainException;
import com.mmigroup.pop.mappers.FrontMapper;
import fr.mmi.ref.dna.domain.DomainLanguage;
import fr.mmi.ref.resources.dna.model.LanguageRepresentation;
import org.springframework.stereotype.Component;

@Component
public class LanguageRepresentationMapper implements FrontMapper<DomainLanguage, LanguageRepresentation> {


    @Override
    public DomainLanguage toOneDomain(LanguageRepresentation languageRepresentation) throws DomainException {
        return null;
    }

    @Override
    public LanguageRepresentation toOneRepresentation(DomainLanguage domainLanguage) {
        return new LanguageRepresentation().create(domainLanguage.getId(), domainLanguage.getLanguage());
    }
}
