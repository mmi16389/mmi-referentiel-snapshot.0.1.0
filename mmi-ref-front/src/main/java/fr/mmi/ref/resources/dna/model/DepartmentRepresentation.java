package fr.mmi.ref.resources.dna.model;

import com.mmigroup.pop.RepresentationBean;

public class DepartmentRepresentation implements RepresentationBean {

    private String label;
    private String code;
    private RegionRepresentation regionRepresentation;

    public DepartmentRepresentation() {
    }

    /**
     *
     * @param code
     * @param label
     * @param regionRepresentation
     * @return
     */
    public DepartmentRepresentation create(String code,String label,RegionRepresentation regionRepresentation) {
        this.code=code;
        this.label=label;
        this.regionRepresentation = regionRepresentation;
        return this;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public RegionRepresentation getRegion() {
        return regionRepresentation;
    }

    public void setRegion(RegionRepresentation regionRepresentation) {
        this.regionRepresentation = regionRepresentation;
    }
}
