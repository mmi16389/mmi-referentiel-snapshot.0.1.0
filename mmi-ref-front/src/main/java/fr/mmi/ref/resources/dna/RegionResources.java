package fr.mmi.ref.resources.dna;

import com.mmigroup.pop.exceptions.DomainException;
import fr.mmi.ref.dna.RegionServices;
import fr.mmi.ref.resources.dna.model.RegionRepresentation;
import fr.mmi.ref.resources.dna.mappers.RegionRepresentionMapper;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


@RestController
@RequestMapping(path = "/api-public/region")
public class RegionResources {

    /**  */
    @Autowired
    private RegionServices regionServices;

    /**  */
    @Autowired
    private RegionRepresentionMapper regionRepresentationMapper;

    @RequestMapping(value = "/get-all", method = RequestMethod.POST)
    @ApiOperation(httpMethod = "POST", value = "")
    public List<RegionRepresentation> getRegions() throws DomainException {
        return regionRepresentationMapper.toRepresentations( regionServices.getRegions() );
    }


    }
