package fr.ofii.ref.resources.td;

import com.opengroup.pop.exceptions.DomainException;
import fr.ofii.ref.td.TerritorialDirectionServices;
import fr.ofii.ref.resources.td.mappers.TerritorialDirectionAddressRepresentationMapper;
import fr.ofii.ref.resources.td.mappers.TerritorialDirectionRepresentationMapper;
import fr.ofii.ref.resources.td.model.TerritorialDirectionRepresentation;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping(path = "/api-public/territorial-direction")
@Api(value = "Territorial Direction Resources")
public class TerritorialDirectionResources {


    @Autowired
    private TerritorialDirectionServices territorialDirectionServices;

    @Autowired
    private TerritorialDirectionAddressRepresentationMapper addressRepresentationMapper;

    /**  */
    @Autowired
    private TerritorialDirectionRepresentationMapper territorialDirectionRepresentationMapper;


    /**
     * retrieve Territorial Direction by  Department Code
     * @param codeDepartment department code
     * @return one Territorial Direction
     * @throws DomainException exception
     */
    @RequestMapping(value = "/get-by-dep-code", method = RequestMethod.POST)
    @ApiOperation(httpMethod = "POST", value = "")
    public TerritorialDirectionRepresentation getTerritorialDirectionByDepartmentCode(String codeDepartment) throws DomainException {
        return territorialDirectionRepresentationMapper.toOneRepresentation( territorialDirectionServices.findTerritorialDirectionByDepartmentCode(codeDepartment));
    }


}
