package fr.mmi.ref.resources.dna;

import com.mmigroup.pop.exceptions.DomainException;
import fr.mmi.ref.dna.DepartmentServices;
import fr.mmi.ref.resources.dna.model.DepartmentRepresentation;
import fr.mmi.ref.resources.dna.mappers.DepartmentRepresentationMapper;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


@RestController
@RequestMapping(path = "/api-public/department")
public class DepartmentResources {

    /**  */
    @Autowired
    private DepartmentServices departmentServices;

    /**  */
    @Autowired
    private DepartmentRepresentationMapper departmentRepresentationMapper;

    /**
     * get all departments
     *
     * @return department list
     * @throws DomainException exception
     */
    @RequestMapping(value = "/get-all", method = RequestMethod.GET)
    @ApiOperation(httpMethod = "GET", value = "")
    public List<DepartmentRepresentation> getAllDepartments() throws DomainException {
        return departmentRepresentationMapper.toRepresentations( departmentServices.getAllDepartments() );
    }

    /**
     * retrieve department by its code
     * @param code department code
     * @return one department
     * @throws DomainException exception
     */
    @RequestMapping(value = "/get-by-code", method = RequestMethod.POST)
    @ApiOperation(httpMethod = "POST", value = "")
    public DepartmentRepresentation getDepartmentByCode(String code) throws DomainException {
        return departmentRepresentationMapper.toOneRepresentation( departmentServices.getDepartmentByCode( code ) );
    }

    /**
     * retrieve department match 'label'
     *
     * @param label label department
     * @return department list
     * @throws DomainException exception
     */
    @RequestMapping(value = "/get-contains-name", method = RequestMethod.POST)
    @ApiOperation(httpMethod = "POST", value = "")
    public List<DepartmentRepresentation> getDepartmentContainsName(String label) throws DomainException {
        return departmentRepresentationMapper.toRepresentations( departmentServices.getDepartmentContainsLabel( label ) );
    }


}
