package fr.ofii.ref.resources.td.model;

import com.opengroup.pop.RepresentationBean;


public class TerritorialDirectionAddressRepresentation implements RepresentationBean {

    private Long id;
    private String city;
    private String complement;
    private String inseeCode;
    private String streetName;
    private String streetNumber;
    private String zipCode;

    public TerritorialDirectionAddressRepresentation() {
    }

    public TerritorialDirectionAddressRepresentation assign ( String city, String complement, String inseeCode, String streetName, String streetNumber, String zipCode) {
        this.city = city;
        this.complement = complement;
        this.inseeCode = inseeCode;
        this.streetName = streetName;
        this.streetNumber = streetNumber;
        this.zipCode = zipCode;
        return this;
    }

    public TerritorialDirectionAddressRepresentation create (Long id, String city, String complement, String inseeCode, String streetName, String streetNumber, String zipCode) {

        this.id = id;
        this.city = city;
        this.complement = complement;
        this.inseeCode = inseeCode;
        this.streetName = streetName;
        this.streetNumber = streetNumber;
        this.zipCode = zipCode;
        return this;
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getComplement() {
        return complement;
    }

    public void setComplement(String complement) {
        this.complement = complement;
    }

    public String getInseeCode() {
        return inseeCode;
    }

    public void setInseeCode(String inseeCode) {
        this.inseeCode = inseeCode;
    }

    public String getStreetName() {
        return streetName;
    }

    public void setStreetName(String streetName) {
        this.streetName = streetName;
    }

    public String getStreetNumber() {
        return streetNumber;
    }

    public void setStreetNumber(String streetNumber) {
        this.streetNumber = streetNumber;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }
}
