package fr.mmi.ref.resources.hexapost;

import com.mmigroup.pop.exceptions.DomainException;
import com.mmigroup.pop.exceptions.InternalFrontException;
import fr.mmi.ref.hexapost.HexapostReferentialServices;
import fr.mmi.ref.hexapost.domain.DomainHexapost;
import fr.mmi.ref.resources.hexapost.mappers.HexapostRepresentationMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Hexapost referential Controler
 *
 */
@RestController
@RequestMapping(path = "/api-public/hexapost")
public class HexapostResources {

    /** hexapost service */
    @Autowired
    private HexapostReferentialServices hexapostReferentialServices;

    /** hexapost representation/domain mapper */
    @Autowired
    private HexapostRepresentationMapper adressRepresentationMapper;


    /**
     * return all postalCode/commune whose match filter
     *
     * @param postalCode postalCode/commune to match
     * @return list
     * @throws InternalFrontException exception
     */
    @RequestMapping(value = "/postalCommuneFilterByPostalCode/{postalCode}", method = RequestMethod.GET)
    public List<String> postalCommuneFilterByPostalCode(@PathVariable("postalCode") String postalCode) throws DomainException {

        String thePostalCode = (postalCode == null) ? "" : postalCode;
        return hexapostReferentialServices.postalCommuneFilterByPostalCode(thePostalCode);

    }

    /**
     * get all postal code begin with filter
     * @param filter like "code postal%"
     * @return list address matched
     * @throws DomainException exception
     */
    @RequestMapping(value = "/postalCodeFilter/{filter}", method = RequestMethod.GET)
    public List<String> postalCodeFilter(@PathVariable("filter") String filter) throws DomainException {

        String theFilter = (filter == null) ? "" : filter;
        return hexapostReferentialServices.postalCodeFilter(theFilter);

    }

    /**
     * get all address matching 'postalCode,postalTown' (filter)
     *
     * @param filter like "code postal, commune"
     * @return list address match filter
     * @throws DomainException exception
     */
    @RequestMapping(value = "/postalWayFilter/{filter}", method = RequestMethod.GET)
    public List<DomainHexapost> postalWayFilter(@PathVariable("filter") String filter) throws DomainException {

        String theFilter = (filter == null) ? "" : filter;
        return hexapostReferentialServices.postalWayFilter(theFilter);

    }


    /**
     * all address match filter
     * @param filter  like "libelle voie, code postal, commune"
     * @return list
     * @throws DomainException exception
     */
    @RequestMapping(value = "/finder/{filter}", method = RequestMethod.GET)
    public List<DomainHexapost> finder2(@PathVariable("filter") String filter) throws DomainException {

        String theFilter = filter==null ? ",," : filter;
        return hexapostReferentialServices.wayFilter2( theFilter);
    }



}
