package fr.mmi.ref.resources.dna.mappers;

import com.mmigroup.pop.exceptions.DomainException;
import com.mmigroup.pop.mappers.FrontMapper;
import fr.mmi.ref.dna.domain.DomainDepartment;
import fr.mmi.ref.resources.dna.model.DepartmentRepresentation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class DepartmentRepresentationMapper implements FrontMapper<DomainDepartment, DepartmentRepresentation> {

    @Autowired
    private RegionRepresentionMapper regionRepresentionMapper;



    @Override
    public DomainDepartment toOneDomain(DepartmentRepresentation departmentRepresentation) throws DomainException {
        return null;
    }

    @Override
    public DepartmentRepresentation toOneRepresentation(DomainDepartment domainDepartment) {
        return new DepartmentRepresentation()
                .create(domainDepartment.getCode(),
                        domainDepartment.getLabel(),
                        regionRepresentionMapper.toOneRepresentation(domainDepartment.getDomainRegion())
                );
    }
}
