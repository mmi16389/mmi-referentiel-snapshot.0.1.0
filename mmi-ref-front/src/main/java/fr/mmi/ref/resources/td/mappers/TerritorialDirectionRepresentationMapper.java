package fr.ofii.ref.resources.td.mappers;

import com.opengroup.pop.exceptions.DomainException;
import com.opengroup.pop.mappers.FrontMapper;
import fr.ofii.ref.td.domain.DomainTerritorialDirection;
import fr.ofii.ref.resources.td.model.TerritorialDirectionRepresentation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class TerritorialDirectionRepresentationMapper implements FrontMapper<DomainTerritorialDirection, TerritorialDirectionRepresentation> {

    @Autowired
    private TerritorialDirectionAddressRepresentationMapper addressRepresentationMapper;


    @Override
    public DomainTerritorialDirection toOneDomain(TerritorialDirectionRepresentation territorialDirectionRepresentation) throws DomainException {
        return null;
    }

    @Override
    public TerritorialDirectionRepresentation toOneRepresentation(DomainTerritorialDirection domainTerritorialDirection) {
        if(domainTerritorialDirection==null){
            return null;
        }
        return new TerritorialDirectionRepresentation()
                .create(domainTerritorialDirection.getId(),
                        domainTerritorialDirection.getShortLabel(),
                        domainTerritorialDirection.getLongLabel(),
                        domainTerritorialDirection.getPhoneNumber(),
                        domainTerritorialDirection.getFaxNumber(),
                        domainTerritorialDirection.getService(),
                        domainTerritorialDirection.getFirstNameDirector(),
                        domainTerritorialDirection.getLastNameDirector(),
                        addressRepresentationMapper.toOneRepresentation(domainTerritorialDirection.getAddress()),
                        domainTerritorialDirection.getSignaturePath()

                );
    }


}
