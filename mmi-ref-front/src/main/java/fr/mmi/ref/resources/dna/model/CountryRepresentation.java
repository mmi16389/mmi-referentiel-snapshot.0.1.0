package fr.mmi.ref.resources.dna.model;

import com.mmigroup.pop.RepresentationBean;


public class CountryRepresentation implements RepresentationBean {

    private String id;
    private String name;
    private String nationalityName;

    public CountryRepresentation() {
    }

    /**
     *
     * @param id
     * @param nationalityName
     * @param name
     * @return
     */
    public CountryRepresentation assign(String id, String nationalityName, String name) {
        this.id = id;
        this.nationalityName = nationalityName;
        this.name = name;
        return this;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNationalityName() {
        return nationalityName;
    }

    public void setNationalityName(String nationalityName) {
        this.nationalityName = nationalityName;
    }

}
