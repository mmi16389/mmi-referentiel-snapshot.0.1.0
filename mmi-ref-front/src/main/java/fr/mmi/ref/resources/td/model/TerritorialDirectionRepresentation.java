package fr.ofii.ref.resources.td.model;

import com.opengroup.pop.RepresentationBean;

public class TerritorialDirectionRepresentation implements RepresentationBean {

    private Long id;
    private String shortLabel;
    private String longLabel;
    private String phoneNumber;
    private String faxNumber;
    private String service;
    private String firstNameDirector;
    private String lastNameDirector;
    private TerritorialDirectionAddressRepresentation address;
    private String signaturePath;

    public TerritorialDirectionRepresentation() {
    }

    public TerritorialDirectionRepresentation assign (Long id, String shortLabel, String longLabel, String phoneNumber, String faxNumber, String service, String firstNameDirector, String lastNameDirector, TerritorialDirectionAddressRepresentation address, String signaturePath) {
        this.id = id;
        this.shortLabel = shortLabel;
        this.longLabel = longLabel;
        this.phoneNumber = phoneNumber;
        this.faxNumber = faxNumber;
        this.service = service;
        this.firstNameDirector = firstNameDirector;
        this.lastNameDirector = lastNameDirector;
        this.address = address;
        this.signaturePath = signaturePath;
        return this;
    }

    public TerritorialDirectionRepresentation create(Long id, String shortLabel, String longLabel, String phoneNumber, String faxNumber, String service, String firstNameDirector, String lastNameDirector, TerritorialDirectionAddressRepresentation address, String signaturePath) {
        this.id = id;
        this.shortLabel = shortLabel;
        this.longLabel = longLabel;
        this.phoneNumber = phoneNumber;
        this.faxNumber = faxNumber;
        this.service = service;
        this.firstNameDirector = firstNameDirector;
        this.lastNameDirector = lastNameDirector;
        this.address = address;
        this.signaturePath = signaturePath;
        return this;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getShortLabel() {
        return shortLabel;
    }

    public void setShortLabel(String shortLabel) {
        this.shortLabel = shortLabel;
    }

    public String getLongLabel() {
        return longLabel;
    }

    public void setLongLabel(String longLabel) {
        this.longLabel = longLabel;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getFaxNumber() {
        return faxNumber;
    }

    public void setFaxNumber(String faxNumber) {
        this.faxNumber = faxNumber;
    }

    public String getService() {
        return service;
    }

    public void setService(String service) {
        this.service = service;
    }

    public String getFirstNameDirector() {
        return firstNameDirector;
    }

    public void setFirstNameDirector(String firstNameDirector) {
        this.firstNameDirector = firstNameDirector;
    }

    public String getLastNameDirector() {
        return lastNameDirector;
    }

    public void setLastNameDirector(String lastNameDirector) {
        this.lastNameDirector = lastNameDirector;
    }

    public TerritorialDirectionAddressRepresentation getAddress() {
        return address;
    }

    public void setAddress(TerritorialDirectionAddressRepresentation address) {
        this.address = address;
    }

    public String getSignaturePath() {
        return signaturePath;
    }

    public void setSignaturePath(String signaturePath) {
        this.signaturePath = signaturePath;
    }
}
