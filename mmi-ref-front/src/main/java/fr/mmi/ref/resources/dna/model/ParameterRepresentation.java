package fr.mmi.ref.resources.dna.model;

import com.mmigroup.pop.RepresentationBean;

public class ParameterRepresentation  implements RepresentationBean {

    private String key;
    private String paramName;
    private String value;

    /**
     *
     */
    public ParameterRepresentation() {
    }

    /**
     *
     * @param key
     * @param parameterName
     * @param value
     * @return
     */
    public ParameterRepresentation assign(String key, String parameterName, String value){
        this.key=key;
        this.value=value;
        this.paramName = parameterName;
        return this;
    }

    /**
     *
     * @return
     */
    public String getKey() {
        return key;
    }

    /**
     *
     * @param key
     */
    public void setKey(String key) {
        this.key = key;
    }

    /**
     *
     * @return
     */
    public String getParamName() {
        return paramName;
    }

    /**
     *
     * @param paramName
     */
    public void setParamName(String paramName) {
        this.paramName = paramName;
    }

    /**
     *
     * @return
     */
    public String getValue() {
        return value;
    }

    /**
     *
     * @param value
     */
    public void setValue(String value) {
        this.value = value;
    }
}
