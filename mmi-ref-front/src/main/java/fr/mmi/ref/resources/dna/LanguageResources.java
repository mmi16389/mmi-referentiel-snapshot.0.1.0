package fr.mmi.ref.resources.dna;

import com.mmigroup.pop.exceptions.DomainException;
import fr.mmi.ref.dna.LanguageServices;
import fr.mmi.ref.resources.dna.model.LanguageRepresentation;
import fr.mmi.ref.resources.dna.mappers.LanguageRepresentationMapper;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(path = "/api-public/language")
public class LanguageResources {

    /**  */
    @Autowired
    private LanguageServices languageServices;

    /**  */
    @Autowired
    private LanguageRepresentationMapper languageRepresentationMapper;

    @RequestMapping(value = "/get-by-id", method = RequestMethod.POST)
    @ApiOperation(httpMethod = "POST", value = "Get one language")
    public LanguageRepresentation getLanguage(Long id)  throws DomainException {
        return languageRepresentationMapper.toOneRepresentation( languageServices.getLanguage( id ) );
    }

    @RequestMapping(value = "/get-all", method = RequestMethod.POST)
    @ApiOperation(httpMethod = "POST", value = "Get all language")
    public List<LanguageRepresentation> getLanguages()  throws DomainException {
        return languageRepresentationMapper.toRepresentations( languageServices.getLanguages() );
    }
}
