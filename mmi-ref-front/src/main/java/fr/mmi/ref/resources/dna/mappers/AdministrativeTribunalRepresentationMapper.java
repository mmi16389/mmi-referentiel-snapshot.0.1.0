package fr.mmi.ref.resources.dna.mappers;

import com.mmigroup.pop.exceptions.DomainException;
import com.mmigroup.pop.mappers.FrontMapper;
import fr.mmi.ref.dna.domain.DomainAdministrativeTribunal;
import fr.mmi.ref.resources.dna.model.AdministrativeTribunalRepresentation;
import org.springframework.stereotype.Component;

@Component
public class AdministrativeTribunalRepresentationMapper implements FrontMapper<DomainAdministrativeTribunal, AdministrativeTribunalRepresentation> {


    @Override
    public DomainAdministrativeTribunal toOneDomain(AdministrativeTribunalRepresentation representation) throws DomainException {
        if (representation==null) {
            return null;
        }
        return DomainAdministrativeTribunal
                .create()
                .assignFind(representation.getId(), representation.getShortName(), representation.getLongName());
    }

    @Override
    public AdministrativeTribunalRepresentation toOneRepresentation(DomainAdministrativeTribunal domain) {
        return new AdministrativeTribunalRepresentation()
                .assignFind(domain.getId(), domain.getShortName(), domain.getLongName());
    }
}
