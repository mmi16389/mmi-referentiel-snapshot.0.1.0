package fr.mmi.ref.resources.dna;

import com.mmigroup.pop.exceptions.DomainException;
import fr.mmi.ref.dna.AppealCourtServices;
import fr.mmi.ref.resources.dna.mappers.AppealCourtRepresentationMapper;
import fr.mmi.ref.resources.dna.model.AppealCourtRepresentation;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


@RestController
@RequestMapping(path = "/api-public/court")
public class AppealCourtResources {

    /**  */
    @Autowired
    private AppealCourtServices services;

    /**  */
    @Autowired
    private AppealCourtRepresentationMapper mapper;

    /**
     * retrieve one appeal court tribunal from id
     *
     * @param id technical id
     * @return one appeal court
     * @throws DomainException exception
     */
    @RequestMapping(value = "/find-one", method = RequestMethod.POST)
    @ApiOperation(httpMethod = "POST", value = "Get one appeal court")
    public AppealCourtRepresentation findOne(Long id)  throws DomainException {
        return mapper.toOneRepresentation( services.findOne(id) );

    }

    /**
     * get all appeal court
     *
     * @return appeal court list
     * @throws DomainException exception
     */
    @RequestMapping(value = "/find-all", method = RequestMethod.POST)
    @ApiOperation(httpMethod = "POST", value = "Get all appeal court")
    public List<AppealCourtRepresentation> findAll()  throws DomainException {
        return mapper.toRepresentations( services.findAll() );
    }

}
