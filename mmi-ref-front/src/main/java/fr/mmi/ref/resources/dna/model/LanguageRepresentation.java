package fr.mmi.ref.resources.dna.model;

import com.mmigroup.pop.RepresentationBean;

/**
 *
 */
public class LanguageRepresentation implements RepresentationBean {
    private Long id;
    private String language;

    public LanguageRepresentation() {
    }

    /**
     *
     * @param id
     * @param language
     * @return
     */
    public LanguageRepresentation create(Long id, String language) {
        this.id = id;
        this.language = language;
        return this;
    }

    /**
     *
     * @return
     */
    public Long getId() {
        return id;
    }

    /**
     *
     * @param id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     *
     * @return
     */
    public String getLanguage() {
        return language;
    }

    /**
     *
     * @param language
     */
    public void setLanguage(String language) {
        this.language = language;
    }
}
