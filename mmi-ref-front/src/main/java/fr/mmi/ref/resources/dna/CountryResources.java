package fr.mmi.ref.resources.dna;

import com.mmigroup.pop.exceptions.DomainException;
import fr.mmi.ref.dna.CountryServices;
import fr.mmi.ref.dna.domain.DomainCountry;
import fr.mmi.ref.resources.dna.model.CountryRepresentation;
import fr.mmi.ref.resources.dna.mappers.CountryRepresentationMapper;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;


@RestController
@RequestMapping(path = "/api-public/country")
public class CountryResources {

    /**  */
    @Autowired
    private CountryServices countryServices;

    /**  */
    @Autowired
    private CountryRepresentationMapper countryRepresentationMapper;

    /** */
    // en fait c pas tip top
    private Function<DomainCountry, CountryRepresentation> mapper =
            country -> new CountryRepresentation()
                    .assign(country.getId(), country.getNationalityName(), country.getName());

    /**
     * retrieve country by its id
     *
     * @param id technical id
     * @return one country
     * @throws DomainException exception
     */
    @RequestMapping(value = "/get-country", method = RequestMethod.POST)
    @ApiOperation(httpMethod = "POST", value = "Get one country")
    public CountryRepresentation getCountry(String id)  throws DomainException {
        //DomainCountry dc = countryServices.getCountry( id );
        //return new CountryRepresentation()
        //        .assign(dc.getId(), dc.getNationalityName(), dc.getName());

        return mapper.apply( countryServices.getCountry( id ) );
        //return countryRepresentationMapper.toOneRepresentation( countryServices.getCountry( id ) );
    }

    /**
     * get all countries
     *
     * @return country list
     * @throws DomainException exception
     */
    @RequestMapping(value = "/get-all-country", method = RequestMethod.POST)
    @ApiOperation(httpMethod = "POST", value = "Get one country")
    public List<CountryRepresentation> getCountries()  throws DomainException {
        return countryServices.getCountries()
                .values()
                .stream()
                .map( mapper )
                .collect(Collectors.toList());

        //return countryRepresentationMapper.toRepresentations( countryServices.getCountries().values() );

    }

    /**
     * get all nationalities
     *
     * @return
     * @throws DomainException
     */
    @RequestMapping(value = "/get-all-nationality", method = RequestMethod.POST)
    @ApiOperation(httpMethod = "POST", value = "Get all nationality")
    public List<CountryRepresentation> getNationalities()  throws DomainException {

        return countryRepresentationMapper.toRepresentations( countryServices.getNationalities() );

    }

    /**
     * retrieve nationalities whose match 'namePattern' pattern
     *
     * @param namePattern
     * @return
     * @throws DomainException
     */
    @RequestMapping(value = "/get-nationality", method = RequestMethod.POST)
    @ApiOperation(httpMethod = "POST", value = "Get one nationality")
    public List<CountryRepresentation> getNationality(String namePattern)  throws DomainException {

        return countryRepresentationMapper.toRepresentations( countryServices.getNationality(namePattern) );

    }

    // TO DO : remove for country referential, check if using
    //DomainCountry saveCountry(DomainCountry country) throws DomainException;

    // TO DO : remove for country referential, ok
    //DomainCountry getCountry(String id)  throws DomainException;

    // This method searches the nationality by keyword ok
    //List<DomainCountry> searchByNationalityName(String nationalityName) throws  DomainException;

    // This method fetch All nationalities from the table Country ok
    //List<DomainCountry>getAllNationality() throws  DomainException;



}
