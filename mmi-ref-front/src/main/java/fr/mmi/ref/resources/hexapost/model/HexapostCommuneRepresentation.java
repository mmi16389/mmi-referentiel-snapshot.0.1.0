package fr.ofii.ref.resources.hexapost.model;

import com.opengroup.pop.RepresentationBean;

/**
 * A HexapostCommune client representation
 *
 */
public class HexapostCommuneRepresentation implements RepresentationBean {

    /** insee code */
    private String inseeCode;
    /** city label */
    private String label;
    /** postal code */
    private String postalCode;
    /** postal type */
    private String postalType;
    /** ols insee code */
    private String oldInseeCode;

    /**
     * default constructor
     */
    public HexapostCommuneRepresentation() {
    }

    /**
     * full constructor
     * @param label city label
     * @param inseeCode insee code
     * @param oldInseeCode old insee code
     * @param postalCode postal code
     * @param postalType postal type
     */
    public HexapostCommuneRepresentation(String label, String inseeCode, String oldInseeCode, String postalCode, String postalType) {
        this.label = label;
        this.inseeCode = inseeCode;
        this.oldInseeCode = oldInseeCode;
        this.postalCode = postalCode;
        this.postalType = postalType;
    }

    /**
     * return insee code
     * @return insee code
     */
    public String getInseeCode() {
        return inseeCode;
    }

    /**
     * set insee code
     * @param inseeCode a insee code
     */
    public void setInseeCode(String inseeCode) {
        this.inseeCode = inseeCode;
    }

    /**
     * return city label
     * @return city label
     */
    public String getLabel() {
        return label;
    }

    /**
     * set city label
     * @param label a city label
     */
    public void setLabel(String label) {
        this.label = label;
    }

    /**
     * return postal code
     * @return a postal code
     */
    public String getPostalCode() {
        return postalCode;
    }

    /**
     * set a postal code
     * @param postalCode postal code
     */
    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    /**
     * return old insee code
     * @return an insee code
     */
    public String getOldInseeCode() {
        return oldInseeCode;
    }

    /**
     * set old insee code
     * @param oldInseeCode an insee code
     */
    public void setOldInseeCode(String oldInseeCode) {
        this.oldInseeCode = oldInseeCode;
    }

    /**
     * return postal type
     * @return a postal type
     */
    public String getPostalType() {
        return postalType;
    }

    /**
     * set postal type
     * @param postalType a type
     */
    public void setPostalType(String postalType) {
        this.postalType = postalType;
    }
}
