package fr.mmi.ref.resources.dna.model;

import com.mmigroup.pop.RepresentationBean;


public class AppealCourtRepresentation implements RepresentationBean {

    private Long id;
    private String shortName;
    private String longName;

    public AppealCourtRepresentation() {
    }

    /**
     *
     * @param id
     * @param shortName
     * @param longName
     * @return
     */
    public AppealCourtRepresentation assignFind(Long id, String shortName, String longName) {
        this.id = id;
        this.shortName = shortName;
        this.longName = longName;
        return this;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public String getLongName() {
        return longName;
    }

    public void setLongName(String longName) {
        this.longName = longName;
    }
}
