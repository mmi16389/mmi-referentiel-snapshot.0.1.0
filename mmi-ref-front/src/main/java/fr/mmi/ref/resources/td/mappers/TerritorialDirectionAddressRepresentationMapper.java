package fr.ofii.ref.resources.td.mappers;

import com.opengroup.pop.exceptions.DomainException;
import com.opengroup.pop.mappers.FrontMapper;
import fr.ofii.ref.td.domain.DomainTerritorialDirectionAddress;
import fr.ofii.ref.resources.td.model.TerritorialDirectionAddressRepresentation;
import org.springframework.stereotype.Component;

@Component
public class TerritorialDirectionAddressRepresentationMapper implements FrontMapper<DomainTerritorialDirectionAddress, TerritorialDirectionAddressRepresentation> {
    @Override
    public DomainTerritorialDirectionAddress toOneDomain(TerritorialDirectionAddressRepresentation territorialDirectionAddressRepresentation) throws DomainException {
        return null;
    }

    @Override
    public TerritorialDirectionAddressRepresentation toOneRepresentation(DomainTerritorialDirectionAddress domainTerritorialDirectionAddress) {
        if(domainTerritorialDirectionAddress==null){
            return null;
        }
        return new TerritorialDirectionAddressRepresentation().create(
                domainTerritorialDirectionAddress.getId(),
                domainTerritorialDirectionAddress.getCity(),
                domainTerritorialDirectionAddress.getComplement(),
                domainTerritorialDirectionAddress.getInseeCode(),
                domainTerritorialDirectionAddress.getStreetName(),
                domainTerritorialDirectionAddress.getStreetNumber(),
                domainTerritorialDirectionAddress.getZipCode()
        );
    }
}
