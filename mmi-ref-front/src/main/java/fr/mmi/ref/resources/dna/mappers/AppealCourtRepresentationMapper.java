package fr.mmi.ref.resources.dna.mappers;

import com.mmigroup.pop.exceptions.DomainException;
import com.mmigroup.pop.mappers.FrontMapper;
import fr.mmi.ref.dna.domain.DomainAppealCourt;
import fr.mmi.ref.resources.dna.model.AppealCourtRepresentation;
import org.springframework.stereotype.Component;

@Component
public class AppealCourtRepresentationMapper implements FrontMapper<DomainAppealCourt, AppealCourtRepresentation> {


    @Override
    public DomainAppealCourt toOneDomain(AppealCourtRepresentation representation) throws DomainException {
        if (representation==null) {
            return null;
        }
        return DomainAppealCourt
                .create()
                .assignFind(representation.getId(), representation.getShortName(), representation.getLongName());
    }

    @Override
    public AppealCourtRepresentation toOneRepresentation(DomainAppealCourt domain) {
        return new AppealCourtRepresentation()
                .assignFind(domain.getId(), domain.getShortName(), domain.getLongName());
    }
}
