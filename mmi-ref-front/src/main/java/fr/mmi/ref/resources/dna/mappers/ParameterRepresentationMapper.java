package fr.mmi.ref.resources.dna.mappers;

import com.mmigroup.pop.exceptions.DomainException;
import com.mmigroup.pop.mappers.FrontMapper;
import fr.mmi.ref.dna.domain.DomainParameter;
import fr.mmi.ref.resources.dna.model.ParameterRepresentation;
import org.springframework.stereotype.Component;

@Component
public class ParameterRepresentationMapper implements FrontMapper<DomainParameter, ParameterRepresentation> {


    @Override
    public DomainParameter toOneDomain(ParameterRepresentation parameterRepresentation) throws DomainException {
        return null;
    }

    @Override
    public ParameterRepresentation toOneRepresentation(DomainParameter domainParameter) {
        return new ParameterRepresentation().assign(domainParameter.getKey(), domainParameter.getParamName(), domainParameter.getValue());
    }
}
