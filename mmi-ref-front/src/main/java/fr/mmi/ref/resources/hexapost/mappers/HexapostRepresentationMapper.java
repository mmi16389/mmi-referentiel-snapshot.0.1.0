package fr.mmi.ref.resources.hexapost.mappers;

import com.mmigroup.pop.exceptions.DomainException;
import com.mmigroup.pop.mappers.FrontMapper;
import fr.ofii.ref.hexapost.domain.DomainHexapost;
import fr.ofii.ref.resources.hexapost.model.HexapostRepresentation;
import fr.ofii.ref.resources.hexapost.model.HexapostCommuneRepresentation;
import org.springframework.stereotype.Component;

/**
 * Address representation mappers
 *
 * @author Open group
 * @since 1.0.0
 */
@Component
public class HexapostRepresentationMapper implements FrontMapper<DomainHexapost, HexapostRepresentation> {

    /**
     * Map one Representation object to one Domain object
     *
     * @param representation representation source object
     * @return a domain object
     * @throws DomainException exception
     */
    @Override
    public DomainHexapost toOneDomain(HexapostRepresentation representation) throws DomainException {
        return null;
    }

    /**
     * Map one Domain object to one Representation object
     *
     * @param domain source source object
     * @return a representation object
     */
    @Override
    public HexapostRepresentation toOneRepresentation(DomainHexapost domain){
        //
        HexapostCommuneRepresentation commune = new HexapostCommuneRepresentation();
        commune.setLabel( domain.getTownLabel() );
        commune.setInseeCode( null );
        commune.setOldInseeCode( null );
        commune.setPostalType( null );
        commune.setPostalCode( domain.getPostalCode() );

        //
        return new HexapostRepresentation( domain.getId(),null,null, commune,null, domain.getWayLabel() );
    }
}
