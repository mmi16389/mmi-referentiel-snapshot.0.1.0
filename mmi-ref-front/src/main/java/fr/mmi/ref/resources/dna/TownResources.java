package fr.mmi.ref.resources.dna;

import com.mmigroup.pop.exceptions.DomainException;
import fr.mmi.ref.hexapost.HexapostReferentialServices;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(path = "/api-public/town")
public class TownResources {

    /** hexapost service */
    @Autowired
    private HexapostReferentialServices hexapostReferentialServices;

    @RequestMapping(value = "/get-all", method = RequestMethod.POST)
    @ApiOperation(httpMethod = "POST", value = "Get one country")
    public List<String> getAll(String zipCode)  throws DomainException {
        return hexapostReferentialServices.getListCodeInseeByZip(zipCode );
    }

}
