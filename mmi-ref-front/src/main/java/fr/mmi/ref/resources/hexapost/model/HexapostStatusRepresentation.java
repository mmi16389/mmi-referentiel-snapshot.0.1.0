package fr.ofii.ref.resources.hexapost.model;

import com.opengroup.pop.RepresentationBean;

/**
 * Some Misc data representation for the client
 */
public class HexapostStatusRepresentation implements RepresentationBean {
    /** indic */
    private Integer indic;
    /** majCode */
    private String majCode;
    /** filler */
    private String filler;

    /**
     * default constructor
     */
    public HexapostStatusRepresentation() {
    }

    /**
     * full constructor
     * @param indic hexapost indicator
     * @param majCode maj code
     * @param filler filler
     */
    public HexapostStatusRepresentation(Integer indic, String majCode, String filler) {
        this.indic = indic;
        this.majCode = majCode;
        this.filler = filler;
    }


    /**
     * get hexapost indicator
     * @return indicator
     */
    public Integer getIndic() {
        return indic;
    }

    /**
     * set hexapost indicator
     * @param indic value
     */
    public void setIndic(Integer indic) {
        this.indic = indic;
    }

    /**
     * get hexapost status
     * @return status
     */
    public String getMajCode() {
        return majCode;
    }

    /**
     * set hexapost status
     * @param majCode status
     */
    public void setMajCode(String majCode) {
        this.majCode = majCode;
    }

    /**
     * return filler
     * @return a filler
     */
    public String getFiller() {
        return filler;
    }

    /**
     * set filler
     * @param filler a filler
     */
    public void setFiller(String filler) {
        this.filler = filler;
    }
}
