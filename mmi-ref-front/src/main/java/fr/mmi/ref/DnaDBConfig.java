package fr.mmi.ref;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.jdbc.DataSourceBuilder;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

//@Profile("dna")
@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(
        entityManagerFactoryRef = "dnaEntityManagerFactory",
        transactionManagerRef = "dnaTransactionManager",
        basePackages = { "fr.mmi.ref.jpa.dna" }
)
public class DnaDBConfig {

    @Bean(name = "dnaDataSource")
    @ConfigurationProperties(prefix = "spring.dna.datasource")
    public DataSource dataSource() {
        return DataSourceBuilder.create().build();
    }


    @Bean(name = "dnaEntityManagerFactory")
    public LocalContainerEntityManagerFactoryBean entityManagerFactory(
            EntityManagerFactoryBuilder builder,
            @Qualifier("dnaDataSource") DataSource dataSource
    ) {
        return builder
                .dataSource(dataSource)
                .packages("fr.ofii.ref.jpa.dna.entities")
                .persistenceUnit("dna")
                .build();
    }

    @Bean(name = "dnaTransactionManager")
    public PlatformTransactionManager transactionManager(
            @Qualifier("dnaEntityManagerFactory") EntityManagerFactory entityManagerFactory
    ) {
        return new JpaTransactionManager(entityManagerFactory);
    }


}
