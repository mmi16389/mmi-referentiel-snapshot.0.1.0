package fr.mmi.ref;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.security.Http401AuthenticationEntryPoint;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.*;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;

/**
 * This is where we configure the web application server instance and authentication policy
 *
 * @author Open groupe
 * @since 1.0.0
 */
@Configuration
@EnableAutoConfiguration
//@EnableCaching
@ComponentScan({"fr.mmi.ref", "com.mmigroup.pop"})
@EnableGlobalMethodSecurity(prePostEnabled = true)
@ServletComponentScan({"fr.ofii.ref"})
public class WebApplicationInitializer extends WebSecurityConfigurerAdapter {

    private static final Logger LOGGER = LoggerFactory.getLogger(WebApplicationInitializer.class);

    @Autowired
    private Environment env;

    @Autowired
    private Http401AuthenticationEntryPoint authEntrypoint;


    /**
     * Entry point of the application
     *
     * @param args the input arguments
     * @throws Exception the exception
     */
    public static void main(String[] args) throws Exception {
        new SpringApplicationBuilder(WebApplicationInitializer.class)
                .initializers((ApplicationContextInitializer<ConfigurableApplicationContext>) ac -> {

                    LOGGER.info("##### IBI ");
                    ConfigurableEnvironment appEnvironment = ac.getEnvironment();
                    appEnvironment.getSystemProperties().forEach((key, value) -> LOGGER.info("k=" + key + " v=" + value));
                })
                .run(args);

        //SpringApplication.run(  WebApplicationInitializer.class, args);
    }


    @Override
    protected void configure(HttpSecurity http) throws Exception {

        http.sessionManagement()
                .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                .exceptionHandling().authenticationEntryPoint(authEntrypoint)
                .and()
                .csrf().disable()
                .headers().frameOptions().disable();
    }

    /**
     * Do not remove
     * @return
     * @throws Exception
     */
    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
	 }

    /**
     * Security exception 401 entry point http 401 authentication entry point.
     *
     * @return the http 401 authentication entry point
     */
    @Bean
    public Http401AuthenticationEntryPoint securityException401EntryPoint(){
        return new Http401AuthenticationEntryPoint("Bearer realm=\"webrealm\"");
    }

}
