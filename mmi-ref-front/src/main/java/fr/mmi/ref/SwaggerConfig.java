package fr.mmi.ref;

import com.google.common.collect.Lists;
import com.mmigroup.pop.configuration.FrontConfiguration;
import com.mmigroup.pop.configuration.SwaggerEnabledCondition;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Conditional;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.ApiKey;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;


/**
 * Swagger minimal configuration
 *
 * @author Open groupe
 * @since 1.0.0
 */
@Conditional(SwaggerEnabledCondition.class)
@Configuration
@EnableSwagger2
public class SwaggerConfig {

    @Autowired
    private FrontConfiguration frontConfiguration;

    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis( RequestHandlerSelectors.basePackage( "fr.ofii.ref.resources" ) )
                .paths(PathSelectors.any())
                .build()
                .apiInfo(apiInfo())
                .securitySchemes(Lists.newArrayList(apiKey()))
                ;
    }

    /**
     * API Info as it appears on the swagger-ui page
     */
    private ApiInfo apiInfo() {
        Contact contact = new Contact(  frontConfiguration.getToolSwaggerApiContactName(),
                                        frontConfiguration.getToolSwaggerApiContactUrl(),
                                        frontConfiguration.getToolSwaggerApiContactEmail());
        ApiInfo apiInfo = new ApiInfo(  frontConfiguration.getToolSwaggerApiTitle(),
                                        frontConfiguration.getToolSwaggerApiDescription(),
                                        frontConfiguration.getToolSwaggerApiVersion(),
                                        frontConfiguration.getToolSwaggerApiTermsOfServicesUse(),
                                        contact,
                                        frontConfiguration.getToolSwaggerApiLicence(),
                                        frontConfiguration.getToolSwaggerApiLicenceUrl());
        return apiInfo;
    }

    /**
     * API security key configuration
     */
    private ApiKey apiKey() {
        return new ApiKey("API-Key", "api_key", "header");
    }
}
