package fr.mmi.ref;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.jdbc.DataSourceBuilder;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

//@Profile("td")
@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(
        entityManagerFactoryRef = "tdEntityManagerFactory",
        transactionManagerRef = "tdTransactionManager",
        basePackages = { "fr.mmi.ref.jpa.td" }
)
public class TdDBConfig {

    @Bean(name = "tdDataSource")
    @ConfigurationProperties(prefix = "spring.td.datasource")
    public DataSource dataSource() {
        return DataSourceBuilder.create().build();
    }


    @Bean(name = "tdEntityManagerFactory")
    public LocalContainerEntityManagerFactoryBean entityManagerFactory(
            EntityManagerFactoryBuilder builder,
            @Qualifier("tdDataSource") DataSource dataSource
    ) {
        return builder
                .dataSource(dataSource)
                .packages("fr.ofii.ref.jpa.td.entities")
                .persistenceUnit("td")
                .build();
    }

    @Bean(name = "tdTransactionManager")
    public PlatformTransactionManager transactionManager(
            @Qualifier("tdEntityManagerFactory") EntityManagerFactory entityManagerFactory
    ) {
        return new JpaTransactionManager(entityManagerFactory);
    }


}
