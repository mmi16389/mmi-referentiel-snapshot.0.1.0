package fr.mmi.ref;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;

import java.util.ArrayList;
import java.util.Collection;

/**
 * JWT Configuration
 *
 * @author Open groupe
 * @since 1.0.0
 */
@Configuration
public class JWTConfiguration {

    @Autowired
    private JWTAuthenticationFilter jwtAuthenticationFilter;

    @Autowired
    private Environment env;

    /**
     *
     * @return
     */
    @Bean
    public FilterRegistrationBean jwtFilterRegistrationBean() {
        FilterRegistrationBean registrationBean = new FilterRegistrationBean();
        registrationBean.setName("jwtAuthenticationFilter");
        registrationBean.setFilter(jwtAuthenticationFilter);
        Collection<String> securedUrls = new ArrayList<>();
        String filterUrls = env.getProperty("security.jwt.filter.urls");
        if(filterUrls != null){
            String[] urls = filterUrls.split(",");
            for (String urlPattern : urls){
                securedUrls.add(urlPattern);
            }
        }
        registrationBean.setUrlPatterns(securedUrls);
        registrationBean.setOrder(1);
        return registrationBean;
    }
}
