package fr.ofii.ref;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.jdbc.DataSourceBuilder;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.env.AbstractEnvironment;
import org.springframework.core.env.EnumerablePropertySource;
import org.springframework.core.env.Environment;
import org.springframework.core.env.MutablePropertySources;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import java.util.Arrays;
import java.util.Properties;
import java.util.stream.StreamSupport;

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(
        entityManagerFactoryRef = "hexapostEntityManagerFactory",
        transactionManagerRef = "hexapostTransactionManager",
        basePackages = { "fr.ofii.ref.jpa.hexapost" }
)
public class HexapostDBConfig {
    private static final Logger LOGGER = LoggerFactory.getLogger(HexapostDBConfig.class);

    @Autowired
    private Environment env;

    @Primary
    @Bean(name = "hexapostDataSource")
    @ConfigurationProperties(prefix = "spring.hexapost.datasource")
    public DataSource dataSource() {

        return DataSourceBuilder.create().build();
    }

    @Primary
    @Bean(name = "hexapostEntityManagerFactory")
    public LocalContainerEntityManagerFactoryBean entityManagerFactory(
            EntityManagerFactoryBuilder builder,
            @Qualifier("hexapostDataSource") DataSource dataSource
    ) {
//        Properties props = new Properties();
//        MutablePropertySources propSrcs = ((AbstractEnvironment) env).getPropertySources();
//        StreamSupport.stream(propSrcs.spliterator(), false)
//                .filter(ps -> ps instanceof EnumerablePropertySource)
//                .map(ps -> ((EnumerablePropertySource) ps).getPropertyNames())
//                .flatMap(Arrays::<String>stream)
//                .forEach(propName -> props.setProperty(propName, env.getProperty(propName)));
//        props.keySet().stream().sorted().forEach(x->LOGGER.info("-->" + x+"="+props.get(x)));

        return builder
                .dataSource(dataSource)
                .packages("fr.ofii.ref.jpa.hexapost.entities")
                .persistenceUnit("hexapost")
                .build();
    }

    @Primary
    @Bean(name = "hexapostTransactionManager")
    public PlatformTransactionManager transactionManager(
            @Qualifier("hexapostEntityManagerFactory") EntityManagerFactory entityManagerFactory
    ) {

        return new JpaTransactionManager(entityManagerFactory);
    }


}
