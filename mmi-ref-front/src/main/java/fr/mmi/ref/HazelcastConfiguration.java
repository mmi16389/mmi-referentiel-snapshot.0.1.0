package fr.mmi.ref;

import com.hazelcast.config.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.AbstractEnvironment;
import org.springframework.core.env.EnumerablePropertySource;
import org.springframework.core.env.Environment;
import org.springframework.core.env.MutablePropertySources;

import java.util.Arrays;
import java.util.Properties;
import java.util.stream.StreamSupport;

@Configuration
public class HazelcastConfiguration {

    private static final Logger LOGGER = LoggerFactory.getLogger(HazelcastConfiguration.class);

    private static final String INSTANCE_NAME = "hazelcast.instance.name";
    private static final String MONITORING = "hazelcast.performance.monitoring.enabled";
    private static final String METRIC_LEVEL = "hazelcast.performance.metric.level";
    private static final String MONITOR_DELAY = "hazelcast.performance.monitor.delay.seconds";
    private static final String DIAG_DIRECTORY = "hazelcast.diagnostics.directory";
    private static final String DIAG_FILE_SIZE = "hazelcast.diagnostics.max.rolled.file.size.mb";
    private static final String DIAG_FILE_COUNT = "hazelcast.diagnostics.max.rolled.file.count";



    @Autowired
    private Environment env;

    @Bean
    public Config hazelCastConfig()  {

        //hazelcast.performance.monitoring.enabled=true
        //hazelcast.performance.metric.level=INFO

        LOGGER.info("####### {}", env.getProperty("hazelcast.instance.name") );

        Properties props = new Properties();
        MutablePropertySources propSrcs = ((AbstractEnvironment) env).getPropertySources();
        StreamSupport.stream(propSrcs.spliterator(), false)
                .filter(ps -> ps instanceof EnumerablePropertySource)
                .map(ps -> ((EnumerablePropertySource) ps).getPropertyNames())
                .flatMap(Arrays::<String>stream)
                .filter( x-> x.startsWith("hazelcast"))
                .forEach(propName -> props.setProperty(propName, env.getProperty(propName)));
        props.keySet().stream().sorted().forEach(x->LOGGER.info("-->" + x+"="+props.get(x)));



        Config config = new Config()
                .setInstanceName( env.getProperty("hazelcast.instance.name"))
                .setNetworkConfig(new NetworkConfig()
                        .setJoin(new JoinConfig()
                                .setMulticastConfig( new MulticastConfig().setEnabled(false) )
                        ))
                //).addMapConfig(
                //        new MapConfig()
                //                .setName( CacheNames.ADDRESS.getDesignation() )
                //                .setInMemoryFormat(InMemoryFormat.OBJECT)
                ;

        if ("true".equalsIgnoreCase(env.getProperty( MONITORING ) ) ) {
            LOGGER.debug( MONITORING+"={}", env.getProperty(MONITORING) );
            LOGGER.debug( MONITOR_DELAY+"={}", env.getProperty(MONITOR_DELAY) );
            LOGGER.debug( METRIC_LEVEL+"={}", env.getProperty(METRIC_LEVEL) );
            LOGGER.debug( DIAG_DIRECTORY+"={}", env.getProperty(DIAG_DIRECTORY) );
            LOGGER.debug( DIAG_FILE_SIZE+"={}", env.getProperty(DIAG_FILE_SIZE) );
            LOGGER.debug( DIAG_FILE_COUNT+"={}", env.getProperty(DIAG_FILE_COUNT) );

            config.setProperty( MONITORING, env.getProperty(MONITORING) )
                    .setProperty(MONITOR_DELAY, env.getProperty(MONITOR_DELAY))
                    .setProperty( METRIC_LEVEL, env.getProperty(METRIC_LEVEL) )
                    .setProperty( DIAG_DIRECTORY, env.getProperty( DIAG_DIRECTORY) )
                    .setProperty( DIAG_FILE_SIZE, env.getProperty( DIAG_FILE_SIZE ) )
                    .setProperty( DIAG_FILE_COUNT, env.getProperty( DIAG_FILE_COUNT) );
        }


        return config;
    }
}
