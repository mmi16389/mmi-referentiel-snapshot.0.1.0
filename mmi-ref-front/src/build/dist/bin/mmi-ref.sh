#!/bin/sh
# ---------------------------------------------------------------------------------------------
#              __          __ __                     __       __
#       .-----|  |--.-----|  |  |   .-----.----.----|__.-----|  |_
#       |__ --|     |  -__|  |  |   |__ --|  __|   _|  |  _  |   _|
#       |_____|__|__|_____|__|__|   |_____|____|__| |__|   __|____|
#                                                      |__|
# @author       MMI
# @since        1.0.0
# @description  Manage starting an stopping routine for Spring Boot Embedded Web Server
#
# Environment Variable Prerequisites :
#   POP_BOOT_HOME    May point on your application root path
#
# ---------------------------------------------------------------------------------------------

typeset pidFile=".pidFile"
typeset pidFilePath=$POP_BOOT_HOME/$pidFile
typeset execPath=$POP_BOOT_HOME/lib/${project.artifactId}-${project.version}.${project.packaging}
typeset configurationPath=$POP_BOOT_HOME/conf/application.properties
typeset outputLogPath=$POP_BOOT_HOME/logs/system.log
typeset javaOpts=""
typeset jmxRemoteOpts=""
typeset command="restart"
typeset verbose="false"

function stop {
   if [ -f "$pidFilePath" ];then
       # Kill previous process
       cat $pidFilePath | xargs kill -9
       rm -f $pidFilePath
   fi
   echo "Server is stopped"
}

function start {
   javaOpts=$jmxRemoteOpts
   # Launch a daemon java spring boot server
   #nohup java -Xmx2048m -Xms1024m -jar $javaOpts  $execPath --spring.config.location=file:$configurationPath>/dev/null 2>&1 &
   #nohup java -jar -Xmx2048M -XX:PermSize=2018M -XX:MaxPermSize=2048M $execPath --spring.config.location=file:$configurationPath>/dev/null 2>&1 &
   #              -XX:MaxDirectMemorySize=1G \
   #              -javaagent:mapdb-hz-offheap-VERSION-javaagent.jar \
   #              -Dhz.cluster.max.heartbeat.seconds=300 \
   #              -Dhazelcast.diagnostics.enabled=true \
   #              -Dhazelcast.diagnostics.metric.level=info \
   #              -Dhazelcast.diagnostics.invocation.sample.period.seconds=30 \
   #              -Dhazelcast.diagnostics.pending.invocations.period.seconds=30 \
   #              -Dhazelcast.diagnostics.slowoperations.period.seconds=30 \
   #              -Dhazelcast.diagnostics.storeLatency.period.seconds=60 \
   #              -Dhazelcast.diagnostics.enabled=true \
   #              -Dhazelcast.diagnostics.metric.level=info \
   #-Xmx8192m -Xms4096m

   nohup java -Xmx4096m \
              -Xms2048m \
              -jar \
              $javaOpts \
              $execPath \
              --spring.config.location=file:$configurationPath \
              >/dev/null 2>&1 &
   pid=$!
   echo $pid>$pidFilePath
   cat /proc/meminfo >> $outputLogPath

   ps -ax | grep 'java' >> $outputLogPath
   echo "ibi Server is started with PID=$pid - Logs are in $POP_BOOT_HOME/logs/system.log" >> $outputLogPath
}

function outputTail {
   tail -f $outputLogPath
}

function usage {
   cat <<EOF
Usage: scriptname [-v] [-j] port [-r] command
   -v   executes and prints out verbose messages (press CTRL+C to exit)
   -j   jmx remote on given port
   -r   the command restart|stop (if no provided, default to restart)
EOF
}

while getopts ":r:j:v" opt; do
   case $opt in
      j)
         jmxRemoteOpts="-Dcom.sun.management.jmxremote "
         jmxRemoteOpts=$jmxRemoteOpts"-Dcom.sun.management.jmxremote.port=$OPTARG "
         jmxRemoteOpts=$jmxRemoteOpts"-Dcom.sun.management.jmxremote.local.only=false "
         jmxRemoteOpts=$jmxRemoteOpts"-Dcom.sun.management.jmxremote.authenticate=false "
         jmxRemoteOpts=$jmxRemoteOpts"-Dcom.sun.management.jmxremote.ssl=false"
         echo "Start with jmx remote options : "$jmxRemoteOpts
         ;;
      v)
         verbose=true
         echo "Verbose mode activated ..."
         ;;
      r) command=$OPTARG
         ;;
      \?)
         usage
         ;;
   esac
done

if [ "$command" = "stop" ]; then
   stop
elif [ "$command" = "restart" ] ; then
   stop
   start
   if [ "$verbose" = "true" ]; then
      outputTail
   fi
else
   usage
fi
