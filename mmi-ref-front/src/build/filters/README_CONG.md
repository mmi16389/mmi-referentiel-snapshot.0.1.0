# REFERENTIAL PROPERTIES DESCRIPTION



## Valuation of all variables needed to integrate the application
|Property|Description|Default value
|---|---|---
|**filter.application.environment**|Define the environment|Integration


## Web application server configuration
|Property|Description|Default value
|---|---|---
|**filter.server.port**| |8076
|**filter.server.cors.pattern.urls**| |/**
|**filter.tomcat.ajp.port**| |8006
|**filter.tomcat.ajp.enabled**| |true


## Database connexion
### jpa
|Property|Description|Default value
|---|---|---
|**filter.jpa.show-sql**| |true
|**filter.jpa.hibernate.ddl-auto**| |none
|**filter.jpa.hibernate.generate_statistics**| |false
|**filter.jpa.properties.javax.persistence.query.timeout**| |60000


### Hexapost Database connexion
|Property|Description|Default value
|---|---|---
|**filter.hexapost.datasource.url**| |jdbc:postgresql://sp1d.teamlog.intra:5432/mmi-ref-integration
|**filter.hexapost.datasource.username**| |mmi-dev
|**filter.hexapost.datasource.password**| |$mmiDev
|**filter.hexapost.datasource.driver-class-name**| |org.postgresql.Driver
|**filter.hexapost.datasource.tomcat.initialSize**| |4
|**filter.hexapost.datasource.hibernate.dialect**| |org.hibernate.dialect.PostgreSQLDialect

### Dna Database connexion
|Property|Description|Default value
|---|---|---
|**filter.dna.datasource.url**| |jdbc:postgresql://sp1d.teamlog.intra:5432/mmi-dna-integration
|**filter.dna.datasource.username**| |mmi-dev
|**filter.dna.datasource.password**| |$mmiDev
|**filter.dna.datasource.driver-class-name**| |org.postgresql.Driver
|**filter.dna.datasource.tomcat.initialSize**| |4
|**filter.dna.datasource.hibernate.dialect**| |org.hibernate.dialect.PostgreSQLDialect

### Territorial Direction (TD) Database connexion
|Property|Description|Default value
|---|---|---
|**filter.td.datasource.url**| |jdbc:postgresql://10.170.20.3:5432/mmi-ref-dt
|**filter.td.datasource.username**| |mmi-dev
|**filter.td.datasource.password**| |$mmiDev
|**filter.td.datasource.driver-class-name**| |org.postgresql.Driver
|**filter.td.datasource.tomcat.initialSize**| |4
|**filter.td.datasource.hibernate.dialect**| |org.hibernate.dialect.PostgreSQLDialect


## LDAP connexion (normalement pas utilisé)
|Property|Description|Default value
|---|---|---
|**filter.security.ldap.serverImplementation**| |OpenLDAP
|**filter.security.ldap.ldapUrl**| |ldap://10.170.23.196:389
|**filter.security.ldap.ldapLogin**| |cn=Manager
|**filter.security.ldap.ldapPassword**| |admin
|**filter.security.ldap.userSearchBase**| |ou=users
|**filter.security.ldap.userSearchFilter**| |(uid={0})
|**filter.security.ldap.groupSearchBase**| |ou=groups
|**filter.security.ldap.groupSearchFilter**| |(uniqueMember={0})

## SECURITY configuration
|Property|Description|Default value
|---|---|---
|**filter.security.jwt.secret.path**| |/app-server/security/.keys
|**filter.security.jwt.secret.alias**| |HS256
|**filter.security.jwt.secret.storepass**| |storepop
|**filter.security.jwt.secret.keypass**| |oungawa

## LOGGING configuration - WARN default is in classpath:logback.xml
|Property|Description|Default value
|---|---|---
|**filter.logger.configuration.path**| |/app-server/mmi-ref/conf/logback.xml
|**filter.log.folder.path**| |/app-server/mmi-ref/logs

## Email configuration check the useful
|Property|Description|Default value
|---|---|---
|**filter.mail.sender.address**| |myapplication@open-groupe.com
|**filter.mail.host**| |localhost
|**filter.mail.port**| |25
|**filter.mail.username**| |
|**filter.mail.password**| |
|**filter.mail.protocol**| |smtp
|**filter.mail.default-encoding**| |UTF-8
|**filter.mail.properties.mail.smtp.auth**| |false
|**filter.mail.properties.mail.smtp.starttls.enable**| |false

## Tool configuration
|Property|Description|Default value
|---|---|---
|**filter.tool.swagger.enabled**| |true
|**filter.tool.swagger.api.title**| |API REF
|**filter.tool.swagger.api.description**| |API Description - Integration environment
|**filter.tool.swagger.api.terms-of-services-use**| |Terms of services use
|**filter.tool.swagger.api.contact.name**| |Contact name
|**filter.tool.swagger.api.contact.url**| |Contact URL
|**filter.tool.swagger.api.contact.email**| |Contact email
|**filter.tool.swagger.api.licence**| |Licence
|**filter.tool.swagger.api.licence.url**| |Licence URL

## Hazelcast configuration
|Property|Description|Default value
|---|---|---
|**filter.hazelcast.instance.name**|define instance name, should be change in case of application locks|hazelcast-instance-c
|**filter.hazelcast.performance.monitoring.enabled**|disabled or enabled monitoring function|true
|**filter.hazelcast.performance.metric.level**|monitoring logging level|INFO
|**filter.hazelcast.performance.monitor.delay.seconds**|monitoring delay in seconds|300
|**filter.hazelcast.diagnostics.directory**|monitoring logging path|/app-server/mmi-ref/logs
|**filter.hazelcast.diagnostics.max.rolled.file.size.mb**|logging files size in MegaBytes|10
|**filter.hazelcast.diagnostics.max.rolled.file.count**|total logging files|10

## Territorial Direction Signature Root Path configuration
|Property|Description|Default value
|---|---|---
|**filter.signature.root.path**| |/app-server/shared-data/signature